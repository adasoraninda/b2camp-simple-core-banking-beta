package com.adasoraninda.usermanagementservice.service.impl;

import com.adasoraninda.usermanagementservice.handle.exception.MMenuException;
import com.adasoraninda.usermanagementservice.handle.exception.code.MMenuExceptionCode;
import com.adasoraninda.usermanagementservice.model.entity.MMenu;
import com.adasoraninda.usermanagementservice.model.request.MMenuRequest;
import com.adasoraninda.usermanagementservice.model.response.MMenuResponse;
import com.adasoraninda.usermanagementservice.repository.MMenuRepository;
import com.adasoraninda.usermanagementservice.service.MMenuService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MMenuServiceImpl implements MMenuService {

    private final ModelMapper mapper;

    private final MMenuRepository repository;

    @Override
    public List<MMenuResponse> getAllMenus() {
        var menus = repository.findAllMenu();

        if (menus.isEmpty()) {
            throw new MMenuException(
                    MMenuExceptionCode.MENUS_IS_EMPTY,
                    "Data menu tidak ada");
        }

        return menus.stream()
                .map(menu -> mapper.map(menu, MMenuResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public MMenuResponse getMenuById(Long menuId) {
        return repository.findMenuById(menuId)
                .map(menu -> mapper.map(menu, MMenuResponse.class))
                .orElseThrow(() -> new MMenuException(
                        MMenuExceptionCode.MENU_NOT_FOUND,
                        "Menu dengan id " + menuId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public MMenuResponse createMenu(MMenuRequest menuRequest, String role) {
        var menu = mapper.map(menuRequest, MMenu.class);
        menu.create(role);

        return mapper.map(
                repository.save(menu),
                MMenuResponse.class);
    }

    @Override
    @Transactional
    public MMenuResponse updateMenu(Long menuId, String role, MMenuRequest menuRequest) {
        var oldMenu = repository.findMenuById(menuId)
                .orElseThrow(() -> new MMenuException(
                        MMenuExceptionCode.MENU_NOT_FOUND,
                        "Menu dengan id " + menuId + " tidak ditemukan"));

        oldMenu.update(menuRequest.getName(), menuRequest.getPath(), role);

        return mapper.map(
                repository.save(oldMenu),
                MMenuResponse.class);
    }

    @Override
    @Transactional
    public void deleteMenu(Long menuId, String role) {
        var isMenuExists = repository.existsMenuById(menuId);

        if (!isMenuExists) {
            throw new MMenuException(
                    MMenuExceptionCode.MENU_NOT_FOUND,
                    "Menu dengan id " + menuId + " tidak ditemukan");
        }

        repository.softDelete(menuId, role, LocalDateTime.now());
    }
}
