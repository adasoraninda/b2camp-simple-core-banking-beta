package com.adasoraninda.usermanagementservice.service;

import com.adasoraninda.usermanagementservice.model.request.MUserRequest;
import com.adasoraninda.usermanagementservice.model.response.MUserResponse;

import java.util.List;

public interface MUserService {

    List<MUserResponse> getAllUsers();

    MUserResponse getUserById(Long userId);

    MUserResponse createUser(MUserRequest userRequest, String role);

    MUserResponse updateUser(Long userId, String role, MUserRequest userRequest);

    void deleteUser(Long userId, String role);

}
