package com.adasoraninda.usermanagementservice.service;

import com.adasoraninda.usermanagementservice.model.request.MMenuRequest;
import com.adasoraninda.usermanagementservice.model.response.MMenuResponse;

import java.util.List;

public interface MMenuService {

    List<MMenuResponse> getAllMenus();

    MMenuResponse getMenuById(Long menuId);

    MMenuResponse createMenu(MMenuRequest menuRequest, String role);

    MMenuResponse updateMenu(Long menuId, String role, MMenuRequest menuRequest);

    void deleteMenu(Long menuId, String role);

}
