package com.adasoraninda.usermanagementservice.service.impl;

import com.adasoraninda.usermanagementservice.handle.exception.MRoleException;
import com.adasoraninda.usermanagementservice.handle.exception.code.MRoleExceptionCode;
import com.adasoraninda.usermanagementservice.model.entity.MRole;
import com.adasoraninda.usermanagementservice.model.request.MRoleRequest;
import com.adasoraninda.usermanagementservice.model.response.MRoleResponse;
import com.adasoraninda.usermanagementservice.repository.MRoleRepository;
import com.adasoraninda.usermanagementservice.service.MRoleService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MRoleServiceImpl implements MRoleService {

    private final ModelMapper mapper;

    private final MRoleRepository repository;

    @Override
    public List<MRoleResponse> getAllRoles() {
        var roles = repository.findAllRole();

        if (roles.isEmpty()) {
            throw new MRoleException(
                    MRoleExceptionCode.ROLES_IS_EMPTY,
                    "Data role tidak ada");
        }

        return roles.stream()
                .map(role -> mapper.map(role, MRoleResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public MRoleResponse getRoleById(Long roleId) {
        return repository.findRoleById(roleId)
                .map(role -> mapper.map(role, MRoleResponse.class))
                .orElseThrow(() -> new MRoleException(
                        MRoleExceptionCode.ROLE_NOT_FOUND,
                        "Role dengan id " + roleId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public MRoleResponse createRole(MRoleRequest roleRequest, String role) {
        var newRole = mapper.map(roleRequest, MRole.class);
        newRole.create(role);

        return mapper.map(
                repository.save(newRole),
                MRoleResponse.class);
    }

    @Override
    @Transactional
    public MRoleResponse updateRole(Long roleId, String role, MRoleRequest roleRequest) {
        var oldRole = repository.findRoleById(roleId)
                .orElseThrow(() -> new MRoleException(
                        MRoleExceptionCode.ROLE_NOT_FOUND,
                        "Role dengan id " + roleId + " tidak ditemukan"));

        oldRole.update(roleRequest.getName(), role);

        return mapper.map(oldRole, MRoleResponse.class);
    }

    @Override
    @Transactional
    public void deleteRole(Long roleId, String role) {
        var roleIsExists = repository.existsRoleById(roleId);

        if (!roleIsExists) {
            throw new MRoleException(
                    MRoleExceptionCode.ROLE_NOT_FOUND,
                    "Role dengan id " + roleId + " tidak ditemukan");
        }

        repository.softDelete(roleId, role, LocalDateTime.now());
    }
}
