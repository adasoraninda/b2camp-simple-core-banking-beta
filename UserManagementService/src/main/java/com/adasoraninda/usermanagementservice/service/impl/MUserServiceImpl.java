package com.adasoraninda.usermanagementservice.service.impl;

import com.adasoraninda.usermanagementservice.controller.client.ParameterFeignClient;
import com.adasoraninda.usermanagementservice.handle.SaveHandleEntity;
import com.adasoraninda.usermanagementservice.handle.exception.MRoleException;
import com.adasoraninda.usermanagementservice.handle.exception.MUserException;
import com.adasoraninda.usermanagementservice.handle.exception.code.MRoleExceptionCode;
import com.adasoraninda.usermanagementservice.handle.exception.code.MUserExceptionCode;
import com.adasoraninda.usermanagementservice.model.entity.MUser;
import com.adasoraninda.usermanagementservice.model.request.MUserRequest;
import com.adasoraninda.usermanagementservice.model.response.MUserResponse;
import com.adasoraninda.usermanagementservice.repository.MRoleRepository;
import com.adasoraninda.usermanagementservice.repository.MUserRepository;
import com.adasoraninda.usermanagementservice.service.MUserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MUserServiceImpl implements MUserService {

    private final ModelMapper mapper;

    private MUserRepository userRepository;
    private MRoleRepository roleRepository;

    private final ParameterFeignClient parameterFeignClient;

    @Override
    public List<MUserResponse> getAllUsers() {
        var users = userRepository.findAllUser();

        if (users.isEmpty()) {
            throw new MUserException(
                    MUserExceptionCode.USERS_IS_EMPTY,
                    "Data pengguna tidak ada");
        }

        return users.stream()
                .map(u -> mapper.map(u, MUserResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public MUserResponse getUserById(Long userId) {
        return userRepository.findUserById(userId)
                .map(u -> mapper.map(u, MUserResponse.class))
                .orElseThrow(() -> new MUserException(
                        MUserExceptionCode.USER_NOT_FOUND,
                        "Pengguna dengan id " + userId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public MUserResponse createUser(MUserRequest userRequest, String role) {
        var mRole = roleRepository.findRoleById(userRequest.getRoleId())
                .orElseThrow(() -> new MRoleException(
                        MRoleExceptionCode.ROLE_NOT_FOUND,
                        "Role dengan id " + userRequest.getRoleId() + " tidak ditemukan"));

        var mCity = SaveHandleEntity.handle(() -> parameterFeignClient.getCityById(userRequest.getCityId()),
                "Gagal mendapatkan data kota");
        var mDistrict = SaveHandleEntity.handle(() -> parameterFeignClient.getDistrictById(userRequest.getDistrictId()),
                "Gagal mendapatkan data distrik");
        var rReligion = SaveHandleEntity.handle(() -> parameterFeignClient.getReligionById(userRequest.getReligionId()),
                "Gagal mendapatkan data agama");
        var rEducation = SaveHandleEntity.handle(() -> parameterFeignClient.getEducationById(userRequest.getEducationId()),
                "Gagal mendapatkan data pendidikan");
        var rMaritalStats = SaveHandleEntity.handle(() -> parameterFeignClient.getMaritalStatusById(userRequest.getMaritalStatusId()),
                "Gagal mendapatkan data status pernikahan");

        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        var user = mapper.map(userRequest, MUser.class);
        user.create(mRole, rReligion, rMaritalStats, rEducation, mCity, mDistrict, role);

        return mapper.map(userRepository.save(user), MUserResponse.class);
    }

    @Override
    @Transactional
    public MUserResponse updateUser(Long userId, String role, MUserRequest userRequest) {
        var oldUser = userRepository.findUserById(userId)
                .orElseThrow(() -> new MUserException(
                        MUserExceptionCode.USER_NOT_FOUND,
                        "Pengguna dengan id " + userId + " tidak ditemukan"));

        var mRole = roleRepository.findRoleById(userRequest.getRoleId())
                .orElseThrow(() -> new MRoleException(
                        MRoleExceptionCode.ROLE_NOT_FOUND,
                        "Role dengan id " + userRequest.getRoleId() + " tidak ditemukan"));

        var mCity = SaveHandleEntity.handle(() -> parameterFeignClient.getCityById(userRequest.getCityId()),
                "Gagal mendapatkan data kota");
        var mDistrict = SaveHandleEntity.handle(() -> parameterFeignClient.getDistrictById(userRequest.getDistrictId()),
                "Gagal mendapatkan data distrik");
        var rReligion = SaveHandleEntity.handle(() -> parameterFeignClient.getReligionById(userRequest.getReligionId()),
                "Gagal mendapatkan data agama");
        var rEducation = SaveHandleEntity.handle(() -> parameterFeignClient.getEducationById(userRequest.getEducationId()),
                "Gagal mendapatkan data pendidikan");
        var rMaritalStats = SaveHandleEntity.handle(() -> parameterFeignClient.getMaritalStatusById(userRequest.getMaritalStatusId()),
                "Gagal mendapatkan data status pernikahan");

        oldUser.update(
                userRequest.getUsername(), userRequest.getPassword(), userRequest.getName(), userRequest.getEntryDate(), userRequest.getExpiredDate(), userRequest.getNik(),
                mRole, rReligion, rMaritalStats, rEducation, mCity, mDistrict, role);

        return mapper.map(userRepository.save(oldUser), MUserResponse.class);
    }

    @Override
    @Transactional
    public void deleteUser(Long userId, String role) {
        var userIsExists = userRepository.existsUserById(userId);

        if (!userIsExists) {
            throw new MUserException(
                    MUserExceptionCode.USER_NOT_FOUND,
                    "Pengguna dengan id " + userId + " tidak ditemukan");
        }

        userRepository.softDelete(userId, role, LocalDateTime.now());
    }
}
