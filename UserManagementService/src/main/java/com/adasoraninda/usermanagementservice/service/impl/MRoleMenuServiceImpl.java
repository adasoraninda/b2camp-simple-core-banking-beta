package com.adasoraninda.usermanagementservice.service.impl;

import com.adasoraninda.usermanagementservice.handle.exception.MMenuException;
import com.adasoraninda.usermanagementservice.handle.exception.MRoleException;
import com.adasoraninda.usermanagementservice.handle.exception.MRoleMenuException;
import com.adasoraninda.usermanagementservice.handle.exception.code.MMenuExceptionCode;
import com.adasoraninda.usermanagementservice.handle.exception.code.MRoleExceptionCode;
import com.adasoraninda.usermanagementservice.handle.exception.code.MRoleMenuExceptionCode;
import com.adasoraninda.usermanagementservice.model.entity.MRoleMenu;
import com.adasoraninda.usermanagementservice.model.request.MRoleMenuRequest;
import com.adasoraninda.usermanagementservice.model.response.MRoleMenuResponse;
import com.adasoraninda.usermanagementservice.repository.MMenuRepository;
import com.adasoraninda.usermanagementservice.repository.MRoleMenuRepository;
import com.adasoraninda.usermanagementservice.repository.MRoleRepository;
import com.adasoraninda.usermanagementservice.service.MRoleMenuService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MRoleMenuServiceImpl implements MRoleMenuService {

    private final ModelMapper mapper;

    private MMenuRepository menuRepository;
    private MRoleRepository roleRepository;
    private final MRoleMenuRepository roleMenuRepository;

    @Override
    public List<MRoleMenuResponse> getAllRoleMenus() {
        var roleMenus = roleMenuRepository.findAllRoleMenu();

        if (roleMenus.isEmpty()) {
            throw new MRoleMenuException(
                    MRoleMenuExceptionCode.ROLE_MENU_IS_EMPTY,
                    "Data role menu tidak ada");
        }

        return roleMenus.stream()
                .map(rm -> mapper.map(rm, MRoleMenuResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public MRoleMenuResponse getRoleMenuById(Long roleMenuId) {
        return roleMenuRepository.findRoleMenuById(roleMenuId)
                .map(rm -> mapper.map(rm, MRoleMenuResponse.class))
                .orElseThrow(() -> new MRoleMenuException(
                        MRoleMenuExceptionCode.ROLE_MENU_NOT_FOUND,
                        "Role menu dengan id " + roleMenuId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public MRoleMenuResponse createRoleMenu(MRoleMenuRequest roleMenuRequest, String role) {
        var menu = menuRepository.findMenuById(roleMenuRequest.getMenuId())
                .orElseThrow(() -> new MMenuException(
                        MMenuExceptionCode.MENU_NOT_FOUND,
                        "Menu dengan id " + roleMenuRequest.getMenuId() + " tidak ditemukan"));

        var mRole = roleRepository.findRoleById(roleMenuRequest.getRoleId())
                .orElseThrow(() -> new MRoleException(
                        MRoleExceptionCode.ROLE_NOT_FOUND,
                        "Role dengan id " + roleMenuRequest.getRoleId() + " tidak ditemukan"));

        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        var roleMenu = mapper.map(roleMenuRequest, MRoleMenu.class);
        roleMenu.create(mRole, menu, role);

        return mapper.map(roleMenuRepository.save(roleMenu), MRoleMenuResponse.class);
    }

    @Override
    @Transactional
    public MRoleMenuResponse updateRoleMenu(Long roleMenuId, String role, MRoleMenuRequest roleMenuRequest) {
        var oldRoleMenu = roleMenuRepository.findRoleMenuById(roleMenuId)
                .orElseThrow(() -> new MRoleMenuException(
                        MRoleMenuExceptionCode.ROLE_MENU_NOT_FOUND,
                        "Role menu dengan id " + roleMenuId + " tidak ditemukan"));

        var menu = menuRepository.findMenuById(roleMenuRequest.getMenuId())
                .orElseThrow(() -> new MMenuException(
                        MMenuExceptionCode.MENU_NOT_FOUND,
                        "Menu dengan id " + roleMenuRequest.getMenuId() + " tidak ditemukan"));

        var mRole = roleRepository.findRoleById(roleMenuRequest.getRoleId())
                .orElseThrow(() -> new MRoleException(
                        MRoleExceptionCode.ROLE_NOT_FOUND,
                        "Role dengan id " + roleMenuRequest.getRoleId() + " tidak ditemukan"));

        oldRoleMenu.update(mRole, menu, role);

        return mapper.map(
                roleMenuRepository.save(oldRoleMenu),
                MRoleMenuResponse.class);
    }

    @Override
    @Transactional
    public void deleteRoleMenu(Long roleMenuId, String role) {
        var roleMenuIsExists = roleMenuRepository.existsRoleMenuById(roleMenuId);

        if (!roleMenuIsExists) {
            throw new MRoleMenuException(
                    MRoleMenuExceptionCode.ROLE_MENU_NOT_FOUND,
                    "Role menu dengan id " + roleMenuId + " tidak ditemukan");
        }

        roleMenuRepository.softDelete(roleMenuId, role, LocalDateTime.now());
    }
}
