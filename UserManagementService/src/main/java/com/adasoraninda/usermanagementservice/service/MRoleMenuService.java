package com.adasoraninda.usermanagementservice.service;

import com.adasoraninda.usermanagementservice.model.request.MRoleMenuRequest;
import com.adasoraninda.usermanagementservice.model.response.MRoleMenuResponse;

import java.util.List;

public interface MRoleMenuService {

    List<MRoleMenuResponse> getAllRoleMenus();

    MRoleMenuResponse getRoleMenuById(Long roleMenuId);

    MRoleMenuResponse createRoleMenu(MRoleMenuRequest roleMenuRequest, String role);

    MRoleMenuResponse updateRoleMenu(Long roleMenuId, String role, MRoleMenuRequest roleMenuRequest);

    void deleteRoleMenu(Long roleMenuId, String role);

}
