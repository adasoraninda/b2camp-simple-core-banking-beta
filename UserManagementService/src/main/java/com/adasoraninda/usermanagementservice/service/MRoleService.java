package com.adasoraninda.usermanagementservice.service;

import com.adasoraninda.usermanagementservice.model.request.MRoleRequest;
import com.adasoraninda.usermanagementservice.model.response.MRoleResponse;

import java.util.List;

public interface MRoleService {

    List<MRoleResponse> getAllRoles();

    MRoleResponse getRoleById(Long roleId);

    MRoleResponse createRole(MRoleRequest roleRequest, String role);

    MRoleResponse updateRole(Long roleId, String role, MRoleRequest roleRequest);

    void deleteRole(Long roleId, String role);

}
