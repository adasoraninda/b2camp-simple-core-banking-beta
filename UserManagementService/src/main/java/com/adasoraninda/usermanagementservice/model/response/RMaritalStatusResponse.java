package com.adasoraninda.usermanagementservice.model.response;

import lombok.Data;

@Data
public class RMaritalStatusResponse {
    private Long id;
    private String name;
    private String description;
}
