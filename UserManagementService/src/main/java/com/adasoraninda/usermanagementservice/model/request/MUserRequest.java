package com.adasoraninda.usermanagementservice.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
public class MUserRequest {

    @NotNull(message = "Username harus di isi")
    @NotEmpty(message = "Username tidak boleh kosong")
    @NotBlank(message = "Username tidak boleh hanya spasi")
    @Size(max = 50, message = "Username tidak boleh lebih dari 50 karakter")
    private String username;

    @NotNull(message = "Password harus di isi")
    @NotEmpty(message = "Password tidak boleh kosong")
    @NotBlank(message = "Password tidak boleh hanya spasi")
    @Min(value = 8, message = "Minimal password 8 karakter")
    private String password;

    @NotNull(message = "Nama harus di isi")
    @NotEmpty(message = "Nama tidak boleh kosong")
    @NotBlank(message = "Nama tidak boleh hanya spasi")
    @Size(max = 100, message = "Nama tidak boleh lebih dari 100 karakter")
    private String name;

    @NotNull(message = "NIK harus di isi")
    @NotEmpty(message = "NIK tidak boleh kosong")
    @NotBlank(message = "NIK tidak boleh hanya spasi")
    @Size(max = 20, message = "NIK tidak boleh lebih dari 20 karakter")
    private String nik;

    @NotNull(message = "Tanggal masuk harus di isi")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime entryDate;

    @NotNull(message = "Tanggal kadaluarsa harus di isi")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime expiredDate;

    @NotNull(message = "Id role harus di isi")
    private Long roleId;

    @NotNull(message = "Id agama harus di isi")
    private Long religionId;

    @NotNull(message = "Id pernikahan harus di isi")
    private Long maritalStatusId;

    @NotNull(message = "Id edukasi harus di isi")
    private Long educationId;

    @NotNull(message = "Id Kota harus di isi")
    private Long cityId;

    @NotNull(message = "Id distrik harus di isi")
    private Long districtId;
}
