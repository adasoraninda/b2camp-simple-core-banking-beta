package com.adasoraninda.usermanagementservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class MMenuRequest {
    @NotNull(message = "Nama menu harus di isi")
    @NotEmpty(message = "Nama menu tidak boleh kosong")
    @NotBlank(message = "Nama menu tidak boleh hanya spasi")
    @Size(max = 50, message = "Nama menu tidak boleh lebih dari 50 karakter")
    private String name;

    @NotNull(message = "Path menu harus di isi")
    @NotEmpty(message = "Path menu tidak boleh kosong")
    @NotBlank(message = "Path menu tidak boleh hanya spasi")
    private String path;
}
