package com.adasoraninda.usermanagementservice.model.response;

import lombok.Data;

@Data
public class MCityResponse {
    private Long id;
    private String name;
    private String description;
}
