package com.adasoraninda.usermanagementservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_user")
@EqualsAndHashCode(callSuper = true)
public class MUser extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, unique = true)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private LocalDateTime entryDate;

    @Column(nullable = false)
    private LocalDateTime expiredDate;

    @Column(nullable = false, unique = true)
    private String nik;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private MRole role;

    @ManyToOne
    @JoinColumn(name = "religion_id", nullable = false, referencedColumnName = "id")
    private RReligion religion;

    @ManyToOne
    @JoinColumn(name = "marital_status_id", nullable = false, referencedColumnName = "id")
    private RMaritalStatus maritalStatus;

    @ManyToOne
    @JoinColumn(name = "education_id", nullable = false, referencedColumnName = "id")
    private REducation education;

    @ManyToOne
    @JoinColumn(name = "city_id", nullable = false, referencedColumnName = "id")
    private MCity city;

    @ManyToOne
    @JoinColumn(name = "district_id", nullable = false, referencedColumnName = "id")
    private MDistrict district;

    public void create(
            MRole mRole,
            RReligion religion,
            RMaritalStatus maritalStatus,
            REducation education,
            MCity city,
            MDistrict district,
            String role) {
        super.create(role);

        setRelation(
                mRole,
                religion,
                maritalStatus,
                education,
                city,
                district);
    }

    public void update(
            String username,
            String password,
            String name,
            LocalDateTime entryDate,
            LocalDateTime expiredDate,
            String nik,
            MRole mRole,
            RReligion religion,
            RMaritalStatus maritalStatus,
            REducation education,
            MCity city,
            MDistrict district,
            String role) {
        super.update(role);

        this.username = username;
        this.password = password;
        this.name = name;
        this.entryDate = entryDate;
        this.expiredDate = expiredDate;
        this.nik = nik;

        setRelation(
                mRole,
                religion,
                maritalStatus,
                education,
                city,
                district);
    }

    private void setRelation(
            MRole role,
            RReligion religion,
            RMaritalStatus maritalStatus,
            REducation education,
            MCity city,
            MDistrict district
    ) {
        this.role = role;
        this.religion = religion;
        this.maritalStatus = maritalStatus;
        this.education = education;
        this.city = city;
        this.district = district;
    }

}
