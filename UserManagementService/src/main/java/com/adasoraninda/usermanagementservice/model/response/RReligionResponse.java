package com.adasoraninda.usermanagementservice.model.response;

import lombok.Data;

@Data
public class RReligionResponse {
    private Long id;
    private String name;
    private String description;
}
