package com.adasoraninda.usermanagementservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MRoleMenuRequest {
    @NotNull(message = "Role id harus di isi")
    private Long roleId;

    @NotNull(message = "Menu id harus di isi")
    private Long menuId;
}
