package com.adasoraninda.usermanagementservice.model.response;

import lombok.Data;

@Data
public class MUserResponse {
    private Long id;
    private String username;
    private String password;
    private String name;
    private String nik;
    private String entryDate;
    private String expiredDate;

    private MRoleResponse role;
    private RReligionResponse religion;
    private RMaritalStatusResponse maritalStatus;
    private REducationResponse education;
    private MCityResponse city;
    private MDistrictResponse district;

    public String getEntryDate() {
        return formatDate(this.entryDate);
    }

    public String getExpiredDate() {
        return formatDate(this.expiredDate);
    }

    private String formatDate(String date) {
        if (!date.contains("T")) {
            return date;
        }

        return String.join(" ", date.split("T"));
    }
}
