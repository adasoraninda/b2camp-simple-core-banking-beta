package com.adasoraninda.usermanagementservice.model.response;

import lombok.Data;

@Data
public class MRoleMenuResponse {
    private Long id;
    private MRoleResponse role;
    private MMenuResponse menu;
}
