package com.adasoraninda.usermanagementservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_role_menu")
@EqualsAndHashCode(callSuper = true)
public class MRoleMenu extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, unique = true)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false, referencedColumnName = "id")
    private MRole role;

    @ManyToOne
    @JoinColumn(name = "menu_id", nullable = false, referencedColumnName = "id")
    private MMenu menu;

    public void create(MRole mRole, MMenu menu,String role) {
        super.create(role);
        this.role = mRole;
        this.menu = menu;
    }

    public void update(MRole mRole, MMenu menu, String role) {
        super.update(role);
        this.role = mRole;
        this.menu = menu;
    }

}
