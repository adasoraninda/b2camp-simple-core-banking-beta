package com.adasoraninda.usermanagementservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class MRoleRequest {
    @NotNull(message = "Nama role harus di isi")
    @NotEmpty(message = "Nama role tidak boleh kosong")
    @NotBlank(message = "Nama role tidak boleh hanya spasi")
    @Size(max = 50, message = "Nama role tidak boleh lebih dari 50 karakter")
    private String name;
}
