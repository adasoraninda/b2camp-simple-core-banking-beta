package com.adasoraninda.usermanagementservice.model.response;

import lombok.Data;

@Data
public class MRoleResponse {
    private Long id;
    private String name;
}
