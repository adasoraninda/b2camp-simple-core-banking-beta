package com.adasoraninda.usermanagementservice.model.response;

import lombok.Data;

@Data
public class MMenuResponse {
    private Long id;
    private String name;
    private String path;
}
