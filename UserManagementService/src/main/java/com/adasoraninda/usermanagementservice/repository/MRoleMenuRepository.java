package com.adasoraninda.usermanagementservice.repository;

import com.adasoraninda.usermanagementservice.model.entity.MRoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MRoleMenuRepository extends JpaRepository<MRoleMenu, Long> {

    @Query(value = "SELECT * FROM m_role_menu " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MRoleMenu> findAllRoleMenu();

    @Query(value = "SELECT * FROM m_role_menu " +
            "WHERE id = ?1 " +
            "AND is_deleted = false",
            nativeQuery = true)
    Optional<MRoleMenu> findRoleMenuById(Long id);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_role_menu " +
            "WHERE id = ?1 " +
            "AND is_deleted = false",
            nativeQuery = true)
    boolean existsRoleMenuById(Long id);

    @Modifying
    @Query(value = "UPDATE m_role_menu " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
