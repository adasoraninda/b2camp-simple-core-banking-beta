package com.adasoraninda.usermanagementservice.repository;

import com.adasoraninda.usermanagementservice.model.entity.MRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MRoleRepository extends JpaRepository<MRole, Long> {

    @Query(value = "SELECT * FROM m_role " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MRole> findAllRole();

    @Query(value = "SELECT * FROM m_role " +
            "WHERE id = ?1 " +
            "AND is_deleted = false",
            nativeQuery = true)
    Optional<MRole> findRoleById(Long id);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_role " +
            "WHERE id = ?1 " +
            "AND is_deleted = false",
            nativeQuery = true)
    boolean existsRoleById(Long id);

    @Modifying
    @Query(value = "UPDATE m_role " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
