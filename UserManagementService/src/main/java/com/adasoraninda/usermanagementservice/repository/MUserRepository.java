package com.adasoraninda.usermanagementservice.repository;

import com.adasoraninda.usermanagementservice.model.entity.MUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MUserRepository extends JpaRepository<MUser, Long> {

    @Query(value = "SELECT * FROM m_user " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MUser> findAllUser();

    @Query(value = "SELECT * FROM m_user " +
            "WHERE id = ?1 " +
            "AND is_deleted = false",
            nativeQuery = true)
    Optional<MUser> findUserById(Long id);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_user " +
            "WHERE id = ?1 " +
            "AND is_deleted = false",
            nativeQuery = true)
    boolean existsUserById(Long id);

    @Modifying
    @Query(value = "UPDATE m_user " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
