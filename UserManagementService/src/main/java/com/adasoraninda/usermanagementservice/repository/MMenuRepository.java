package com.adasoraninda.usermanagementservice.repository;

import com.adasoraninda.usermanagementservice.model.entity.MMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MMenuRepository extends JpaRepository<MMenu, Long> {

    @Query(value = "SELECT * FROM m_menu " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MMenu> findAllMenu();

    @Query(value = "SELECT * FROM m_menu " +
            "WHERE id = ?1 " +
            "AND is_deleted = false",
            nativeQuery = true)
    Optional<MMenu> findMenuById(Long id);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_menu " +
            "WHERE id = ?1 " +
            "AND is_deleted = false",
            nativeQuery = true)
    boolean existsMenuById(Long id);

    @Modifying
    @Query(value = "UPDATE m_menu " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
