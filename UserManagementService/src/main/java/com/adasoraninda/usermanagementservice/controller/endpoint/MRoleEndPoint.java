package com.adasoraninda.usermanagementservice.controller.endpoint;

public final class MRoleEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "roles";
    public static final String pathId = "/{" + variableId + "}";
}
