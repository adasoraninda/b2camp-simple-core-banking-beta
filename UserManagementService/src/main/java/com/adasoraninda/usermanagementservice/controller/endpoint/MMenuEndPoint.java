package com.adasoraninda.usermanagementservice.controller.endpoint;

public final class MMenuEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "menus";
    public static final String pathId = "/{" + variableId + "}";
}
