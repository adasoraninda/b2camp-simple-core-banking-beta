package com.adasoraninda.usermanagementservice.controller;

import com.adasoraninda.usermanagementservice.controller.endpoint.MRoleEndPoint;
import com.adasoraninda.usermanagementservice.model.request.MRoleRequest;
import com.adasoraninda.usermanagementservice.model.response.BaseResponse;
import com.adasoraninda.usermanagementservice.model.response.MRoleResponse;
import com.adasoraninda.usermanagementservice.service.MRoleService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.adasoraninda.usermanagementservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.usermanagementservice.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MRoleEndPoint.pathBase)
public class MRoleController {

    private final MRoleService service;

    @GetMapping
    public BaseResponse<List<MRoleResponse>> getAllRoles() {
        return BaseResponse.success(service.getAllRoles());
    }

    @GetMapping(path = MRoleEndPoint.pathId)
    public BaseResponse<MRoleResponse> getRoleById(
            @PathVariable(value = MRoleEndPoint.variableId) Long id
    ) {
        return BaseResponse.success(service.getRoleById(id));
    }

    @PostMapping
    public BaseResponse<MRoleResponse> createRole(
            @Validated @RequestBody MRoleRequest roleRequest,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
            ) {
        return BaseResponse.success(service.createRole(roleRequest,role));
    }

    @PutMapping(path = MRoleEndPoint.pathId)
    public BaseResponse<MRoleResponse> updateRole(
            @PathVariable(value = MRoleEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role,
            @Validated @RequestBody MRoleRequest roleRequest
    ) {
        return BaseResponse.success(service.updateRole(id, role, roleRequest));
    }

    @DeleteMapping(path = MRoleEndPoint.pathId)
    public BaseResponse<Void> deleteRole(
            @PathVariable(value = MRoleEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        service.deleteRole(id, role);

        return BaseResponse.success(null);
    }

}
