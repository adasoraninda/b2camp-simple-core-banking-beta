package com.adasoraninda.usermanagementservice.controller.endpoint;

public final class MRoleMenuEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "role-menu";
    public static final String pathId = "/{" + variableId + "}";
}
