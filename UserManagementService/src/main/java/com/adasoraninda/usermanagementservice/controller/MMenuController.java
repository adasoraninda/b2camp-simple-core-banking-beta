package com.adasoraninda.usermanagementservice.controller;

import com.adasoraninda.usermanagementservice.controller.endpoint.MMenuEndPoint;
import com.adasoraninda.usermanagementservice.model.request.MMenuRequest;
import com.adasoraninda.usermanagementservice.model.response.BaseResponse;
import com.adasoraninda.usermanagementservice.model.response.MMenuResponse;
import com.adasoraninda.usermanagementservice.service.MMenuService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.adasoraninda.usermanagementservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.usermanagementservice.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MMenuEndPoint.pathBase)
public class MMenuController {

    private final MMenuService service;

    @GetMapping
    public BaseResponse<List<MMenuResponse>> getAllMenus() {
        return BaseResponse.success(service.getAllMenus());
    }

    @GetMapping(path = MMenuEndPoint.pathId)
    public BaseResponse<MMenuResponse> getMenuById(
            @PathVariable(value = MMenuEndPoint.variableId) Long id
    ) {
        return BaseResponse.success(service.getMenuById(id));
    }

    @PostMapping
    public BaseResponse<MMenuResponse> createMenu(
            @Validated @RequestBody MMenuRequest menuRequest,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        return BaseResponse.success(service.createMenu(menuRequest, role));
    }

    @PutMapping(path = MMenuEndPoint.pathId)
    public BaseResponse<MMenuResponse> updateMenu(
            @PathVariable(value = MMenuEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role,
            @Validated @RequestBody MMenuRequest menuRequest
    ) {
        return BaseResponse.success(service.updateMenu(id, role, menuRequest));
    }

    @DeleteMapping(path = MMenuEndPoint.pathId)
    public BaseResponse<Void> deleteMenu(
            @PathVariable(value = MMenuEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        service.deleteMenu(id, role);

        return BaseResponse.success(null);
    }

}
