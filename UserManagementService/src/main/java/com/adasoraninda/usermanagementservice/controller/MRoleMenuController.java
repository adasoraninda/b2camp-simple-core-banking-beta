package com.adasoraninda.usermanagementservice.controller;

import com.adasoraninda.usermanagementservice.controller.endpoint.MRoleMenuEndPoint;
import com.adasoraninda.usermanagementservice.model.request.MRoleMenuRequest;
import com.adasoraninda.usermanagementservice.model.response.BaseResponse;
import com.adasoraninda.usermanagementservice.model.response.MRoleMenuResponse;
import com.adasoraninda.usermanagementservice.service.MRoleMenuService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.adasoraninda.usermanagementservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.usermanagementservice.controller.AppRequestUtil.ROLE_PARAM;


@RestController
@AllArgsConstructor
@RequestMapping(path = MRoleMenuEndPoint.pathBase)
public class MRoleMenuController {

    private final MRoleMenuService service;

    @GetMapping
    public BaseResponse<List<MRoleMenuResponse>> getAllRoleMenus() {
        return BaseResponse.success(service.getAllRoleMenus());
    }

    @GetMapping(path = MRoleMenuEndPoint.pathId)
    public BaseResponse<MRoleMenuResponse> getRoleMenuById(
            @PathVariable(value = MRoleMenuEndPoint.variableId) Long id
    ) {
        return BaseResponse.success(service.getRoleMenuById(id));
    }

    @PostMapping
    public BaseResponse<MRoleMenuResponse> createRoleMenu(
            @Validated @RequestBody MRoleMenuRequest roleMenuRequest,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        return BaseResponse.success(service.createRoleMenu(roleMenuRequest, role));
    }

    @PutMapping(path = MRoleMenuEndPoint.pathId)
    public BaseResponse<MRoleMenuResponse> updateRoleMenu(
            @PathVariable(value = MRoleMenuEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role,
            @Validated @RequestBody MRoleMenuRequest roleMenuRequest
    ) {
        return BaseResponse.success(service.updateRoleMenu(id, role, roleMenuRequest));
    }

    @DeleteMapping(path = MRoleMenuEndPoint.pathId)
    public BaseResponse<Void> deleteRoleMenu(
            @PathVariable(value = MRoleMenuEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        service.deleteRoleMenu(id, role);

        return BaseResponse.success(null);
    }

}
