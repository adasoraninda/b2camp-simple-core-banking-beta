package com.adasoraninda.usermanagementservice.controller.endpoint;

public final class MUserEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "users";
    public static final String pathId = "/{" + variableId + "}";
}
