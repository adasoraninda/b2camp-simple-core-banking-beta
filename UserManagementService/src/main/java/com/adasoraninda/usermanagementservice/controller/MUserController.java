package com.adasoraninda.usermanagementservice.controller;

import com.adasoraninda.usermanagementservice.controller.endpoint.MUserEndPoint;
import com.adasoraninda.usermanagementservice.model.request.MUserRequest;
import com.adasoraninda.usermanagementservice.model.response.BaseResponse;
import com.adasoraninda.usermanagementservice.model.response.MUserResponse;
import com.adasoraninda.usermanagementservice.service.MUserService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.adasoraninda.usermanagementservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.usermanagementservice.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MUserEndPoint.pathBase)
public class MUserController {

    private final MUserService service;

    @GetMapping
    public BaseResponse<List<MUserResponse>> getAllUsers() {
        return BaseResponse.success(service.getAllUsers());
    }

    @GetMapping(path = MUserEndPoint.pathId)
    public BaseResponse<MUserResponse> getUserById(
            @PathVariable(value = MUserEndPoint.variableId) Long id
    ) {
        return BaseResponse.success(service.getUserById(id));
    }

    @PostMapping
    public BaseResponse<MUserResponse> createUser(
            @Validated @RequestBody MUserRequest userRequest,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role
    ) {
        return BaseResponse.success(service.createUser(userRequest, role));
    }

    @PutMapping(path = MUserEndPoint.pathId)
    public BaseResponse<MUserResponse> updateUser(
            @PathVariable(value = MUserEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role,
            @Validated @RequestBody MUserRequest userRequest
    ) {
        return BaseResponse.success(service.updateUser(id, role, userRequest));
    }

    @DeleteMapping(path = MUserEndPoint.pathId)
    public BaseResponse<Void> deleteUser(
            @PathVariable(value = MUserEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role
    ) {
        service.deleteUser(id, role);

        return BaseResponse.success(null);
    }

}
