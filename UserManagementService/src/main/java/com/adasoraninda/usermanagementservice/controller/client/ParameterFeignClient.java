package com.adasoraninda.usermanagementservice.controller.client;

import com.adasoraninda.usermanagementservice.controller.endpoint.*;
import com.adasoraninda.usermanagementservice.model.entity.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "parameter-service")
public interface ParameterFeignClient {

    @GetMapping(path = MCityEndPoint.pathBase + MCityEndPoint.pathIdEntity)
    MCity getCityById(@PathVariable(value = MCityEndPoint.variableId) Long id);

    @GetMapping(path = MDistrictEndPoint.pathBase + MDistrictEndPoint.pathIdEntity)
    MDistrict getDistrictById(@PathVariable(value = MDistrictEndPoint.variableId) Long id);

    @GetMapping(path = REducationEndPoint.pathBase + REducationEndPoint.pathIdEntity)
    REducation getEducationById(@PathVariable(value = REducationEndPoint.variableId) Long id);

    @GetMapping(path = RMaritalStatusEndPoint.pathBase + RMaritalStatusEndPoint.pathIdEntity)
    RMaritalStatus getMaritalStatusById(@PathVariable(value = RMaritalStatusEndPoint.variableId) Long id);

    @GetMapping(path = RReligionEndPoint.pathBase + RReligionEndPoint.pathIdEntity)
    RReligion getReligionById(@PathVariable(value = RReligionEndPoint.variableId) Long id);

}
