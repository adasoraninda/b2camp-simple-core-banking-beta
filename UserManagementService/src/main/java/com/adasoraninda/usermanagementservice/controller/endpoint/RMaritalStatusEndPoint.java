package com.adasoraninda.usermanagementservice.controller.endpoint;

public final class RMaritalStatusEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "marital-stats";
    public static final String pathId = "/{" + variableId + "}";
    public static final String pathIdEntity = pathId + "/entity";
}
