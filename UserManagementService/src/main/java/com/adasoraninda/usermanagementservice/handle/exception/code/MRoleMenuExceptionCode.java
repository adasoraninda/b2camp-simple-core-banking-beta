package com.adasoraninda.usermanagementservice.handle.exception.code;

public enum MRoleMenuExceptionCode {
    ROLE_MENU_IS_EMPTY, ROLE_MENU_NOT_FOUND;
}
