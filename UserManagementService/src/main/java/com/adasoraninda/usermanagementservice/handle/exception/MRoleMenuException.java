package com.adasoraninda.usermanagementservice.handle.exception;

import com.adasoraninda.usermanagementservice.handle.exception.code.MRoleMenuExceptionCode;

public class MRoleMenuException extends RuntimeException {

    private final MRoleMenuExceptionCode code;

    public MRoleMenuException(MRoleMenuExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }

}
