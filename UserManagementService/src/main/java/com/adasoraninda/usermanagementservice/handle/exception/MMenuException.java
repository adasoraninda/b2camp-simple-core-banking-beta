package com.adasoraninda.usermanagementservice.handle.exception;

import com.adasoraninda.usermanagementservice.handle.exception.code.MMenuExceptionCode;

public class MMenuException extends RuntimeException {

    private final MMenuExceptionCode code;

    public MMenuException(MMenuExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }
}
