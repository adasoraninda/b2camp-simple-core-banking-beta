package com.adasoraninda.usermanagementservice.handle.exception;

public class SqlErrorMessage {

    public static String getMessage(String key) {
        if (key.equalsIgnoreCase("username")) {
            return "Nama pengguna sudah terdaftar";
        } else if (key.equalsIgnoreCase("nik")) {
            return "NIK sudah terdaftar";
        } else {
            throw new AppCodeException(key);
        }
    }

}
