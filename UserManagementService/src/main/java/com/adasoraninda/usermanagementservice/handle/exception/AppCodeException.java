package com.adasoraninda.usermanagementservice.handle.exception;

public class AppCodeException extends RuntimeException {

    public AppCodeException(String code) {
        super("Kode " + code + " tidak tersedia");
    }

}
