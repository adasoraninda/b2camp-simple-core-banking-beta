package com.adasoraninda.usermanagementservice.handle.exception.code;

public enum MMenuExceptionCode {
    MENUS_IS_EMPTY, MENU_NOT_FOUND;
}
