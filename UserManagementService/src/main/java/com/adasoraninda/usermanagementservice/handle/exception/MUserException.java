package com.adasoraninda.usermanagementservice.handle.exception;

import com.adasoraninda.usermanagementservice.handle.exception.code.MUserExceptionCode;

public class MUserException extends RuntimeException {

    private final MUserExceptionCode code;

    public MUserException(MUserExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }
}
