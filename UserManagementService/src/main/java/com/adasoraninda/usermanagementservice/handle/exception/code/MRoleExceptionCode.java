package com.adasoraninda.usermanagementservice.handle.exception.code;

public enum MRoleExceptionCode {
    ROLES_IS_EMPTY, ROLE_NOT_FOUND;
}
