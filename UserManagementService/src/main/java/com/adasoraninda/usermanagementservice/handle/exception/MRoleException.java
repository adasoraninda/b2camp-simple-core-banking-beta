package com.adasoraninda.usermanagementservice.handle.exception;

import com.adasoraninda.usermanagementservice.handle.exception.code.MRoleExceptionCode;

public class MRoleException extends RuntimeException {

    private final MRoleExceptionCode code;

    public MRoleException(MRoleExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }

}
