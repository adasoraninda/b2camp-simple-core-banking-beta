package com.adasoraninda.usermanagementservice.handle;

import com.adasoraninda.usermanagementservice.handle.exception.*;
import com.adasoraninda.usermanagementservice.model.response.BaseResponse;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

@RestControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllError(Exception ex) {
        var messageBuilder = new StringBuilder();

        try {
            messageBuilder.append(ex.getCause().getCause().getLocalizedMessage());
            if (messageBuilder.toString().contains("Detail:")) {
                var key = messageBuilder.substring(
                        messageBuilder.indexOf("(") + 1, messageBuilder.indexOf(")"));

                messageBuilder.replace(
                        0,
                        messageBuilder.length(),
                        SqlErrorMessage.getMessage(key));

            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            messageBuilder.append(ex.getLocalizedMessage());
        }

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(BaseResponse.error(messageBuilder.toString()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleInputError(MethodArgumentNotValidException ex) {
        var message = ex.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(", "));

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(BaseResponse.error(message));
    }

    @ExceptionHandler(value = {
            MUserException.class,
            MRoleException.class,
            MMenuException.class,
            MRoleMenuException.class})
    public ResponseEntity<Object> handleAppError(Exception ex) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(BaseResponse.error(ex.toString(), ex.getMessage()));
    }

}
