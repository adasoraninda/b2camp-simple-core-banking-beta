package com.adasoraninda.usermanagementservice.handle.exception.code;

public enum MUserExceptionCode {
    USERS_IS_EMPTY, USER_NOT_FOUND;
}
