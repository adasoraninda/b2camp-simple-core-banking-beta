package com.adasoraninda.transactionservice.repository;

import com.adasoraninda.transactionservice.model.entity.TJournalLedgerDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TJournalLedgerDetailRepository extends JpaRepository<TJournalLedgerDetail, Long> {

    @Query(value = "SELECT * FROM t_journal_ledger_detail " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<TJournalLedgerDetail> findAllJournalLedgerDetail();

    @Query(value = "SELECT * FROM t_journal_ledger_detail " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<TJournalLedgerDetail> findJournalLedgerDetailById(Long jldId);

    @Query(value = "SELECT COUNT(*) > 0 FROM t_journal_ledger_detail " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existsJournalLedgerDetailById(Long jldId);

    @Modifying
    @Query(value = "UPDATE t_journal_ledger_detail " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);
}
