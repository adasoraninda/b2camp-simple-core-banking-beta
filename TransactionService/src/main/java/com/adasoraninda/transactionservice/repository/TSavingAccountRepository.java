package com.adasoraninda.transactionservice.repository;

import com.adasoraninda.transactionservice.model.entity.TSavingAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TSavingAccountRepository extends JpaRepository<TSavingAccount, Long> {

    @Query(value = "SELECT * FROM t_saving_account " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<TSavingAccount> findAllSavingAccount();

    @Query(value = "SELECT * FROM t_saving_account " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<TSavingAccount> findSavingAccountById(Long asId);

    @Query(value = "SELECT * FROM t_saving_account " +
            "WHERE is_deleted = false " +
            "AND account_number = ?1",
            nativeQuery = true)
    Optional<TSavingAccount> findSavingAccountByNumber(String accountNumber);

    @Query(value = "SELECT COUNT(*) > 0 FROM t_saving_account " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existsSavingAccountById(Long asId);

    @Modifying
    @Query(value = "UPDATE t_saving_account " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
