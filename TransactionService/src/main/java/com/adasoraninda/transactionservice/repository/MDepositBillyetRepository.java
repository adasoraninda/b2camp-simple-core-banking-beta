package com.adasoraninda.transactionservice.repository;

import com.adasoraninda.transactionservice.model.entity.MDepositBillyet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MDepositBillyetRepository extends JpaRepository<MDepositBillyet, Long> {

    @Query(value = "SELECT * FROM m_deposit_billyet " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MDepositBillyet> findAllDepositBillyet();

    @Query(value = "SELECT * FROM m_deposit_billyet " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MDepositBillyet> findDepositBillyetById(Long dbId);

    @Query(value = "SELECT * FROM m_deposit_billyet " +
            "WHERE is_deleted = false " +
            "AND billyet_number = ?1",
            nativeQuery = true)
    Optional<MDepositBillyet> findDepositBillyetByNumber(String number);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_deposit_billyet " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existsDepositBillyetById(Long dbId);

    @Modifying
    @Query(value = "UPDATE m_deposit_billyet " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);
}
