package com.adasoraninda.transactionservice.repository;

import com.adasoraninda.transactionservice.model.entity.TDepositAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TDepositAccountRepository extends JpaRepository<TDepositAccount, Long> {

    @Query(value = "SELECT * FROM t_deposit_account " +
            "WHERE is_deleted = false " +
            "AND status_id = 1",
            nativeQuery = true)
    List<TDepositAccount> findAllDepositAccount();

    @Query(value = "SELECT * FROM t_deposit_account " +
            "WHERE is_deleted = false " +
            "AND id = ?1 " +
            "AND status_id = 1",
            nativeQuery = true)
    Optional<TDepositAccount> findDepositAccountById(Long daId);

    @Query(value = "SELECT * FROM t_deposit_account " +
            "WHERE is_deleted = false " +
            "AND account_number = ?1 " +
            "AND status_id = 1",
            nativeQuery = true)
    Optional<TDepositAccount> findDepositAccountByNumber(String accountNumber);

    @Query(value = "SELECT COUNT(*) > 0 FROM t_deposit_account " +
            "WHERE id = ?1 " +
            "AND status_id = 1",
            nativeQuery = true)
    boolean existsDepositAccountById(Long daId);

    @Modifying
    @Query(value = "UPDATE t_deposit_account " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true, " +
            "status_id = 2 " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
