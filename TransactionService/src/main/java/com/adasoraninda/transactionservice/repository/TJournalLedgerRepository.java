package com.adasoraninda.transactionservice.repository;

import com.adasoraninda.transactionservice.model.entity.TJournalLedger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TJournalLedgerRepository extends JpaRepository<TJournalLedger, Long> {

    @Query(value = "SELECT * FROM t_journal_ledger " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<TJournalLedger> findAllJournalLedger();

    @Query(value = "SELECT * FROM t_journal_ledger " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<TJournalLedger> findJournalLedgerById(Long jlId);

    @Query(value = "SELECT COUNT(*) > 0 FROM t_journal_ledger " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existsJournalLedgerById(Long jlId);

    @Modifying
    @Query(value = "UPDATE t_journal_ledger " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);
}
