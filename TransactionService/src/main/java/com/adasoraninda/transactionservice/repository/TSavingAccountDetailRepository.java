package com.adasoraninda.transactionservice.repository;

import com.adasoraninda.transactionservice.model.entity.TSavingAccountDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TSavingAccountDetailRepository extends JpaRepository<TSavingAccountDetail, Long> {

    @Query(value = "SELECT * FROM t_saving_account_detail " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<TSavingAccountDetail> findAllSavingAccountDetail();

    @Query(value = "SELECT * FROM t_saving_account_detail " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<TSavingAccountDetail> findSavingAccountDetailById(Long asdId);

    @Query(value = "SELECT COUNT(*) > 0 FROM t_saving_account_detail " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existsSavingAccountDetailById(Long asdId);

    @Modifying
    @Query(value = "UPDATE t_saving_account_detail " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
