package com.adasoraninda.transactionservice.controller;

public final class AppRequestUtil {
    public static final String ROLE_PARAM = "role";
    public static final String DEFAULT_ROLE = "system";
}
