package com.adasoraninda.transactionservice.controller.code;

import com.adasoraninda.transactionservice.handle.exception.AppCodeException;

public enum TransactionCode {
    SETOR_TUNAI(1001), TARIK_TUNAI(1002), OVERBOOK(1003);

    private final int code;

    TransactionCode(int code) {
        this.code = code;
    }

    public static TransactionCode valueOf(int code) {
        if (code == SETOR_TUNAI.code) {
            return SETOR_TUNAI;
        } else if (code == TARIK_TUNAI.code) {
            return TARIK_TUNAI;
        } else if (code == OVERBOOK.code) {
            return OVERBOOK;
        } else {
            throw new AppCodeException(String.valueOf(code));
        }
    }

    public int getCode() {
        return code;
    }

}
