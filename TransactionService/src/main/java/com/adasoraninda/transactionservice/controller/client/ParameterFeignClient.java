package com.adasoraninda.transactionservice.controller.client;

import com.adasoraninda.transactionservice.controller.endpoint.*;
import com.adasoraninda.transactionservice.model.entity.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "parameter-service")
public interface ParameterFeignClient {

    @GetMapping(path = MCoaEndPoint.pathBase + MCoaEndPoint.pathCodeEntity)
    MCoa getCoaByCode(
            @PathVariable(value = MCoaEndPoint.variableCode) String code);

    @GetMapping(path = MCifEndPoint.pathBase + MCifEndPoint.pathNikEntity)
    MCif getCifByNik(@PathVariable(MCifEndPoint.variableNik) String nik);

    @GetMapping(path = MSavingProductEndPoint.pathBase + MSavingProductEndPoint.pathNameEntity)
    MSavingProduct getSavingProductByProductName(
            @PathVariable(value = MSavingProductEndPoint.variableName) String productName);

    @GetMapping(path = MTransactionCodeEndPoint.pathBase + MTransactionCodeEndPoint.pathCodeEntity)
    MTransactionCode getTransactionCodeByCode(
            @PathVariable(value = MTransactionCodeEndPoint.variableCode) Integer code);

    @GetMapping(path = MDepositProductEndPoint.pathBase + MDepositProductEndPoint.pathNameEntity)
    MDepositProduct getDepositProductByName(
            @PathVariable(value = MDepositProductEndPoint.variableName) String name);

    @GetMapping(path = RStatusEndPoint.pathBase +RStatusEndPoint.pathCodeEntity)
    RStatus getStatusByCode(
            @PathVariable(value = RStatusEndPoint.variableCode) String code);

}
