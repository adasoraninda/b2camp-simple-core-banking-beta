package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.TSavingAccountEndPoint;
import com.adasoraninda.transactionservice.model.request.TSavingAccountRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.model.response.TSavingAccountResponse;
import com.adasoraninda.transactionservice.service.TSavingAccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = TSavingAccountEndPoint.pathBase)
public class TSavingAccountController {

    private final TSavingAccountService service;

    @GetMapping
    public BaseResponse<List<TSavingAccountResponse>> getAllSavingAccount() {
        return BaseResponse.success(service.getAllSavingAccount());
    }

    @GetMapping(path = TSavingAccountEndPoint.pathId)
    public BaseResponse<TSavingAccountResponse> getSavingAccountById(
            @PathVariable(value = TSavingAccountEndPoint.variableId) Long saId
    ) {
        return BaseResponse.success(service.getSavingAccountById(saId));
    }

    @PostMapping
    public BaseResponse<TSavingAccountResponse> createSavingAccount(
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody TSavingAccountRequest saRequest) {
        return BaseResponse.success(service.createSavingAccount(role, saRequest));
    }

    @PutMapping(path = TSavingAccountEndPoint.pathId)
    public BaseResponse<TSavingAccountResponse> updateSavingAccount(
            @PathVariable(value = TSavingAccountEndPoint.variableId) Long saId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody TSavingAccountRequest saRequest) {
        return BaseResponse.success(service.updateSavingAccount(saId, role, saRequest));
    }

    @DeleteMapping(path = TSavingAccountEndPoint.pathId)
    public BaseResponse<Void> deleteSavingAccount(
            @PathVariable(value = TSavingAccountEndPoint.variableId) Long saId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        service.deleteSavingAccount(saId, role);
        return BaseResponse.success(null);
    }

}
