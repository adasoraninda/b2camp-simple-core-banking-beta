package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.TellerInterestTransactionEndPoint;
import com.adasoraninda.transactionservice.model.request.TellerInterestTransactionRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.model.response.TDepositAccountResponse;
import com.adasoraninda.transactionservice.service.TellerInterestTransactionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping(path = TellerInterestTransactionEndPoint.pathBase)
public class TellerInterestTransactionController {

    private final TellerInterestTransactionService service;

    @GetMapping
    public BaseResponse<TDepositAccountResponse> interestRateTransaction(
            @Valid @RequestBody TellerInterestTransactionRequest titRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        return BaseResponse.success(service.interestRateTransaction(titRequest, role));
    }

}
