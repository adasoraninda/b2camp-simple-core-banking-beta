package com.adasoraninda.transactionservice.controller.code;

public enum StatusCode {
    ACTIVE, INACTIVE
}
