package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.TDepositAccountDetailEndPoint;
import com.adasoraninda.transactionservice.model.request.TDepositAccountDetailRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.model.response.TDepositAccountDetailResponse;
import com.adasoraninda.transactionservice.service.TDepositAccountDetailService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = TDepositAccountDetailEndPoint.pathBase)
public class TDepositAccountDetailController {

    private final TDepositAccountDetailService service;

    @GetMapping
    public BaseResponse<List<TDepositAccountDetailResponse>> getAllDepositAccountDetail() {
        return BaseResponse.success(service.getAllDepositAccountDetail());
    }

    @GetMapping(path = TDepositAccountDetailEndPoint.pathId)
    public BaseResponse<TDepositAccountDetailResponse> getDepositAccountDetailById(
            @PathVariable(value = TDepositAccountDetailEndPoint.variableId) Long dadId
    ) {
        return BaseResponse.success(service.getDepositAccountDetailById(dadId));
    }

    @PostMapping
    public BaseResponse<TDepositAccountDetailResponse> createDepositAccountDetail(
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody TDepositAccountDetailRequest dadRequest) {
        return BaseResponse.success(service.createDepositAccountDetail(role, dadRequest));
    }

    @PutMapping(path = TDepositAccountDetailEndPoint.pathId)
    public BaseResponse<TDepositAccountDetailResponse> updateDepositAccountDetailById(
            @PathVariable(value = TDepositAccountDetailEndPoint.variableId) Long dadId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody TDepositAccountDetailRequest dadRequest) {
        return BaseResponse.success(service.updateDepositAccountDetailById(dadId, role, dadRequest));
    }

    @DeleteMapping(path = TDepositAccountDetailEndPoint.pathId)
    public BaseResponse<Object> deleteDepositAccountDetailById(
            @PathVariable(value = TDepositAccountDetailEndPoint.variableId) Long dadId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        service.deleteDepositAccountDetailById(dadId, role);
        return BaseResponse.success(null);
    }

}
