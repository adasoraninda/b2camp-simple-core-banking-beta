package com.adasoraninda.transactionservice.controller.endpoint;

public final class MTransactionCodeEndPoint {
    public static final String variableId = "id";
    public static final String variableCode = "code";
    public static final String pathBase = "transaction-codes";
    public static final String pathId = "/{" + variableId + "}";
    public static final String pathCode = "/{" + variableCode + "}";
    public static final String pathIdEntity = pathId + "/entity/id";
    public static final String pathCodeEntity = pathCode + "/entity/code";
}
