package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.TJournalLedgerDetailEndPoint;
import com.adasoraninda.transactionservice.model.request.TJournalLedgerDetailRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.model.response.TJournalLedgerDetailResponse;
import com.adasoraninda.transactionservice.service.TJournalLedgerDetailService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = TJournalLedgerDetailEndPoint.pathBase)
public class TJournalLedgerDetailController {

    private final TJournalLedgerDetailService service;

    @GetMapping
    public BaseResponse<List<TJournalLedgerDetailResponse>> getAllJournalLedgerDetail() {
        return BaseResponse.success(service.getAllJournalLedgerDetail());
    }

    @GetMapping(value = TJournalLedgerDetailEndPoint.pathId)
    public BaseResponse<TJournalLedgerDetailResponse> getJournalLedgerDetailById(
            @PathVariable(value = TJournalLedgerDetailEndPoint.variableId) Long jldId) {
        return BaseResponse.success(service.getJournalLedgerDetailById(jldId));
    }

    @PostMapping
    public BaseResponse<TJournalLedgerDetailResponse> createJournalLedgerDetail(
            @Valid @RequestBody TJournalLedgerDetailRequest jldRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        return BaseResponse.success(service.createJournalLedgerDetail(jldRequest, role));
    }

    @PutMapping(value = TJournalLedgerDetailEndPoint.pathId)
    public BaseResponse<TJournalLedgerDetailResponse> updateJournalLedgerDetailById(
            @PathVariable(value = TJournalLedgerDetailEndPoint.variableId) Long jldId,
            @Valid @RequestBody TJournalLedgerDetailRequest jldRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        return BaseResponse.success(service.updateJournalLedgerDetailById(jldId, jldRequest, role));
    }

    @DeleteMapping(value = TJournalLedgerDetailEndPoint.pathId)
    public BaseResponse<Object> deleteJournalLedgerDetailById(
            @PathVariable(value = TJournalLedgerDetailEndPoint.variableId) Long jldId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        service.deleteJournalLedgerDetailById(jldId, role);

        return BaseResponse.success(null);
    }

}
