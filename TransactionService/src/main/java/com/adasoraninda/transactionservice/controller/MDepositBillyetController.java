package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.MDepositBillyetEndPoint;
import com.adasoraninda.transactionservice.model.request.MDepositBillyetRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.model.response.MDepositBillyetResponse;
import com.adasoraninda.transactionservice.service.MDepositBillyetService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = MDepositBillyetEndPoint.pathBase)
public class MDepositBillyetController {

    private final MDepositBillyetService service;

    @GetMapping
    public BaseResponse<List<MDepositBillyetResponse>> getAllDepositBillyet() {
        return BaseResponse.success(service.getAllDepositBillyet());
    }

    @GetMapping(path = MDepositBillyetEndPoint.pathId)
    public BaseResponse<MDepositBillyetResponse> getDepositBillyetById(
            @PathVariable(value = MDepositBillyetEndPoint.variableId) Long dbId
    ) {
        return BaseResponse.success(service.getDepositBillyetById(dbId));
    }

    @PostMapping
    public BaseResponse<MDepositBillyetResponse> createDepositBillyet(
            @Valid @RequestBody MDepositBillyetRequest dbRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        return BaseResponse.success(service.createDepositBillyet(dbRequest, role));
    }

    @PutMapping(path = MDepositBillyetEndPoint.pathId)
    public BaseResponse<MDepositBillyetResponse> updateDepositBillyetById(
            @PathVariable(value = MDepositBillyetEndPoint.variableId) Long dbId,
            @Valid @RequestBody MDepositBillyetRequest dbRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        return BaseResponse.success(service.updateDepositBillyetById(dbId, dbRequest, role));
    }

    @DeleteMapping(path = MDepositBillyetEndPoint.pathId)
    public BaseResponse<Object> deleteDepositBillyetById(
            @PathVariable(value = MDepositBillyetEndPoint.variableId) Long dbId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        service.deleteDepositBillyetById(dbId, role);

        return BaseResponse.success(null);
    }

}
