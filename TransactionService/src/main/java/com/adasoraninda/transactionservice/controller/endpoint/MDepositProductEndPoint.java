package com.adasoraninda.transactionservice.controller.endpoint;

public final class MDepositProductEndPoint {
    public static final String variableId = "id";
    public static final String variableName = "name";
    public static final String pathBase = "deposit-products";
    public static final String pathId = "/{" + variableId + "}";
    public static final String pathName = "/{" + variableName + "}";
    public static final String pathIdEntity = pathId + "/entity/id";
    public static final String pathNameEntity = pathName + "/entity/name";
}
