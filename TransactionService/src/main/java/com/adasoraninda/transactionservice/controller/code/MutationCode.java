package com.adasoraninda.transactionservice.controller.code;

public enum MutationCode {
    CREDIT("C"), DEBIT("D");

    private final String code;

    MutationCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code;
    }
}
