package com.adasoraninda.transactionservice.controller.endpoint;

public final class TJournalLedgerDetailEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "journal-ledger-details";
    public static final String pathId = "/{" + variableId + "}";
}
