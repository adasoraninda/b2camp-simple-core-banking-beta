package com.adasoraninda.transactionservice.controller.endpoint;

public final class CustomerDepositLiquidationEndPoint {
    public static final String pathBase = "customer-deposit-liquidation";
}
