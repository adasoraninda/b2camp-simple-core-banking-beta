package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.CustomerDepositLiquidationEndPoint;
import com.adasoraninda.transactionservice.model.request.CustomerDepositLiquidationRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.service.CustomerDepositLiquidationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping(CustomerDepositLiquidationEndPoint.pathBase)
public class CustomerDepositLiquidationController {

    private final CustomerDepositLiquidationService service;

    @GetMapping
    public BaseResponse<Map<String, Object>> depositLiquidation(
            @Valid @RequestBody CustomerDepositLiquidationRequest cdlRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role
    ) {
        return BaseResponse.success(service.depositLiquidation(
                cdlRequest,
                role));
    }

}
