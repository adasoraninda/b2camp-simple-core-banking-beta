package com.adasoraninda.transactionservice.controller.endpoint;

public final class MDepositBillyetEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "deposit-billyets";
    public static final String pathId = "/{" + variableId + "}";
}
