package com.adasoraninda.transactionservice.controller.endpoint;

public final class TSavingAccountEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "saving-accounts";
    public static final String pathId = "/{" + variableId + "}";
}
