package com.adasoraninda.transactionservice.controller.endpoint;

public final class TJournalLedgerEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "journal-ledgers";
    public static final String pathId = "/{" + variableId + "}";
}
