package com.adasoraninda.transactionservice.controller.endpoint;

public final class TSavingAccountDetailEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "saving-account-details";
    public static final String pathId = "/{" + variableId + "}";
}
