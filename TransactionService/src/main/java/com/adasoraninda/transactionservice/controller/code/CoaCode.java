package com.adasoraninda.transactionservice.controller.code;

public enum CoaCode {
    KAS_TELLER("0001"),
    TABUNGAN_UMUM("2001"),
    DEPOSITO_TAHUNAN("2002"),
    DEPOSITO_PENDIDIKAN("2003");

    private final String code;

    CoaCode(String code) {
        this.code = code;
    }

    public static CoaCode getCoa(String coaCode) {
        if (coaCode.equalsIgnoreCase(KAS_TELLER.getCode())) {
            return KAS_TELLER;
        }

        if (coaCode.equalsIgnoreCase(TABUNGAN_UMUM.getCode())) {
            return TABUNGAN_UMUM;
        }

        if (coaCode.equalsIgnoreCase(DEPOSITO_TAHUNAN.getCode())) {
            return DEPOSITO_TAHUNAN;
        }

        if (coaCode.equalsIgnoreCase(DEPOSITO_PENDIDIKAN.getCode())) {
            return DEPOSITO_PENDIDIKAN;
        }

        throw new IllegalArgumentException(coaCode + " tidak ada");
    }

    public String getCode() {
        return this.code;
    }
}
