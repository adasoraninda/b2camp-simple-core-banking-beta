package com.adasoraninda.transactionservice.controller.endpoint;

public final class MSavingProductEndPoint {
    public static final String variableId = "id";
    public static final String variableName = "product-name";
    public static final String pathBase = "saving-products";
    public static final String pathId = "/{" + variableId + "}";
    public static final String pathName = "/{" + variableName + "}";
    public static final String pathIdEntity = pathId + "/entity/id";
    public static final String pathNameEntity = pathName + "/entity/name";
}
