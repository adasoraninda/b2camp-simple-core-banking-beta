package com.adasoraninda.transactionservice.controller.endpoint;

public final class TDepositAccountDetailEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "deposit-account-details";
    public static final String pathId = "/{" + variableId + "}";
}
