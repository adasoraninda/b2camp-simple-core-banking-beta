package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.TJournalLedgerEndPoint;
import com.adasoraninda.transactionservice.model.request.TJournalLedgerRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.model.response.TJournalLedgerResponse;
import com.adasoraninda.transactionservice.service.TJournalLedgerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = TJournalLedgerEndPoint.pathBase)
public class TJournalLedgerController {

    private final TJournalLedgerService service;

    @GetMapping
    public BaseResponse<List<TJournalLedgerResponse>> getAllJournalLedger() {
        return BaseResponse.success(service.getAllJournalLedger());
    }

    @GetMapping(path = TJournalLedgerEndPoint.pathId)
    public BaseResponse<TJournalLedgerResponse> getJournalLedgerById(
            @PathVariable(value = TJournalLedgerEndPoint.variableId) Long jlId
    ) {
        return BaseResponse.success(service.getJournalLedgerById(jlId));
    }

    @PostMapping
    public BaseResponse<TJournalLedgerResponse> createJournalLedger(
            @Valid @RequestBody TJournalLedgerRequest jlRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        return BaseResponse.success(service.createJournalLedger(jlRequest, role));
    }

    @PutMapping(path = TJournalLedgerEndPoint.pathId)
    public BaseResponse<TJournalLedgerResponse> updateJournalLedgerById(
            @PathVariable(value = TJournalLedgerEndPoint.variableId) Long jlId,
            @Valid @RequestBody TJournalLedgerRequest jlRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        return BaseResponse.success(service.updateJournalLedgerById(jlId, jlRequest, role));
    }

    @DeleteMapping(path = TJournalLedgerEndPoint.pathId)
    public BaseResponse<Object> deleteJournalLedgerById(
            @PathVariable(value = TJournalLedgerEndPoint.variableId) Long jlId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        service.deleteJournalLedgerById(jlId, role);

        return BaseResponse.success(null);
    }

}
