package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.TDepositAccountEndPoint;
import com.adasoraninda.transactionservice.model.request.TDepositAccountRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.model.response.TDepositAccountResponse;
import com.adasoraninda.transactionservice.service.TDepositAccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = TDepositAccountEndPoint.pathBase)
public class TDepositAccountController {

    private final TDepositAccountService service;

    @GetMapping
    public BaseResponse<List<TDepositAccountResponse>> getAllDepositAccount() {
        return BaseResponse.success(service.getAllDepositAccount());
    }

    @GetMapping(path = TDepositAccountEndPoint.pathId)
    public BaseResponse<TDepositAccountResponse> getDepositAccountById(
            @PathVariable(value = TDepositAccountEndPoint.variableId) Long daId
    ) {
        return BaseResponse.success(service.getDepositAccountById(daId));
    }

    @PostMapping
    public BaseResponse<TDepositAccountResponse> createDepositAccount(
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody TDepositAccountRequest daRequest) {
        return BaseResponse.success(service.createDepositAccount(role, daRequest));
    }

    @PutMapping(path = TDepositAccountEndPoint.pathId)
    public BaseResponse<TDepositAccountResponse> updateDepositAccountById(
            @PathVariable(value = TDepositAccountEndPoint.variableId) Long daId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody TDepositAccountRequest daRequest) {
        return BaseResponse.success(service.updateDepositAccountById(daId, role, daRequest));
    }

    @DeleteMapping(path = TDepositAccountEndPoint.pathId)
    public BaseResponse<Object> deleteDepositAccountById(
            @PathVariable(value = TDepositAccountEndPoint.variableId) Long daId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        service.deleteDepositAccountById(daId, role);

        return BaseResponse.success(null);
    }

}
