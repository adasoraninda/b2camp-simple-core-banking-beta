package com.adasoraninda.transactionservice.controller;

import com.adasoraninda.transactionservice.controller.endpoint.TSavingAccountDetailEndPoint;
import com.adasoraninda.transactionservice.model.request.TSavingAccountDetailRequest;
import com.adasoraninda.transactionservice.model.response.BaseResponse;
import com.adasoraninda.transactionservice.model.response.TSavingAccountDetailResponse;
import com.adasoraninda.transactionservice.service.TSavingAccountDetailService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = TSavingAccountDetailEndPoint.pathBase)
public class TSavingAccountDetailController {

    private final TSavingAccountDetailService service;

    @GetMapping
    public BaseResponse<List<TSavingAccountDetailResponse>> getAllSavingAccountDetail() {
        return BaseResponse.success(service.getAllSavingAccountDetail());
    }

    @GetMapping(path = TSavingAccountDetailEndPoint.pathId)
    public BaseResponse<TSavingAccountDetailResponse> getSavingAccountDetailById(
            @PathVariable(value = TSavingAccountDetailEndPoint.variableId) Long sadId
    ) {
        return BaseResponse.success(service.getSavingAccountDetailById(sadId));
    }

    @PostMapping
    public BaseResponse<TSavingAccountDetailResponse> createSavingDetailAccount(
            @RequestParam(name =
                    AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody TSavingAccountDetailRequest sadRequest
    ) {
        return BaseResponse.success(service.createSavingDetailAccount(role, sadRequest));
    }

    @PutMapping(path = TSavingAccountDetailEndPoint.pathId)
    public BaseResponse<TSavingAccountDetailResponse> updateSavingDetailAccount(
            @PathVariable(value = TSavingAccountDetailEndPoint.variableId) Long sadId,
            @RequestParam(name =
                    AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody TSavingAccountDetailRequest sadRequest
    ) {
        return BaseResponse.success(service.updateSavingDetailAccount(sadId, role, sadRequest));
    }

    @DeleteMapping(path = TSavingAccountDetailEndPoint.pathId)
    public BaseResponse<Void> deleteSavingAccountDetail(
            @PathVariable(value = TSavingAccountDetailEndPoint.variableId) Long sadId,
            @RequestParam(name =
                    AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role
    ) {
        service.deleteSavingAccountDetail(sadId, role);
        return BaseResponse.success(null);
    }

}
