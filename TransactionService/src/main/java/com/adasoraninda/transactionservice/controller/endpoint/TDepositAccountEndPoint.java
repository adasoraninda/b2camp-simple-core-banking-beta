package com.adasoraninda.transactionservice.controller.endpoint;

public final class TDepositAccountEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "deposit-accounts";
    public static final String pathId = "/{" + variableId + "}";
}
