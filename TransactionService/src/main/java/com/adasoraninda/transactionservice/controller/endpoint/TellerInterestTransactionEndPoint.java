package com.adasoraninda.transactionservice.controller.endpoint;

public final class TellerInterestTransactionEndPoint {
    public static final String pathBase = "teller-interest-transaction";
}
