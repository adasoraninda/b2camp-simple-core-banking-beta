package com.adasoraninda.transactionservice.controller.endpoint;

public final class MCifEndPoint {
    public static final String variableId = "id";
    public static final String variableNik = "nik";
    public static final String pathBase = "cif";
    public static final String pathId = "/{" + variableId + "}";
    public static final String pathNik = "/{" + variableNik + "}";
    public static final String pathIdEntity = pathId + "/entity/id";
    public static final String pathNikEntity = pathNik + "/entity/nik";
}
