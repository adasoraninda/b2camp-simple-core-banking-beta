package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_journal_ledger_detail")
@EqualsAndHashCode(callSuper = true)
public class TJournalLedgerDetail extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    private String description;

    // Mutation (CREDIT/DEBIT)
    @Column(nullable = false)
    private String list;

    // TODO: Ini di generate otomatis
    @Column(nullable = false)
    private String reference;

    @Column(nullable = false)
    private BigDecimal nominal;

    @OneToOne
    @JoinColumn(name="header_id", nullable = false)
    private TJournalLedger header;

    @ManyToOne
    @JoinColumn(name = "coa_id", nullable = false)
    private MCoa coa;

    @ManyToOne
    @JoinColumn(name = "transaction_code", nullable = false)
    private MTransactionCode transactionCode;

    public void create(
            String description,
            String list,
            String reference,
            BigDecimal nominal,
            TJournalLedger header,
            MCoa coa,
            MTransactionCode transactionCode,
            String role) {
        super.create(role);
        this.description = description;
        this.list = list;
        this.reference = reference;
        this.nominal = nominal;
        this.header = header;
        this.coa = coa;
        this.transactionCode = transactionCode;
    }

    public void update(
            String description,
            String list,
            String reference,
            BigDecimal nominal,
            TJournalLedger header,
            MCoa coa,
            MTransactionCode transactionCode,
            String role) {
        super.update(role);
        this.description = description;
        this.list = list;
        this.reference = reference;
        this.nominal = nominal;
        this.header = header;
        this.coa = coa;
        this.transactionCode = transactionCode;
    }

}
