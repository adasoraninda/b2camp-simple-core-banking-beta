package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_saving_account")
@EqualsAndHashCode(callSuper = true)
public class TSavingAccount extends BaseEntity {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String accountNumber;

    @Column(nullable = false)
    private BigDecimal balance;

    @ManyToOne
    @JoinColumn(name = "m_cif_id", nullable = false)
    private MCif cif;

    @ManyToOne
    @JoinColumn(name = "m_saving_product_id", nullable = false)
    private MSavingProduct savingProduct;

    public void create(MCif cif, MSavingProduct savingProduct, String role) {
        super.create(role);
        this.cif = cif;
        this.savingProduct = savingProduct;
    }

    public void update(String accountNumber, BigDecimal balance, MCif cif, MSavingProduct savingProduct, String role) {
        super.update(role);
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.cif = cif;
        this.savingProduct = savingProduct;
    }

}
