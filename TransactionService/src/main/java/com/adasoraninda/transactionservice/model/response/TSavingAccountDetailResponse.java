package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TSavingAccountDetailResponse {
    private Long id;
    private String mutation;
    private BigDecimal transactionAmount;
    private TSavingAccountResponse savingAccount;
    private MTransactionCodeResponse transactionCode;
}
