package com.adasoraninda.transactionservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class TJournalLedgerRequest {
    @NotNull(message = "Deskripsi harus di isi")
    @NotEmpty(message = "Descripsi tidak boleh kosong")
    @NotBlank(message = "Deskripsi tidak boleh hanya spasi")
    private String description;

    @NotNull(message = "Referensi harus di isi")
    @NotEmpty(message = "Referensi tidak boleh kosong")
    @NotBlank(message = "Referensi tidak boleh hanya spasi")
    private String reference;

    // Untuk mendapatkan MTransactionCode
    @NotNull(message = "Kode transaksi harus di isi")
    private Integer code;

    @NotNull(message = "Jumlah nominal harus di isi")
    private BigDecimal nominal;
}
