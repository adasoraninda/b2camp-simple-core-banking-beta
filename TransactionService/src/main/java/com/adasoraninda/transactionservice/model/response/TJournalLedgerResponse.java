package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TJournalLedgerResponse {
    private Long id;
    private String description;
    private String reference;
    private MTransactionCodeResponse transactionCode;
    private BigDecimal nominal;
}
