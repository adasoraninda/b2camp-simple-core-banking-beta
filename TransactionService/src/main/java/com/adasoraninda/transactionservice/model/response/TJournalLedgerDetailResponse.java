package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TJournalLedgerDetailResponse {
    private Long id;
    private String description;
    private String list;
    private String reference;
    private BigDecimal nominal;
    private Long headerId;
    private MCoaResponse coa;
    private MTransactionCodeResponse transactionCode;
}
