package com.adasoraninda.transactionservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
public class TSavingAccountRequest {

    @NotNull(message = "Nomor rekening harus di isi")
    @NotEmpty(message = "Nomor rekening tidak boleh kosong")
    @NotBlank(message = "Nomor rekening tidak boleh hanya spasi")
    private String accountNumber;

    @NotNull(message = "Jumlah saldo harus di isi")
    private BigDecimal balance;

    @NotNull(message = "NIK harus di isi")
    @NotEmpty(message = "NIK tidak boleh kosong")
    @NotBlank(message = "NIK tidak boleh hanya karakter spasi")
    @Size(max = 20, message = "NIK tidak boleh lebih dari 20 karakter")
    private String nik;

    @NotNull(message = "Nama produk harus di isi")
    @NotEmpty(message = "Nama produk tidak boleh kosong")
    @NotBlank(message = "Nama produk tidak boleh hanya spasi")
    private String productName;

}
