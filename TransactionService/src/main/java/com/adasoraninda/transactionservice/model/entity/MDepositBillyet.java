package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_deposit_billyet")
@EqualsAndHashCode(callSuper = true)
public class MDepositBillyet extends BaseEntity {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique=true)
    private String billyetNumber;

    @OneToOne
    @JoinColumn(name = "t_deposit_account_id", nullable = false)
    private TDepositAccount depositAccount;

    public void create(String billyetNumber, TDepositAccount depositAccount, String role) {
        super.create(role);
        this.billyetNumber = billyetNumber;
        this.depositAccount = depositAccount;
    }

    public void update(String billyetNumber, TDepositAccount depositAccount, String role) {
        super.update(role);
        this.billyetNumber = billyetNumber;
        this.depositAccount = depositAccount;
    }

}
