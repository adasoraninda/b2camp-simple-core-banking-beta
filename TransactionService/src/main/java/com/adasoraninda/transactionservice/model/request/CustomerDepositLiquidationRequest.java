package com.adasoraninda.transactionservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CustomerDepositLiquidationRequest {

    // Untuk mendapatkan MDepositBillyet
    @NotNull(message = "Nomor billyet harus di isi")
    @NotEmpty(message = "Nomor billyet tidak boleh kosong")
    @NotBlank(message = "Nomor billyet tidak boleh hanya spasi")
    private String billyetNumber;

    // Untuk mendapatkan TDepositAccount
    @NotNull(message = "Nomor akun deposito harus di isi")
    @NotEmpty(message = "Nomor akun deposito tidak boleh kosong")
    @NotBlank(message = "Nomor akun deposito tidak boleh hanya spasi")
    private String depositAccountNumber;

    // Untuk mendapatkan TSavingAccount
    @NotNull(message = "Nomor akun tabungan harus di isi")
    @NotEmpty(message = "Nomor akun tabungan tidak boleh kosong")
    @NotBlank(message = "Nomor akun tabungan tidak boleh hanya spasi")
    private String savingAccountNumber;

}
