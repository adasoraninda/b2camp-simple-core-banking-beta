package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

@Data
public class MDepositProductResponse {
    private Long id;
    private String name;
    private Double interestRate;
    private String code;
    private MCoaResponse coa;
}
