package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TDepositAccountDetailResponse {
    private Long id;
    private String mutation;
    private BigDecimal transactionAmount;
    private TDepositAccountResponse depositAccount;
    private MTransactionCodeResponse transactionCode;
}
