package com.adasoraninda.transactionservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class TDepositAccountDetailRequest {

    /**
     * Isi variabel lihat
     *
     * @see com.adasoraninda.transactionservice.controller.code.MutationCode
     */
    @NotNull(message = "Mutasi harus di isi")
    @NotEmpty(message = "Mutasi tidak boleh kosong")
    @NotBlank(message = "Mutasi tidak boleh hanya spasi")
    private String mutation;

    @NotNull(message = "Jumlah uang transaksi harus di isi")
    private BigDecimal transactionAmount;

    // Untuk mendapatkan akun deposit (deposit account)
    @NotNull(message = "Nomor rekening akun deposito harus di isi")
    @NotEmpty(message = "Nomor rekening akun deposito tidak boleh kosong")
    @NotBlank(message = "Nomor rekening akun deposito tidak boleh hanya spasi")
    private String depositAccountNumber;

    // Untuk mendapatkan akun tabungan (saving account)
    @NotNull(message = "Nomor rekening akun tabungan harus di isi")
    @NotEmpty(message = "Nomor rekening akun tabungan tidak boleh kosong")
    @NotBlank(message = "Nomor rekening akun tabungan tidak boleh hanya spasi")
    private String savingAccountNumber;
    /**
     * Isi dari variabel lihat
     *
     * @see com.adasoraninda.transactionservice.controller.code.TransactionCode
     * Untuk mendapatkan data transaction (code MTransactionCode)
     */
    @NotNull(message = "Kode transaksi harus di isi")
    private Integer transactionCode;

}
