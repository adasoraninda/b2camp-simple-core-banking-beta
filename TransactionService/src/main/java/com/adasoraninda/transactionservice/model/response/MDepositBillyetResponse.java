package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

@Data
public class MDepositBillyetResponse {
    private Long id;
    private String billyetNumber;
    private TDepositAccountResponse depositAccount;
}
