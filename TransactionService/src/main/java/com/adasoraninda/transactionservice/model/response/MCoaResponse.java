package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

@Data
public class MCoaResponse {
    private Long id;
    private String coaCode;
    private String name;
    private String description;
}
