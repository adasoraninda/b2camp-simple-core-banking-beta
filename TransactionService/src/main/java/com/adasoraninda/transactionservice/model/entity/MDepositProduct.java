package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_deposit_product")
@EqualsAndHashCode(callSuper = true)
public class MDepositProduct extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false, unique=true)
    private String name;

    @Column(nullable = false)
    private Double interestRate;

    @Column(nullable = false, unique=true)
    private String code;

    @ManyToOne
    @JoinColumn(name = "coa_id", nullable = false)
    private MCoa coa;

}
