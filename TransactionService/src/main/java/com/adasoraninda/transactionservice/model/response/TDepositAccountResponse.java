package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TDepositAccountResponse {
    private Long id;
    private BigDecimal nominal;
    private String accountNumber;
    private Integer timePeriod;
    private TSavingAccountResponse savingAccount;
    private MCifResponse cif;
    private MDepositProductResponse depositProduct;
    private RStatusResponse status;
    private String startDate;
    private String dueDate;
}
