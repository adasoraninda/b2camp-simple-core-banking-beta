package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TSavingAccountResponse {
    private Long id;
    private String accountNumber;
    private BigDecimal balance;
    private MCifResponse cif;
    private MSavingProductResponse savingProduct;
}
