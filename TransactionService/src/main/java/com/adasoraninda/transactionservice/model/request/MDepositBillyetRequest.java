package com.adasoraninda.transactionservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MDepositBillyetRequest {

    @NotNull(message = "Nomor billyet harus di isi")
    @NotEmpty(message = "Nomor billyet tidak boleh kosong")
    @NotBlank(message = "Nomor billyet tidak boleh hanya spasi")
    private String billyetNumber;

    // Untuk mendapatkan akun deposito
    @NotNull(message = "Nomor akun deposito harus di isi")
    @NotEmpty(message = "Nomor akun deposito tidak boleh kosong")
    @NotBlank(message = "Nomor akun deposito tidak boleh hanya spasi")
    private String depositAccountNumber;
}
