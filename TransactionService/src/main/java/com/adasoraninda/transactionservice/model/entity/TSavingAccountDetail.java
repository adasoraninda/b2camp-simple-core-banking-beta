package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "t_saving_account_detail")
public class TSavingAccountDetail extends BaseEntity {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String mutation;

    @Column(nullable = false)
    private BigDecimal transactionAmount;

    @ManyToOne
    @JoinColumn(name = "t_saving_account_id", nullable = false)
    private TSavingAccount savingAccount;

    @ManyToOne
    @JoinColumn(name = "t_transaction_code", nullable = false)
    private MTransactionCode transactionCode;

    public void create(
            String mutation,
            BigDecimal transactionAmount,
            TSavingAccount savingAccount,
            MTransactionCode transactionCode,
            String role) {
        super.create(role);
        this.mutation = mutation;
        this.transactionAmount = transactionAmount;
        this.savingAccount = savingAccount;
        this.transactionCode = transactionCode;
    }

    public void update(
            String mutation,
            BigDecimal transactionAmount,
            TSavingAccount savingAccount,
            MTransactionCode transactionCode,
            String role) {
        super.update(role);
        this.mutation = mutation;
        this.transactionAmount = transactionAmount;
        this.savingAccount = savingAccount;
        this.transactionCode = transactionCode;
    }
}
