package com.adasoraninda.transactionservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TellerInterestTransactionRequest {

    // Untuk mendapatkan TDepositAccount
    @NotNull(message = "Nomor akun deposito harus di isi")
    @NotEmpty(message = "Nomor akun deposito tidak boleh kosong")
    @NotBlank(message = "Nomor akun deposito tidak boleh hanya spasi")
    private String depositAccountNumber;

}
