package com.adasoraninda.transactionservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class TJournalLedgerDetailRequest {

    @NotNull(message = "Deskripsi harus di isi")
    @NotEmpty(message = "Descripsi tidak boleh kosong")
    @NotBlank(message = "Deskripsi tidak boleh hanya spasi")
    private String description;

    // Mutation
    @NotNull(message = "List harus di isi")
    @NotEmpty(message = "List tidak boleh kosong")
    @NotBlank(message = "List tidak boleh hanya spasi")
    private String list;

    @NotNull(message = "Referensi harus di isi")
    @NotEmpty(message = "Referensi tidak boleh kosong")
    @NotBlank(message = "Referensi tidak boleh hanya spasi")
    private String reference;

    @NotNull(message = "Jumlah nominal harus di isi")
    private BigDecimal nominal;

    // Untuk mendapatkan Journal Ledger
    @NotNull(message = "Id header harus di isi")
    private Long headerId;

    // Untuk mendapatkan data MCoa
    @NotNull(message = "Kode COA harus di isi")
    @NotEmpty(message = "Kode COA tidak boleh kosong")
    @NotBlank(message = "Kode COA tidak boleh hanya spasi")
    private String coaCode;

    // Untuk mendapatkan data MTransactionCode
    @NotNull(message = "Kode transaksi harus di isi")
    private Integer code;
}
