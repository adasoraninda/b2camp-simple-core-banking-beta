package com.adasoraninda.transactionservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class TSavingAccountDetailRequest {

    /**
     * Isi variabel bisa dilihat di
     *
     * @see com.adasoraninda.transactionservice.controller.code.TransactionCode
     */
    @NotNull(message = "Kode transaksi harus di isi")
    private Integer transactionCode;

    @NotNull(message = "Jumlah uang transaksi harus di isi")
    private BigDecimal transactionAmount;

    @NotNull(message = "Nomor rekening harus di isi")
    @NotEmpty(message = "Nomor rekening tidak boleh kosong")
    @NotBlank(message = "Nomor rekening tidak boleh hanya spasi")
    private String accountNumber;

    @NotNull(message = "Nomor rekening tujuan harus di isi")
    @NotEmpty(message = "Nomor rekening tujuan tidak boleh kosong")
    @NotBlank(message = "Nomor rekening tujuan tidak boleh hanya spasi")
    private String targetAccountNumber;

}
