package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_deposit_account")
@EqualsAndHashCode(callSuper = true)
public class TDepositAccount extends BaseEntity {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private BigDecimal nominal;

    @Column(nullable = false, unique=true)
    private String accountNumber;

    @Column(nullable = false)
    private Integer timePeriod;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate dueDate;

    @OneToOne
    @JoinColumn(name = "saving_id", nullable = false)
    private TSavingAccount savingAccount;

    @ManyToOne
    @JoinColumn(name = "cif_id", nullable = false)
    private MCif cif;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private MDepositProduct depositProduct;

    @ManyToOne
    @JoinColumn(name = "status_id", nullable = false)
    private RStatus status;

    public void create(
            BigDecimal nominal,
            String accountNumber,
            Integer timePeriod,
            LocalDate startDate,
            LocalDate dueDate,
            TSavingAccount savingAccount,
            MCif cif,
            MDepositProduct depositProduct,
            RStatus status,
            String role) {
        super.create(role);
        this.nominal = nominal;
        this.accountNumber = accountNumber;
        this.timePeriod = timePeriod;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.savingAccount = savingAccount;
        this.cif = cif;
        this.depositProduct = depositProduct;
        this.status = status;
    }

    public void update(
            BigDecimal nominal,
            String accountNumber,
            Integer timePeriod,
            LocalDate startDate,
            LocalDate dueDate,
            TSavingAccount savingAccount,
            MCif cif,
            MDepositProduct depositProduct,
            RStatus status,
            String role) {
        super.update(role);
        this.nominal = nominal;
        this.accountNumber = accountNumber;
        this.timePeriod = timePeriod;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.savingAccount = savingAccount;
        this.cif = cif;
        this.depositProduct = depositProduct;
        this.status = status;
    }

}
