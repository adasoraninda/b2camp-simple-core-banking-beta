package com.adasoraninda.transactionservice.model.request;

import com.adasoraninda.transactionservice.controller.code.StatusCode;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class TDepositAccountRequest {

    private BigDecimal nominal = BigDecimal.ZERO;

    @NotNull(message = "Nomor rekening depsito harus di isi")
    @NotEmpty(message = "Nomor rekening depsito tidak boleh kosong")
    @NotBlank(message = "Nomor rekening depsito tidak boleh hanya spasi")
    private String accountNumber;

    @NotNull(message = "Jangka waktu harus di isi")
    private Integer timePeriod;

    @NotNull(message = "Tanggal mulai harus di isi")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDate startDate;

    @NotNull(message = "Tanggal jatuh tempo harus di isi")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDate dueDate;

    // Untuk mendapatkan data tabungan (saving account)
    @NotNull(message = "Nomor rekening tabungan harus di isi")
    @NotEmpty(message = "Nomor rekening tabungan tidak boleh kosong")
    @NotBlank(message = "Nomor rekening tabungan tidak boleh hanya spasi")
    private String savingAccountNumber;

    // Untuk mendapatkan CIF
    @NotNull(message = "NIK harus di isi")
    @NotEmpty(message = "NIK tidak boleh kosong")
    @NotBlank(message = "NIK tidak boleh hanya spasi")
    @Size(max = 20, message = "NIK tidak boleh lebih dari 20 karakter")
    private String nik;

    // Untuk mendapatkan data deposit product
    @NotNull(message = "Nama produk deposito harus di isi")
    @NotEmpty(message = "Nama produk tidak boleh kosong")
    @NotBlank(message = "Nama produk tidak boleh hanya spasi")
    private String depositProductName;

    // Untuk mendapatkan data status (ACTIVE/INACTIVE)
    // @NotNull(message = "Kode status harus di isi")
    // @NotEmpty(message = "Kode status tidak boleh kosong")
    // @NotBlank(message = "Kode status tidak boleh hanya spasi")
    private String statusCode = StatusCode.ACTIVE.name();

}
