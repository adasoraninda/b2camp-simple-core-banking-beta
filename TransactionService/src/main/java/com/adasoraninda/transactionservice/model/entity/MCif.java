package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_cif")
@EqualsAndHashCode(callSuper = true)
public class MCif extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(name = "id_ktp", length = 20, nullable = false, unique = true)
    private String idKtp;

    @Column(nullable = false)
    private String name;

    @Column(unique = true)
    private String npwp;

    @Column(name = "no_telephone", length = 15, nullable = false, unique = true)
    private String noTelepon;

    @Column(unique = true)
    private String email;

    @Column(nullable = false)
    private String type;

}

