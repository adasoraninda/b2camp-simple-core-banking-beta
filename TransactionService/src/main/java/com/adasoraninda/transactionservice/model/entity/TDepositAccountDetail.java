package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_deposit_account_detail")
@EqualsAndHashCode(callSuper = true)
public class TDepositAccountDetail extends BaseEntity {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String mutation;

    @Column(nullable = false)
    private BigDecimal transactionAmount;

    @ManyToOne
    @JoinColumn(name = "deposit_account_id", nullable = false)
    private TDepositAccount depositAccount;

    @ManyToOne
    @JoinColumn(name = "transaction_code_id", nullable = false)
    private MTransactionCode transactionCode;

    public void create(
            String mutation,
            BigDecimal transactionAmount,
            TDepositAccount depositAccount,
            MTransactionCode transactionCode,
            String role) {
        super.create(role);
        this.mutation = mutation;
        this.transactionAmount = transactionAmount;
        this.depositAccount = depositAccount;
        this.transactionCode = transactionCode;
    }

    public void update(
            String mutation,
            BigDecimal transactionAmount,
            TDepositAccount depositAccount,
            MTransactionCode transactionCode,
            String role) {
        super.update(role);
        this.mutation = mutation;
        this.transactionAmount = transactionAmount;
        this.depositAccount = depositAccount;
        this.transactionCode = transactionCode;
    }

}
