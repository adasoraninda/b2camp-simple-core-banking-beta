package com.adasoraninda.transactionservice.model.response;

import lombok.Data;

@Data
public class RStatusResponse {
    private Long id;
    private String code;
    private String name;
}
