package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_coa")
@EqualsAndHashCode(callSuper = true)
public class MCoa extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false, unique=true)
    private String coaCode;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

}