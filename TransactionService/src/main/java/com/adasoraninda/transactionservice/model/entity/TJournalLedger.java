package com.adasoraninda.transactionservice.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_journal_ledger")
@EqualsAndHashCode(callSuper = true)
public class TJournalLedger extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    private String description;

    // TODO: Ini di generate otomatis
    @Column(nullable = false)
    private String reference;

    @ManyToOne
    @JoinColumn(name = "transaction_code", nullable = false)
    private MTransactionCode transactionCode;

    @Column(nullable = false)
    private BigDecimal nominal;

    public void create(
            String description,
            String reference,
            MTransactionCode transactionCode,
            BigDecimal nominal,
            String role) {
        super.create(role);
        this.description = description;
        this.reference = reference;
        this.transactionCode = transactionCode;
        this.nominal = nominal;
    }

    public void update(
            String description,
            String reference,
            MTransactionCode transactionCode,
            BigDecimal nominal,
            String role) {
        super.update(role);
        this.description = description;
        this.reference = reference;
        this.transactionCode = transactionCode;
        this.nominal = nominal;
    }

}
