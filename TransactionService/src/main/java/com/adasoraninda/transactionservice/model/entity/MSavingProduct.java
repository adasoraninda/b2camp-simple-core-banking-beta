package com.adasoraninda.transactionservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_saving_product")
@EqualsAndHashCode(callSuper = true)
public class MSavingProduct extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(nullable = false, unique=true)
    private String productName;

    @Column(nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "coa_id", nullable = false)
    private MCoa coa;

}
