package com.adasoraninda.transactionservice.handle.exception.code;

public enum TDepositAccountExceptionCode {
    DEPOSIT_ACCOUNT_IS_NOT_FOUND,
    DEPOSIT_ACCOUNT_IS_EMPTY
}
