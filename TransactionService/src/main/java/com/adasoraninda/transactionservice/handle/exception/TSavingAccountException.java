package com.adasoraninda.transactionservice.handle.exception;

import com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountExceptionCode;

public class TSavingAccountException extends RuntimeException {

    private final TSavingAccountExceptionCode code;

    public TSavingAccountException(TSavingAccountExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }
}
