package com.adasoraninda.transactionservice.handle;

import java.lang.reflect.Field;
import java.util.function.Supplier;

public class SaveHandleEntity {

    public static <T> T handle(Supplier<T> response, String errorMessage) {
        try {
            Field field = response.get().getClass().getDeclaredField("id");
            field.setAccessible(true);
            Object value = field.get(response.get());

            if (value == null) {
                throw new NullPointerException(errorMessage);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new NullPointerException(errorMessage);
        }

        return response.get();
    }

}
