package com.adasoraninda.transactionservice.handle.exception;

public class SqlErrorMessage {

    public static String getMessage(String key) {
        if (key.equalsIgnoreCase("id_ktp")) {
            return "KTP sudah terdaftar";
        } else if (key.equalsIgnoreCase("npwp")) {
            return "NPWP sudah terdaftar";
        } else if (key.equalsIgnoreCase("no_telephone")) {
            return "No telepon sudah terdaftar";
        } else if (key.equalsIgnoreCase("email")) {
            return "Email sudah terdaftar";
        } else if (key.equalsIgnoreCase("coa_code")) {
            return "Kode COA sudah terdaftar";
        } else if (key.equalsIgnoreCase("name")) {
            return "Nama sudah terdaftar";
        } else if (key.equalsIgnoreCase("code")) {
            return "Kode sudah terdaftar";
        } else if (key.equalsIgnoreCase("product_name")) {
            return "Nama produk sudah terdaftar";
        } else if (key.equalsIgnoreCase("transaction_code")) {
            return "Kode transaksi sudah terdaftar";
        } else if (key.equalsIgnoreCase("transaction_name")) {
            return "Nama transaksi sudah terdaftar";
        } else if(key.equalsIgnoreCase("billyet_number")){
            return "Nomor billyet sudah terdaftar";
        } else if(key.equalsIgnoreCase("account_number")){
            return "Nomor akun sudah terdaftar";
        }else {
            throw new AppCodeException(key);
        }
    }

}
