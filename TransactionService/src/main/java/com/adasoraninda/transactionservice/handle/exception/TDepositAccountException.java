package com.adasoraninda.transactionservice.handle.exception;

import com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountExceptionCode;

public class TDepositAccountException extends RuntimeException {

    private final TDepositAccountExceptionCode code;

    public TDepositAccountException(TDepositAccountExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }
}
