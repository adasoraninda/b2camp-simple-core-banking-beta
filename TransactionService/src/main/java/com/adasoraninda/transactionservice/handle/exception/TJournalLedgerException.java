package com.adasoraninda.transactionservice.handle.exception;

import com.adasoraninda.transactionservice.handle.exception.code.TJournalLedgerExceptionCode;

public class TJournalLedgerException extends RuntimeException {

    private final TJournalLedgerExceptionCode code;

    public TJournalLedgerException(TJournalLedgerExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }
}
