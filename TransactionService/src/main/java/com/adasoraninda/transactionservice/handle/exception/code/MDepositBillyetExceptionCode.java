package com.adasoraninda.transactionservice.handle.exception.code;

public enum MDepositBillyetExceptionCode {
    DEPOSIT_BILLYET_IS_NOT_EMPTY,
    DEPOSIT_BILLYET_IS_NOT_FOUND
}
