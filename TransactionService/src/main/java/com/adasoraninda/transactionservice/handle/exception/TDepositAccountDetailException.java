package com.adasoraninda.transactionservice.handle.exception;

import com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountDetailExceptionCode;

public class TDepositAccountDetailException extends RuntimeException {

    private final TDepositAccountDetailExceptionCode code;

    public TDepositAccountDetailException(TDepositAccountDetailExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }

}
