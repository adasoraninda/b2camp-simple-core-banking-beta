package com.adasoraninda.transactionservice.handle.exception;

import com.adasoraninda.transactionservice.handle.exception.code.BalanceExceptionCode;

public class BalanceException extends RuntimeException {

    private final BalanceExceptionCode code;

    public BalanceException(BalanceExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }

}
