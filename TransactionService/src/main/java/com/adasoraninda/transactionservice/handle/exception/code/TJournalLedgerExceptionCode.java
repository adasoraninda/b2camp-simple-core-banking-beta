package com.adasoraninda.transactionservice.handle.exception.code;

public enum TJournalLedgerExceptionCode {
    JOURNAL_LEDGER_IS_NOT_FOUND,
    JOURNAL_LEDGER_IS_EMPTY,
}
