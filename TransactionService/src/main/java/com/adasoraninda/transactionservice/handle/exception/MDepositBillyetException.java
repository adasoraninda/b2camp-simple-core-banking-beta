package com.adasoraninda.transactionservice.handle.exception;

import com.adasoraninda.transactionservice.handle.exception.code.MDepositBillyetExceptionCode;

public class MDepositBillyetException extends RuntimeException {

    private final MDepositBillyetExceptionCode code;

    public MDepositBillyetException(MDepositBillyetExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }

}
