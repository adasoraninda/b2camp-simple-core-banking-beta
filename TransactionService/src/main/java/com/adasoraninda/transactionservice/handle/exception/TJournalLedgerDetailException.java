package com.adasoraninda.transactionservice.handle.exception;

import com.adasoraninda.transactionservice.handle.exception.code.TJournalLedgerDetailExceptionCode;

public class TJournalLedgerDetailException extends RuntimeException {

    private final TJournalLedgerDetailExceptionCode code;

    public TJournalLedgerDetailException(TJournalLedgerDetailExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }

}
