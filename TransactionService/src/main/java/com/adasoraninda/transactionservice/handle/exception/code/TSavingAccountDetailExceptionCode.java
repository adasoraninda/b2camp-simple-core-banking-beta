package com.adasoraninda.transactionservice.handle.exception.code;

public enum TSavingAccountDetailExceptionCode {
    SAVING_ACCOUNT_DETAIL_IS_NOT_FOUND,
    SAVING_ACCOUNT_DETAIL_IS_EMPTY,
    SAVING_ACCOUNT_DETAIL_ACC_NUMBER_NOT_VALID;
}
