package com.adasoraninda.transactionservice.handle.exception.code;

public enum TSavingAccountExceptionCode {
    SAVING_ACCOUNT_IS_NOT_FOUND,
    SAVING_ACCOUNT_IS_EMPTY
}
