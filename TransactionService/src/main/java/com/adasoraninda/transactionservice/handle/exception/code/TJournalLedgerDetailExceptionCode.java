package com.adasoraninda.transactionservice.handle.exception.code;

public enum TJournalLedgerDetailExceptionCode {
    JOURNAL_LEDGER_DETAIL_IS_NOT_FOUND,
    JOURNAL_LEDGER_DETAIL_IS_EMPTY,
}
