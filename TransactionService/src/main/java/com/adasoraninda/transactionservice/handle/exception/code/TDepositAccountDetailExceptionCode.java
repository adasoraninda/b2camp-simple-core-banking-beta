package com.adasoraninda.transactionservice.handle.exception.code;

public enum TDepositAccountDetailExceptionCode {
    DEPOSIT_ACCOUNT_DETAIL_IS_NOT_FOUND,
    DEPOSIT_ACCOUNT_DETAIL_IS_EMPTY,
}
