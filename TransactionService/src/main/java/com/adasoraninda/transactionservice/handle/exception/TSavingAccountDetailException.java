package com.adasoraninda.transactionservice.handle.exception;

import com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountDetailExceptionCode;

public class TSavingAccountDetailException extends RuntimeException {

    private final TSavingAccountDetailExceptionCode code;

    public TSavingAccountDetailException(TSavingAccountDetailExceptionCode code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code.name();
    }
}
