package com.adasoraninda.transactionservice.service.impl.deposit;

import com.adasoraninda.transactionservice.controller.code.MutationCode;
import com.adasoraninda.transactionservice.controller.code.TransactionCode;
import com.adasoraninda.transactionservice.handle.exception.AppCodeException;
import com.adasoraninda.transactionservice.handle.exception.TJournalLedgerException;
import com.adasoraninda.transactionservice.model.entity.*;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.adasoraninda.transactionservice.handle.exception.code.TJournalLedgerExceptionCode.JOURNAL_LEDGER_IS_EMPTY;

public class DepositJournal {

    public static class Execute {
        private final TDepositAccount depositAccount;
        private final TSavingAccount savingAccount;
        private final MTransactionCode transactionCode;
        private final BigDecimal amount;
        private final String role;
        private final String reference;
        private TJournalLedger journalLedger;

        public Execute(
                MTransactionCode transactionCode,
                BigDecimal amount,
                String role,
                String reference,
                TDepositAccount depositAccount,
                TSavingAccount savingAccount) {
            this.transactionCode = transactionCode;
            this.amount = amount;
            this.role = role;
            this.reference = reference;
            this.depositAccount = depositAccount;
            this.savingAccount = savingAccount;
        }

        public Execute createJournalLedger(
                MCoa coa, Function<TJournalLedger, TJournalLedger> result) {
            var journalLedger = new TJournalLedger();
            journalLedger.create(
                    getDescription(coa),
                    reference,
                    transactionCode,
                    amount,
                    role);
            this.journalLedger = result.apply(journalLedger);
            return this;
        }

        public Execute createJournalLedgerDetail(
                MCoa coa,
                MutationCode mutation,
                Consumer<TJournalLedgerDetail> result) {
            var journalLedgerDetail = new TJournalLedgerDetail();

            if (journalLedger == null) {
                throw new TJournalLedgerException(
                        JOURNAL_LEDGER_IS_EMPTY,
                        "Journal ledger bernilai null");
            }

            journalLedgerDetail.create(
                    getDetailDescription(coa, mutation),
                    mutation.name(),
                    journalLedger.getReference(),
                    amount,
                    journalLedger,
                    coa,
                    transactionCode,
                    role);

            result.accept(journalLedgerDetail);
            return this;
        }

        private String getDescription(MCoa coa) {
            var descriptionBuilder = new StringBuilder();

            if (TransactionCode.valueOf(transactionCode.getTransactionCode())
                    == TransactionCode.OVERBOOK) {
                return descriptionBuilder
                        .append(savingAccount.getAccountNumber())
                        .append(" transfer ke ")
                        .append(depositAccount.getAccountNumber())
                        .toString();
            }

            if (TransactionCode.valueOf(transactionCode.getTransactionCode())
                    == TransactionCode.SETOR_TUNAI) {
                return descriptionBuilder
                        .append(savingAccount.getAccountNumber())
                        .append(" transfer ke ")
                        .append(coa.getName())
                        .toString();
            }

            if (TransactionCode.valueOf(transactionCode.getTransactionCode())
                    == TransactionCode.TARIK_TUNAI) {
                return descriptionBuilder
                        .append(depositAccount.getAccountNumber())
                        .append(" transfer ke ")
                        .append(savingAccount.getAccountNumber())
                        .toString();
            }

            throw new AppCodeException(transactionCode.getTransactionCode().toString());
        }

        private String getDetailDescription(MCoa coa, MutationCode mutation) {
            var descriptionBuilder = new StringBuilder();

            if (TransactionCode.valueOf(transactionCode.getTransactionCode())
                    == TransactionCode.OVERBOOK) {
                if (mutation == MutationCode.CREDIT) {
                    return descriptionBuilder
                            .append(depositAccount.getAccountNumber())
                            .append(" - ")
                            .append(depositAccount.getCif().getName())
                            .toString();
                }

                if (mutation == MutationCode.DEBIT) {
                    return descriptionBuilder
                            .append(savingAccount.getAccountNumber())
                            .append(" - ")
                            .append(savingAccount.getCif().getName())
                            .toString();
                }
            }

            if (TransactionCode.valueOf(transactionCode.getTransactionCode())
                    == TransactionCode.SETOR_TUNAI) {
                if (mutation == MutationCode.CREDIT) {
                    return descriptionBuilder
                            .append(depositAccount.getAccountNumber())
                            .append(" - ")
                            .append(depositAccount.getCif().getName())
                            .toString();
                }

                if (mutation == MutationCode.DEBIT) {
                    return descriptionBuilder
                            .append(coa.getName())
                            .toString();
                }
            }

            if (TransactionCode.valueOf(transactionCode.getTransactionCode())
                    == TransactionCode.TARIK_TUNAI) {
                if (mutation == MutationCode.DEBIT) {
                    return descriptionBuilder
                            .append(depositAccount.getAccountNumber())
                            .append(" - ")
                            .append(depositAccount.getCif().getName())
                            .toString();
                }

                if (mutation == MutationCode.CREDIT) {
                    return descriptionBuilder
                            .append(coa.getName())
                            .toString();
                }
            }

            throw new AppCodeException(transactionCode.getTransactionCode().toString());
        }

    }

}
