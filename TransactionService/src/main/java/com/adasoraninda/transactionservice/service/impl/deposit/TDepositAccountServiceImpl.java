package com.adasoraninda.transactionservice.service.impl.deposit;

import static com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountExceptionCode.DEPOSIT_ACCOUNT_IS_EMPTY;
import static com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountExceptionCode.DEPOSIT_ACCOUNT_IS_NOT_FOUND;

import com.adasoraninda.transactionservice.controller.client.ParameterFeignClient;
import com.adasoraninda.transactionservice.handle.SaveHandleEntity;
import com.adasoraninda.transactionservice.handle.exception.TDepositAccountException;
import com.adasoraninda.transactionservice.handle.exception.TSavingAccountException;
import com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountExceptionCode;
import com.adasoraninda.transactionservice.model.entity.TDepositAccount;
import com.adasoraninda.transactionservice.model.request.TDepositAccountRequest;
import com.adasoraninda.transactionservice.model.response.TDepositAccountResponse;
import com.adasoraninda.transactionservice.repository.TDepositAccountRepository;
import com.adasoraninda.transactionservice.repository.TSavingAccountRepository;
import com.adasoraninda.transactionservice.service.TDepositAccountService;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TDepositAccountServiceImpl implements TDepositAccountService {

    private ParameterFeignClient paramClient;
    private TDepositAccountRepository daRepository;
    private TSavingAccountRepository saRepository;
    private final ModelMapper mapper;

    @Override
    public List<TDepositAccountResponse> getAllDepositAccount() {
        var allDeposit = daRepository.findAllDepositAccount();

        if (allDeposit.isEmpty()) {
            throw new TDepositAccountException(
                    DEPOSIT_ACCOUNT_IS_EMPTY,
                    "Semua data akun deposito tidak ada");
        }

        return allDeposit.stream()
                .map(da -> mapper.map(da, TDepositAccountResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public TDepositAccountResponse getDepositAccountById(Long daId) {
        return daRepository.findDepositAccountById(daId)
                .map(da -> mapper.map(da, TDepositAccountResponse.class))
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Data akun deposito dengan id " + daId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public TDepositAccountResponse createDepositAccount(
            String role,
            TDepositAccountRequest daRequest) {

        var cif = SaveHandleEntity.handle(
                () -> paramClient.getCifByNik(daRequest.getNik()),
                "Gagal mendapatkan akun CIF");

        var savingAccount = saRepository.findSavingAccountByNumber(daRequest.getSavingAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        TSavingAccountExceptionCode.SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data akun tabungan dengan id " + daRequest.getAccountNumber() + " tidak ditemukan"));

        var depositProduct = SaveHandleEntity.handle(
                () -> paramClient.getDepositProductByName(daRequest.getDepositProductName()),
                "Gagal mendapatkan produk deposito");

        var status = SaveHandleEntity.handle(
                () -> paramClient.getStatusByCode(daRequest.getStatusCode()),
                "Gagal mendapatkan status");

        var depositAccount = new TDepositAccount();
        depositAccount.create(
                daRequest.getNominal(),
                daRequest.getAccountNumber(),
                daRequest.getTimePeriod(),
                daRequest.getStartDate(),
                daRequest.getDueDate(),
                savingAccount,
                cif,
                depositProduct,
                status,
                role);

        return mapper.map(
                daRepository.save(depositAccount),
                TDepositAccountResponse.class);
    }

    @Override
    @Transactional
    public TDepositAccountResponse updateDepositAccountById(
            Long daId,
            String role,
            TDepositAccountRequest daRequest) {
        var depositAccount = daRepository.findDepositAccountById(daId)
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Akun deposito dengan id " + daId + " tidak ditemukan"));

        var cif = SaveHandleEntity.handle(
                () -> paramClient.getCifByNik(daRequest.getNik()),
                "Gagal mendapatkan akun CIF");

        var savingAccount = saRepository.findSavingAccountByNumber(daRequest.getSavingAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        TSavingAccountExceptionCode.SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data akun tabungan dengan id " + daRequest.getAccountNumber() + " tidak ditemukan"));

        var depositProduct = SaveHandleEntity.handle(
                () -> paramClient.getDepositProductByName(daRequest.getDepositProductName()),
                "Gagal mendapatkan produk deposito");

        var status = SaveHandleEntity.handle(
                () -> paramClient.getStatusByCode(daRequest.getStatusCode()),
                "Gagal mendapatkan status");

        depositAccount.update(
                daRequest.getNominal(),
                daRequest.getAccountNumber(),
                daRequest.getTimePeriod(),
                daRequest.getStartDate(),
                daRequest.getDueDate(),
                savingAccount,
                cif,
                depositProduct,
                status,
                role);

        return mapper.map(
                daRepository.save(depositAccount),
                TDepositAccountResponse.class);
    }

    @Override
    @Transactional
    public void deleteDepositAccountById(Long daId, String role) {
        if (!daRepository.existsDepositAccountById(daId)) {
            throw new TDepositAccountException(
                    DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                    "Data akun deposito dengan id " + daId + " tidak ditemukan");
        }

        daRepository.softDelete(daId, role, LocalDateTime.now());
    }
}
