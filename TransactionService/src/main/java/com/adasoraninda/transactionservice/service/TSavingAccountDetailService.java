package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.TSavingAccountDetailRequest;
import com.adasoraninda.transactionservice.model.response.TSavingAccountDetailResponse;

import java.util.List;

public interface TSavingAccountDetailService {

    List<TSavingAccountDetailResponse> getAllSavingAccountDetail();

    TSavingAccountDetailResponse getSavingAccountDetailById(Long sadId);

    TSavingAccountDetailResponse createSavingDetailAccount(String role, TSavingAccountDetailRequest sadRequest);

    TSavingAccountDetailResponse updateSavingDetailAccount(Long sadId, String role, TSavingAccountDetailRequest sadRequest);

    void deleteSavingAccountDetail(Long sadId, String role);

}
