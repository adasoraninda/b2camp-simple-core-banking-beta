package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.TSavingAccountRequest;
import com.adasoraninda.transactionservice.model.response.TSavingAccountResponse;

import java.util.List;

public interface TSavingAccountService {

    List<TSavingAccountResponse> getAllSavingAccount();

    TSavingAccountResponse getSavingAccountById(Long saId);

    TSavingAccountResponse createSavingAccount(String role, TSavingAccountRequest saRequest);

    TSavingAccountResponse updateSavingAccount(Long saId, String role, TSavingAccountRequest saRequest);

    void deleteSavingAccount(Long saId, String role);
}
