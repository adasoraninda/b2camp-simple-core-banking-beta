package com.adasoraninda.transactionservice.service.impl.saving;

import com.adasoraninda.transactionservice.controller.client.ParameterFeignClient;
import com.adasoraninda.transactionservice.handle.SaveHandleEntity;
import com.adasoraninda.transactionservice.handle.exception.TSavingAccountException;
import com.adasoraninda.transactionservice.model.entity.TSavingAccount;
import com.adasoraninda.transactionservice.model.request.TSavingAccountRequest;
import com.adasoraninda.transactionservice.model.response.TSavingAccountResponse;
import com.adasoraninda.transactionservice.repository.TSavingAccountRepository;
import com.adasoraninda.transactionservice.service.TSavingAccountService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountExceptionCode.SAVING_ACCOUNT_IS_EMPTY;
import static com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountExceptionCode.SAVING_ACCOUNT_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class TSavingAccountServiceImpl implements TSavingAccountService {

    private final TSavingAccountRepository repository;
    private final ModelMapper mapper;
    private final ParameterFeignClient parameterClient;

    @Override
    public List<TSavingAccountResponse> getAllSavingAccount() {
        var allSavingAccounts = repository.findAllSavingAccount();

        if (allSavingAccounts.isEmpty()) {
            throw new TSavingAccountException(
                    SAVING_ACCOUNT_IS_EMPTY,
                    "Semua data akun tabungan tidak ada");
        }

        return allSavingAccounts.stream()
                .map(sa -> mapper.map(sa, TSavingAccountResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public TSavingAccountResponse getSavingAccountById(Long saId) {
        return repository.findSavingAccountById(saId)
                .map(sa -> mapper.map(sa, TSavingAccountResponse.class))
                .orElseThrow(() -> new TSavingAccountException(
                        SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data akun tabungan dengan id " + saId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public TSavingAccountResponse createSavingAccount(String role, TSavingAccountRequest saRequest) {
        var cif = SaveHandleEntity.handle(() -> parameterClient.getCifByNik(saRequest.getNik()),
                "Gagal mendapatkan data CIF");

        var productName = SaveHandleEntity.handle(() -> parameterClient.getSavingProductByProductName(saRequest.getProductName()),
                "Gagal mendapatkan data produk tabungan");

        var savingAccount = mapper.map(saRequest, TSavingAccount.class);
        savingAccount.create(cif, productName, role);

        return mapper.map(repository.save(savingAccount), TSavingAccountResponse.class);
    }

    @Override
    @Transactional
    public TSavingAccountResponse updateSavingAccount(Long saId, String role, TSavingAccountRequest saRequest) {
        var savingAccount = repository.findSavingAccountById(saId)
                .orElseThrow(() -> new TSavingAccountException(SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data akun tabungan dengan id " + saId + " tidak ditemukan"));

        var cif = SaveHandleEntity.handle(() -> parameterClient.getCifByNik(saRequest.getNik()),
                "Gagal mendapatkan data CIF");

        var productName = SaveHandleEntity.handle(() -> parameterClient.getSavingProductByProductName(saRequest.getProductName()),
                "Gagal mendapatkan data produk tabungan");

        savingAccount.update(saRequest.getAccountNumber(), saRequest.getBalance(), cif, productName, role);

        return mapper.map(repository.save(savingAccount), TSavingAccountResponse.class);
    }

    @Override
    @Transactional
    public void deleteSavingAccount(Long saId, String role) {
        if (!repository.existsSavingAccountById(saId)) {
            throw new TSavingAccountException(SAVING_ACCOUNT_IS_NOT_FOUND,
                    "Data akun tabungan dengan id " + saId + " tidak ditemukan");
        }

        repository.softDelete(saId, role, LocalDateTime.now());
    }
}
