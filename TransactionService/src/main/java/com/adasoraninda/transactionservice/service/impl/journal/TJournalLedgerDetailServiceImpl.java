package com.adasoraninda.transactionservice.service.impl.journal;

import com.adasoraninda.transactionservice.controller.client.ParameterFeignClient;
import com.adasoraninda.transactionservice.controller.code.MutationCode;
import com.adasoraninda.transactionservice.handle.SaveHandleEntity;
import com.adasoraninda.transactionservice.handle.exception.TJournalLedgerDetailException;
import com.adasoraninda.transactionservice.handle.exception.TJournalLedgerException;
import com.adasoraninda.transactionservice.model.entity.TJournalLedgerDetail;
import com.adasoraninda.transactionservice.model.request.TJournalLedgerDetailRequest;
import com.adasoraninda.transactionservice.model.response.TJournalLedgerDetailResponse;
import com.adasoraninda.transactionservice.repository.TJournalLedgerDetailRepository;
import com.adasoraninda.transactionservice.repository.TJournalLedgerRepository;
import com.adasoraninda.transactionservice.service.TJournalLedgerDetailService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.adasoraninda.transactionservice.handle.exception.code.TJournalLedgerDetailExceptionCode.JOURNAL_LEDGER_DETAIL_IS_EMPTY;
import static com.adasoraninda.transactionservice.handle.exception.code.TJournalLedgerDetailExceptionCode.JOURNAL_LEDGER_DETAIL_IS_NOT_FOUND;
import static com.adasoraninda.transactionservice.handle.exception.code.TJournalLedgerExceptionCode.JOURNAL_LEDGER_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class TJournalLedgerDetailServiceImpl implements TJournalLedgerDetailService {
    private final ParameterFeignClient paramClient;
    private final TJournalLedgerDetailRepository jldRepository;
    private final TJournalLedgerRepository jlRepository;
    private final ModelMapper mapper;

    @Override
    public List<TJournalLedgerDetailResponse> getAllJournalLedgerDetail() {
        var listJournalLedgerDetail = jldRepository.findAllJournalLedgerDetail();

        if (listJournalLedgerDetail.isEmpty()) {
            throw new TJournalLedgerDetailException(
                    JOURNAL_LEDGER_DETAIL_IS_EMPTY,
                    "Semua data journal ledger detail tidak ada");
        }

        return listJournalLedgerDetail.stream()
                .map(jld -> mapper.map(jld, TJournalLedgerDetailResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public TJournalLedgerDetailResponse getJournalLedgerDetailById(Long jldId) {
        return jldRepository.findJournalLedgerDetailById(jldId)
                .map(jld -> mapper.map(jld, TJournalLedgerDetailResponse.class))
                .orElseThrow(() -> new TJournalLedgerDetailException(
                        JOURNAL_LEDGER_DETAIL_IS_NOT_FOUND,
                        "Semua data journal ledger detail dengan id " + jldId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public TJournalLedgerDetailResponse createJournalLedgerDetail(
            TJournalLedgerDetailRequest jldRequest,
            String role) {

        if (!jldRequest.getList().equalsIgnoreCase(MutationCode.CREDIT.name()) ||
                !jldRequest.getList().equalsIgnoreCase(MutationCode.DEBIT.name())) {
            throw new TJournalLedgerDetailException(
                    JOURNAL_LEDGER_DETAIL_IS_NOT_FOUND,
                    "List harus bernilai DEBIT/CREDIT");
        }

        var coa = SaveHandleEntity.handle(
                () -> paramClient.getCoaByCode(jldRequest.getCoaCode()),
                "Gagal mendapatkan COA");

        var transactionCode = SaveHandleEntity.handle(
                () -> paramClient.getTransactionCodeByCode(jldRequest.getCode()),
                "Gagal mendapatkan kode transaksi");

        var journalLedger = jlRepository.findJournalLedgerById(jldRequest.getHeaderId())
                .orElseThrow(() -> new TJournalLedgerException(
                        JOURNAL_LEDGER_IS_NOT_FOUND,
                        "Journal Ledger dengan id " + jldRequest.getHeaderId() + " tidak ditemukan"));

        var journalLedgerDetail = new TJournalLedgerDetail();
        journalLedgerDetail.create(
                jldRequest.getDescription(),
                jldRequest.getList(),
                jldRequest.getDescription(),
                jldRequest.getNominal(),
                journalLedger,
                coa,
                transactionCode,
                role);

        return mapper.map(
                jldRepository.save(journalLedgerDetail),
                TJournalLedgerDetailResponse.class);
    }

    @Override
    @Transactional
    public TJournalLedgerDetailResponse updateJournalLedgerDetailById(
            Long jldId,
            TJournalLedgerDetailRequest jldRequest,
            String role) {

        if (!jldRequest.getList().equalsIgnoreCase(MutationCode.CREDIT.name()) ||
                !jldRequest.getList().equalsIgnoreCase(MutationCode.DEBIT.name())) {
            throw new TJournalLedgerDetailException(
                    JOURNAL_LEDGER_DETAIL_IS_NOT_FOUND,
                    "List harus bernilai DEBIT/CREDIT");
        }

        var coa = SaveHandleEntity.handle(
                () -> paramClient.getCoaByCode(jldRequest.getCoaCode()),
                "Gagal mendapatkan COA");

        var transactionCode = SaveHandleEntity.handle(
                () -> paramClient.getTransactionCodeByCode(jldRequest.getCode()),
                "Gagal mendapatkan kode transaksi");

        var journalLedgerDetail = jldRepository.findJournalLedgerDetailById(jldId)
                .orElseThrow(() -> new TJournalLedgerDetailException(
                        JOURNAL_LEDGER_DETAIL_IS_NOT_FOUND,
                        "Semua data journal ledger detail dengan id " + jldId + " tidak ditemukan"));

        var journalLedger = jlRepository.findJournalLedgerById(jldRequest.getHeaderId())
                .orElseThrow(() -> new TJournalLedgerException(
                        JOURNAL_LEDGER_IS_NOT_FOUND,
                        "Journal Ledger dengan id " + jldRequest.getHeaderId() + " tidak ditemukan"));


        journalLedgerDetail.update(
                jldRequest.getDescription(),
                jldRequest.getList(),
                jldRequest.getDescription(),
                jldRequest.getNominal(),
                journalLedger,
                coa,
                transactionCode,
                role);

        return mapper.map(jldRepository.save(journalLedgerDetail), TJournalLedgerDetailResponse.class);
    }

    @Override
    @Transactional
    public void deleteJournalLedgerDetailById(Long jldId, String role) {
        if (jldRepository.existsJournalLedgerDetailById(jldId)) {
            throw new TJournalLedgerDetailException(
                    JOURNAL_LEDGER_DETAIL_IS_NOT_FOUND,
                    "Semua data journal ledger detail dengan id " + jldId + " tidak ditemukan");
        }

        jldRepository.softDelete(jldId, role, LocalDateTime.now());
    }
}
