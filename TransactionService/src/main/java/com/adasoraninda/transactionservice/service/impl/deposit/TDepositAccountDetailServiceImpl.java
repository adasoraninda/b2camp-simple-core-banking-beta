package com.adasoraninda.transactionservice.service.impl.deposit;

import com.adasoraninda.transactionservice.controller.client.ParameterFeignClient;
import com.adasoraninda.transactionservice.controller.code.CoaCode;
import com.adasoraninda.transactionservice.controller.code.MutationCode;
import com.adasoraninda.transactionservice.controller.code.TransactionCode;
import com.adasoraninda.transactionservice.handle.SaveHandleEntity;
import com.adasoraninda.transactionservice.handle.exception.BalanceException;
import com.adasoraninda.transactionservice.handle.exception.TDepositAccountDetailException;
import com.adasoraninda.transactionservice.handle.exception.TDepositAccountException;
import com.adasoraninda.transactionservice.handle.exception.TSavingAccountException;
import com.adasoraninda.transactionservice.model.entity.*;
import com.adasoraninda.transactionservice.model.request.TDepositAccountDetailRequest;
import com.adasoraninda.transactionservice.model.response.TDepositAccountDetailResponse;
import com.adasoraninda.transactionservice.repository.*;
import com.adasoraninda.transactionservice.service.TDepositAccountDetailService;
import com.adasoraninda.transactionservice.service.impl.CreditTransaction;
import com.adasoraninda.transactionservice.service.impl.DebitTransaction;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.adasoraninda.transactionservice.handle.exception.code.BalanceExceptionCode.BALANCE_NOT_ENOUGH;
import static com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountDetailExceptionCode.DEPOSIT_ACCOUNT_DETAIL_IS_EMPTY;
import static com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountDetailExceptionCode.DEPOSIT_ACCOUNT_DETAIL_IS_NOT_FOUND;
import static com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountExceptionCode.DEPOSIT_ACCOUNT_IS_NOT_FOUND;
import static com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountExceptionCode.SAVING_ACCOUNT_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class TDepositAccountDetailServiceImpl implements TDepositAccountDetailService {

    private final TDepositAccountRepository daRepository;
    private final TDepositAccountDetailRepository dadRepository;
    private final TSavingAccountRepository saRepository;
    private final TSavingAccountDetailRepository sadRepository;
    private final TJournalLedgerRepository jlRepository;
    private final TJournalLedgerDetailRepository jldRepository;

    private final ParameterFeignClient parameterClient;
    private final ModelMapper mapper;

    @Override
    public List<TDepositAccountDetailResponse> getAllDepositAccountDetail() {
        var allDepositAccountDetail = dadRepository.findAllDepositAccountDetail();

        if (allDepositAccountDetail.isEmpty()) {
            throw new TDepositAccountDetailException(
                    DEPOSIT_ACCOUNT_DETAIL_IS_EMPTY,
                    "Semua data rincian akun deposito tidak ada");
        }

        return allDepositAccountDetail.stream()
                .map(dad -> mapper.map(dad, TDepositAccountDetailResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public TDepositAccountDetailResponse getDepositAccountDetailById(Long dadId) {
        return dadRepository.findDepositAccountDetailById(dadId)
                .map(dad -> mapper.map(dad, TDepositAccountDetailResponse.class))
                .orElseThrow(() -> new TDepositAccountDetailException(
                        DEPOSIT_ACCOUNT_DETAIL_IS_NOT_FOUND,
                        "Data rincian akun dengan id " + dadId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public TDepositAccountDetailResponse createDepositAccountDetail(
            String role,
            TDepositAccountDetailRequest dadRequest) {
        var savingAccount = saRepository.findSavingAccountByNumber(dadRequest.getSavingAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data akun tabungan dengan nomor " + dadRequest.getSavingAccountNumber() + " tidak ditemukan"));

        var depositAccount = daRepository.findDepositAccountByNumber(dadRequest.getDepositAccountNumber())
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Data akun deposito dengan nomor " + dadRequest.getDepositAccountNumber() + " tidak ditemukan"));

        var transactionCode = SaveHandleEntity.handle(
                () -> parameterClient.getTransactionCodeByCode(dadRequest.getTransactionCode()),
                "Gagal mendapatkan kode transaksi");

        if (savingAccount.getBalance().compareTo(dadRequest.getTransactionAmount()) < 0) {
            throw new BalanceException(
                    BALANCE_NOT_ENOUGH,
                    "Harap lakukan setor tunai terlebih dahulu");
        }

        sadRepository.save(debitSavingAccount(
                savingAccount,
                dadRequest.getTransactionAmount(),
                transactionCode,
                role));

        var depositAccountDetail = creditDepositAccount(
                depositAccount,
                dadRequest.getTransactionAmount(),
                transactionCode,
                role,
                null);

        createJournal(
                depositAccount,
                savingAccount,
                transactionCode,
                dadRequest.getTransactionAmount(),
                role);

        return mapper.map(
                dadRepository.save(depositAccountDetail),
                TDepositAccountDetailResponse.class);
    }

    @Override
    @Transactional
    public TDepositAccountDetailResponse updateDepositAccountDetailById(
            Long dadId,
            String role,
            TDepositAccountDetailRequest dadRequest) {
        var depositAccountDetail = dadRepository.findDepositAccountDetailById(dadId)
                .orElseThrow(() -> new TDepositAccountDetailException(
                        DEPOSIT_ACCOUNT_DETAIL_IS_NOT_FOUND,
                        "Data rincian akun dengan id " + dadId + " tidak ditemukan"));

        var savingAccount = saRepository.findSavingAccountByNumber(dadRequest.getSavingAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data akun tabungan dengan nomor " + dadRequest.getSavingAccountNumber() + " tidak ditemukan"));

        var depositAccount = daRepository.findDepositAccountByNumber(dadRequest.getDepositAccountNumber())
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Data akun deposito dengan nomor " + dadRequest.getDepositAccountNumber() + " tidak ditemukan"));

        var transactionCode = SaveHandleEntity.handle(
                () -> parameterClient.getTransactionCodeByCode(dadRequest.getTransactionCode()),
                "Gagal mendapatkan kode transaksi");

        if (savingAccount.getBalance().compareTo(dadRequest.getTransactionAmount()) < 0) {
            throw new BalanceException(
                    BALANCE_NOT_ENOUGH,
                    "Harap lakukan setor tunai terlebih dahulu");
        }

        sadRepository.save(debitSavingAccount(
                savingAccount,
                dadRequest.getTransactionAmount(),
                transactionCode,
                role));

        var newDetailDepositAccount = creditDepositAccount(
                depositAccount,
                dadRequest.getTransactionAmount(),
                transactionCode,
                role,
                depositAccountDetail);

        createJournal(
                depositAccount,
                savingAccount,
                transactionCode,
                dadRequest.getTransactionAmount(),
                role);

        return mapper.map(
                dadRepository.save(newDetailDepositAccount),
                TDepositAccountDetailResponse.class);
    }

    @Override
    @Transactional
    public void deleteDepositAccountDetailById(Long dadId, String role) {
        if (!dadRepository.existsDepositAccountDetailById(dadId)) {
            throw new TDepositAccountDetailException(
                    DEPOSIT_ACCOUNT_DETAIL_IS_NOT_FOUND,
                    "Data rincian akun dengan id " + dadId + " tidak ditemukan");
        }

        dadRepository.softDelete(dadId, role, LocalDateTime.now());
    }

    @Transactional
    private TDepositAccountDetail creditDepositAccount(
            TDepositAccount depositAccount,
            BigDecimal transactionAmount,
            MTransactionCode transactionCode,
            String role,
            @Nullable TDepositAccountDetail dadAccount
    ) {
        var creditDepositAccount = new CreditTransaction(
                depositAccount.getNominal(),
                transactionAmount);

        depositAccount.setNominal(creditDepositAccount.getBalance());
        depositAccount.setUpdatedDate(LocalDateTime.now());
        depositAccount.setUpdatedBy(role);

        daRepository.save(depositAccount);

        // Create
        if (dadAccount == null) {
            var depositAccountDetail = new TDepositAccountDetail();
            depositAccountDetail.create(
                    creditDepositAccount.getMutation().toString(),
                    transactionAmount,
                    depositAccount,
                    transactionCode,
                    role);
            return depositAccountDetail;
        }

        // Update
        dadAccount.update(
                creditDepositAccount.getMutation().toString(),
                transactionAmount,
                depositAccount,
                transactionCode,
                role);

        return dadAccount;
    }

    @Transactional
    private TSavingAccountDetail debitSavingAccount(
            TSavingAccount savingAccount,
            BigDecimal transactionAmount,
            MTransactionCode transactionCode,
            String role) {
        var debitSavingAccount = new DebitTransaction(
                savingAccount.getBalance(),
                transactionAmount);

        savingAccount.setBalance(debitSavingAccount.getBalance());
        savingAccount.setUpdatedDate(LocalDateTime.now());
        savingAccount.setUpdatedBy(role);

        saRepository.save(savingAccount);

        var savingAccountDetail = new TSavingAccountDetail();
        savingAccountDetail.create(
                debitSavingAccount.getMutation().toString(),
                transactionAmount,
                savingAccount,
                transactionCode,
                role);

        return savingAccountDetail;
    }

    @Transactional
    private void createJournal(
            TDepositAccount depositAccount,
            TSavingAccount savingAccount,
            MTransactionCode transactionCode,
            BigDecimal amount,
            String role) {

        TransactionCode code = TransactionCode.valueOf(transactionCode.getTransactionCode());

        if (code == TransactionCode.OVERBOOK) {
            var coa = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.DEPOSITO_TAHUNAN.getCode()),
                    "Gagal mendapatkan COA");

            new DepositJournal.Execute(transactionCode, amount, role, "000002", depositAccount, savingAccount)
                    .createJournalLedger(coa, jlRepository::save)
                    .createJournalLedgerDetail(coa, MutationCode.CREDIT, jldRepository::save)
                    .createJournalLedgerDetail(coa, MutationCode.DEBIT, jldRepository::save);
        }

        if (code == TransactionCode.SETOR_TUNAI) {
            var coaDebit = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.KAS_TELLER.getCode()),
                    "Gagal mendapatkan COA");

            var coaCredit = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.DEPOSITO_TAHUNAN.getCode()),
                    "Gagal mendapatkan COA");

            new DepositJournal.Execute(transactionCode, amount, role, "000002", depositAccount, savingAccount)
                    .createJournalLedger(coaDebit, jlRepository::save)
                    .createJournalLedgerDetail(coaCredit, MutationCode.CREDIT, jldRepository::save)
                    .createJournalLedgerDetail(coaDebit, MutationCode.DEBIT, jldRepository::save);
        }

        if (code == TransactionCode.TARIK_TUNAI) {
            var coaDebit = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.DEPOSITO_TAHUNAN.getCode()),
                    "Gagal mendapatkan COA");

            var coaCredit = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.KAS_TELLER.getCode()),
                    "Gagal mendapatkan COA");

            new DepositJournal.Execute(transactionCode, amount, role, "000002", depositAccount, savingAccount)
                    .createJournalLedger(coaCredit, jlRepository::save)
                    .createJournalLedgerDetail(coaCredit, MutationCode.CREDIT, jldRepository::save)
                    .createJournalLedgerDetail(coaDebit, MutationCode.DEBIT, jldRepository::save);
        }

    }

}
