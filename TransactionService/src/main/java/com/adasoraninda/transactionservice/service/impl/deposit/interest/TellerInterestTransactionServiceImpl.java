package com.adasoraninda.transactionservice.service.impl.deposit.interest;

import com.adasoraninda.transactionservice.controller.client.ParameterFeignClient;
import com.adasoraninda.transactionservice.controller.code.MutationCode;
import com.adasoraninda.transactionservice.controller.code.TransactionCode;
import com.adasoraninda.transactionservice.handle.SaveHandleEntity;
import com.adasoraninda.transactionservice.handle.exception.TDepositAccountException;
import com.adasoraninda.transactionservice.model.entity.TDepositAccountDetail;
import com.adasoraninda.transactionservice.model.request.TellerInterestTransactionRequest;
import com.adasoraninda.transactionservice.model.response.TDepositAccountResponse;
import com.adasoraninda.transactionservice.repository.TDepositAccountDetailRepository;
import com.adasoraninda.transactionservice.repository.TDepositAccountRepository;
import com.adasoraninda.transactionservice.service.TellerInterestTransactionService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountExceptionCode.DEPOSIT_ACCOUNT_IS_NOT_FOUND;

// Teller melakukan transaksi bagi hasil
@Service
@AllArgsConstructor
public class TellerInterestTransactionServiceImpl implements TellerInterestTransactionService {

    private final TDepositAccountRepository daRepository;
    private final TDepositAccountDetailRepository dadRepository;
    private final ParameterFeignClient paramClient;
    private final ModelMapper mapper;

    @Override
    @Transactional
    public TDepositAccountResponse interestRateTransaction(
            TellerInterestTransactionRequest titRequest,
            String role) {
        var depositAccount = daRepository.findDepositAccountByNumber(titRequest.getDepositAccountNumber())
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Data akun deposito dengan nomor " + titRequest.getDepositAccountNumber() + " tidak ditemukan"));

        var transactionCode = SaveHandleEntity.handle(
                () -> paramClient.getTransactionCodeByCode(TransactionCode.SETOR_TUNAI.getCode()),
                "Gagal medapatkan kode transaksi");

        var interestRate = BigDecimal.valueOf(
                depositAccount.getDepositProduct().getInterestRate() * 100);

        var timePeriod = BigDecimal.valueOf(
                depositAccount.getTimePeriod());

        var profitSharing = depositAccount.getNominal()
                .multiply(interestRate)
                .multiply(timePeriod);

        depositAccount.setNominal(profitSharing);
        daRepository.save(depositAccount);

        var depositAccountDetail = new TDepositAccountDetail();
        depositAccountDetail.create(
                MutationCode.CREDIT.toString(),
                profitSharing,
                depositAccount,
                transactionCode,
                role);
        dadRepository.save(depositAccountDetail);

        return mapper.map(depositAccount, TDepositAccountResponse.class);
    }
}
