package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.CustomerDepositLiquidationRequest;

import java.util.Map;

public interface CustomerDepositLiquidationService {

    Map<String, Object> depositLiquidation(
            CustomerDepositLiquidationRequest cdlRequest,
            String role);

}
