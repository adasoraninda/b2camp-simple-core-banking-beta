package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.TDepositAccountRequest;
import com.adasoraninda.transactionservice.model.response.TDepositAccountResponse;

import java.util.List;

public interface TDepositAccountService {

    List<TDepositAccountResponse> getAllDepositAccount();

    TDepositAccountResponse getDepositAccountById(Long daId);

    TDepositAccountResponse createDepositAccount(
            String role,
            TDepositAccountRequest daRequest);

    TDepositAccountResponse updateDepositAccountById(
            Long daId,
            String role,
            TDepositAccountRequest daRequest);

    void deleteDepositAccountById(
            Long daId,
            String role);

}
