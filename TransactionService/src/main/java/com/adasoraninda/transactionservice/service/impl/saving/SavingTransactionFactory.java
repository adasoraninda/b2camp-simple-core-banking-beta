package com.adasoraninda.transactionservice.service.impl.saving;

import com.adasoraninda.transactionservice.controller.code.TransactionCode;
import com.adasoraninda.transactionservice.handle.exception.AppCodeException;
import com.adasoraninda.transactionservice.service.impl.CreditTransaction;
import com.adasoraninda.transactionservice.service.impl.DebitTransaction;
import com.adasoraninda.transactionservice.service.impl.MutationTransaction;

import java.math.BigDecimal;

public final class SavingTransactionFactory {

    public static MutationTransaction create(TransactionCode code, BigDecimal balance, BigDecimal amount) {
        if (code == TransactionCode.SETOR_TUNAI) {
            return new CreditTransaction(balance, amount);
        } else if (code == TransactionCode.TARIK_TUNAI) {
            return new DebitTransaction(balance, amount);
        } else {
            throw new AppCodeException(code.name());
        }
    }

}
