package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.TDepositAccountDetailRequest;
import com.adasoraninda.transactionservice.model.response.TDepositAccountDetailResponse;

import java.util.List;

public interface TDepositAccountDetailService {

    List<TDepositAccountDetailResponse> getAllDepositAccountDetail();

    TDepositAccountDetailResponse getDepositAccountDetailById(Long dadId);

    TDepositAccountDetailResponse createDepositAccountDetail(
            String role,
            TDepositAccountDetailRequest dadRequest);

    TDepositAccountDetailResponse updateDepositAccountDetailById(
            Long dadId,
            String role,
            TDepositAccountDetailRequest dadRequest);

    void deleteDepositAccountDetailById(
            Long dadId,
            String role);

}
