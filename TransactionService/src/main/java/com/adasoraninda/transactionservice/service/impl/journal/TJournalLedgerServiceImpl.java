package com.adasoraninda.transactionservice.service.impl.journal;

import com.adasoraninda.transactionservice.controller.client.ParameterFeignClient;
import com.adasoraninda.transactionservice.handle.SaveHandleEntity;
import com.adasoraninda.transactionservice.handle.exception.TJournalLedgerException;
import com.adasoraninda.transactionservice.model.entity.TJournalLedger;
import com.adasoraninda.transactionservice.model.request.TJournalLedgerRequest;
import com.adasoraninda.transactionservice.model.response.TJournalLedgerResponse;
import com.adasoraninda.transactionservice.repository.TJournalLedgerRepository;
import com.adasoraninda.transactionservice.service.TJournalLedgerService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.adasoraninda.transactionservice.handle.exception.code.TJournalLedgerExceptionCode.JOURNAL_LEDGER_IS_EMPTY;
import static com.adasoraninda.transactionservice.handle.exception.code.TJournalLedgerExceptionCode.JOURNAL_LEDGER_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class TJournalLedgerServiceImpl implements TJournalLedgerService {
    private final ParameterFeignClient paramClient;
    private final TJournalLedgerRepository jlRepository;
    private final ModelMapper mapper;

    @Override
    public List<TJournalLedgerResponse> getAllJournalLedger() {
        var listJournalLeger = jlRepository.findAllJournalLedger();

        if (listJournalLeger.isEmpty()) {
            throw new TJournalLedgerException(
                    JOURNAL_LEDGER_IS_EMPTY,
                    "Semua data journal ledger tidak ada");
        }

        return listJournalLeger.stream()
                .map(jl -> mapper.map(jl, TJournalLedgerResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public TJournalLedgerResponse getJournalLedgerById(Long jlId) {
        return jlRepository.findJournalLedgerById(jlId)
                .map(jl -> mapper.map(jl, TJournalLedgerResponse.class))
                .orElseThrow(() -> new TJournalLedgerException(
                        JOURNAL_LEDGER_IS_NOT_FOUND,
                        "Data journal ledger dengan id " + jlId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public TJournalLedgerResponse createJournalLedger(TJournalLedgerRequest jlRequest, String role) {
        var transactionCode = SaveHandleEntity.handle(() ->
                        paramClient.getTransactionCodeByCode(jlRequest.getCode()),
                "Gagal mendapatkan kode transaksi");

        var journalLedger = new TJournalLedger();
        journalLedger.create(
                jlRequest.getDescription(),
                jlRequest.getReference(),
                transactionCode,
                jlRequest.getNominal(),
                role);

        return mapper.map(
                jlRepository.save(journalLedger),
                TJournalLedgerResponse.class);
    }

    @Override
    @Transactional
    public TJournalLedgerResponse updateJournalLedgerById(Long jlId, TJournalLedgerRequest jlRequest, String role) {
        var transactionCode = SaveHandleEntity.handle(() ->
                        paramClient.getTransactionCodeByCode(jlRequest.getCode()),
                "Gagal mendapatkan kode transaksi");

        var journalLedger = jlRepository.findJournalLedgerById(jlId)
                .orElseThrow(() -> new TJournalLedgerException(
                        JOURNAL_LEDGER_IS_NOT_FOUND,
                        "Data journal ledger dengan id " + jlId + " tidak ditemukan"));

        journalLedger.update(
                jlRequest.getDescription(),
                jlRequest.getReference(),
                transactionCode,
                jlRequest.getNominal(),
                role);

        return mapper.map(
                jlRepository.save(journalLedger),
                TJournalLedgerResponse.class);
    }

    @Override
    @Transactional
    public void deleteJournalLedgerById(Long jlId, String role) {
        if (!jlRepository.existsJournalLedgerById(jlId)) {
            throw new TJournalLedgerException(
                    JOURNAL_LEDGER_IS_NOT_FOUND,
                    "Data journal ledger dengan id " + jlId + " tidak ditemukan");
        }

        jlRepository.softDelete(jlId, role, LocalDateTime.now());
    }
}
