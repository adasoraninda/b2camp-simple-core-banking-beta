package com.adasoraninda.transactionservice.service.impl.saving;

import com.adasoraninda.transactionservice.controller.client.ParameterFeignClient;
import com.adasoraninda.transactionservice.controller.code.CoaCode;
import com.adasoraninda.transactionservice.controller.code.MutationCode;
import com.adasoraninda.transactionservice.controller.code.TransactionCode;
import com.adasoraninda.transactionservice.handle.SaveHandleEntity;
import com.adasoraninda.transactionservice.handle.exception.TSavingAccountDetailException;
import com.adasoraninda.transactionservice.handle.exception.TSavingAccountException;
import com.adasoraninda.transactionservice.model.entity.MTransactionCode;
import com.adasoraninda.transactionservice.model.entity.TSavingAccount;
import com.adasoraninda.transactionservice.model.entity.TSavingAccountDetail;
import com.adasoraninda.transactionservice.model.request.TSavingAccountDetailRequest;
import com.adasoraninda.transactionservice.model.response.TSavingAccountDetailResponse;
import com.adasoraninda.transactionservice.repository.TJournalLedgerDetailRepository;
import com.adasoraninda.transactionservice.repository.TJournalLedgerRepository;
import com.adasoraninda.transactionservice.repository.TSavingAccountDetailRepository;
import com.adasoraninda.transactionservice.repository.TSavingAccountRepository;
import com.adasoraninda.transactionservice.service.TSavingAccountDetailService;
import com.adasoraninda.transactionservice.service.impl.MutationTransaction;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountDetailExceptionCode.*;
import static com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountExceptionCode.SAVING_ACCOUNT_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class TSavingAccountDetailServiceImpl implements TSavingAccountDetailService {

    private final TSavingAccountDetailRepository accDetailRepository;
    private final TSavingAccountRepository accRepository;
    private final TJournalLedgerRepository jlRepository;
    private final TJournalLedgerDetailRepository jldRepository;

    private final ParameterFeignClient parameterClient;
    private final ModelMapper mapper;

    @Override
    public List<TSavingAccountDetailResponse> getAllSavingAccountDetail() {
        var allSavingAccountDetail = accDetailRepository.findAllSavingAccountDetail();

        if (allSavingAccountDetail.isEmpty()) {
            throw new TSavingAccountDetailException(
                    SAVING_ACCOUNT_DETAIL_IS_EMPTY,
                    "Semua data rincian akun tabungan tidak ada");
        }

        return allSavingAccountDetail.stream()
                .map(sad -> mapper.map(sad, TSavingAccountDetailResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public TSavingAccountDetailResponse getSavingAccountDetailById(Long sadId) {
        return accDetailRepository.findSavingAccountDetailById(sadId)
                .map(sad -> mapper.map(sad, TSavingAccountDetailResponse.class))
                .orElseThrow(() -> new TSavingAccountDetailException(
                        SAVING_ACCOUNT_DETAIL_IS_NOT_FOUND,
                        "Data rincian akun tabungan dengan id " + sadId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public TSavingAccountDetailResponse createSavingDetailAccount(String role, TSavingAccountDetailRequest sadRequest) {
        var transactionCode = SaveHandleEntity.handle(() -> parameterClient.getTransactionCodeByCode(sadRequest.getTransactionCode()),
                "Gagal mendapatkan kode transaksi");

        var savingAccount = accRepository.findSavingAccountByNumber(sadRequest.getAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data akun tabungan dengan nomor rekening " + sadRequest.getAccountNumber() + " tidak ditemukan"));

        var targetSavingAccount = accRepository.findSavingAccountByNumber(sadRequest.getTargetAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data tujuan akun tabungan dengan nomor rekening " + sadRequest.getTargetAccountNumber() + " tidak ditemukan"));

        TransactionCode code = TransactionCode.valueOf(transactionCode.getTransactionCode());

        if (code == TransactionCode.OVERBOOK) {
            if (sadRequest.getAccountNumber().equals(sadRequest.getTargetAccountNumber())) {
                throw new TSavingAccountDetailException(
                        SAVING_ACCOUNT_DETAIL_ACC_NUMBER_NOT_VALID,
                        "Nomor rekening tujuan tidak boleh sama jika menggunakan kode transaksi OVERBOOK");
            }

            var savingAccountDetail = getSavingAccountDetail(
                    TransactionCode.TARIK_TUNAI,
                    savingAccount,
                    transactionCode,
                    sadRequest.getTransactionAmount(),
                    role,
                    null);

            getSavingAccountDetail(
                    TransactionCode.SETOR_TUNAI,
                    targetSavingAccount,
                    transactionCode,
                    sadRequest.getTransactionAmount(),
                    role,
                    null);

            createJournal(
                    savingAccount,
                    targetSavingAccount,
                    transactionCode,
                    sadRequest.getTransactionAmount(),
                    role);

            return mapper.map(
                    accDetailRepository.save(savingAccountDetail),
                    TSavingAccountDetailResponse.class);
        }

        if (!sadRequest.getAccountNumber().equals(sadRequest.getTargetAccountNumber())) {
            throw new TSavingAccountDetailException(
                    SAVING_ACCOUNT_DETAIL_ACC_NUMBER_NOT_VALID,
                    "Nomor rekening tujuan harus sama jika menggunakan kode transaksi DEBIT / CREDIT");
        }

        var savingAccountDetail = getSavingAccountDetail(
                code,
                savingAccount,
                transactionCode,
                sadRequest.getTransactionAmount(),
                role,
                null);

        createJournal(
                savingAccount,
                targetSavingAccount,
                transactionCode,
                sadRequest.getTransactionAmount(),
                role);

        return mapper.map(
                accDetailRepository.save(savingAccountDetail),
                TSavingAccountDetailResponse.class);
    }

    @Override
    @Transactional
    public TSavingAccountDetailResponse updateSavingDetailAccount(Long sadId, String role, TSavingAccountDetailRequest sadRequest) {
        var savingAccountDetail = accDetailRepository.findSavingAccountDetailById(sadId)
                .orElseThrow(() -> new TSavingAccountDetailException(
                        SAVING_ACCOUNT_DETAIL_IS_NOT_FOUND,
                        "Data rincian akun tabungan dengan id " + sadId + " tidak ditemukan"));

        var transactionCode = SaveHandleEntity.handle(
                () -> parameterClient.getTransactionCodeByCode(sadRequest.getTransactionCode()),
                "Gagal mendapatkan kode transaksi");

        var savingAccount = accRepository.findSavingAccountByNumber(sadRequest.getAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data akun tabungan dengan nomor rekening " + sadRequest.getAccountNumber() + " tidak ditemukan"));

        var targetSavingAccount = accRepository.findSavingAccountByNumber(sadRequest.getTargetAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Data tujuan akun tabungan dengan nomor rekening " + sadRequest.getTargetAccountNumber() + " tidak ditemukan"));

        TransactionCode code = TransactionCode.valueOf(transactionCode.getTransactionCode());

        if (code == TransactionCode.OVERBOOK) {
            if (sadRequest.getAccountNumber().equals(sadRequest.getTargetAccountNumber())) {
                throw new TSavingAccountDetailException(
                        SAVING_ACCOUNT_DETAIL_ACC_NUMBER_NOT_VALID,
                        "Nomor rekening tujuan tidak boleh sama jika menggunakan kode transaksi OVERBOOK");
            }

            var savingAccountDetail1 = getSavingAccountDetail(
                    TransactionCode.TARIK_TUNAI,
                    savingAccount,
                    transactionCode,
                    sadRequest.getTransactionAmount(),
                    role,
                    savingAccountDetail);

            getSavingAccountDetail(
                    TransactionCode.SETOR_TUNAI,
                    targetSavingAccount,
                    transactionCode,
                    sadRequest.getTransactionAmount(),
                    role,
                    savingAccountDetail);

            createJournal(
                    savingAccount,
                    targetSavingAccount,
                    transactionCode,
                    sadRequest.getTransactionAmount(),
                    role);

            return mapper.map(
                    accDetailRepository.save(savingAccountDetail1),
                    TSavingAccountDetailResponse.class);
        }

        if (!sadRequest.getAccountNumber().equals(sadRequest.getTargetAccountNumber())) {
            throw new TSavingAccountDetailException(
                    SAVING_ACCOUNT_DETAIL_ACC_NUMBER_NOT_VALID,
                    "Nomor rekening tujuan harus sama jika menggunakan kode transaksi DEBIT / CREDIT");
        }

        savingAccountDetail.update(
                code.toString(),
                sadRequest.getTransactionAmount(),
                savingAccount,
                transactionCode,
                role);

        createJournal(
                savingAccount,
                targetSavingAccount,
                transactionCode,
                sadRequest.getTransactionAmount(),
                role);

        return mapper.map(
                accDetailRepository.save(savingAccountDetail),
                TSavingAccountDetailResponse.class);
    }

    @Override
    @Transactional
    public void deleteSavingAccountDetail(Long sadId, String role) {
        if (!accDetailRepository.existsSavingAccountDetailById(sadId)) {
            throw new TSavingAccountDetailException(
                    SAVING_ACCOUNT_DETAIL_IS_NOT_FOUND,
                    "Data rincian akun tabungan dengan id " + sadId + " tidak ditemukan");
        }

        accDetailRepository.softDelete(sadId, role, LocalDateTime.now());
    }

    @Transactional
    private TSavingAccountDetail getSavingAccountDetail(TransactionCode code,
                                                        TSavingAccount savingAccount,
                                                        MTransactionCode transactionCode,
                                                        BigDecimal txAmount,
                                                        String role,
                                                        @Nullable TSavingAccountDetail savingAccountDetail) {
        MutationTransaction transaction = SavingTransactionFactory.create(
                code,
                savingAccount.getBalance(),
                txAmount);

        savingAccount.setBalance(transaction.getBalance());
        savingAccount.setUpdatedBy(role);
        savingAccount.setUpdatedDate(LocalDateTime.now());

        accRepository.save(savingAccount);

        // Create
        if (savingAccountDetail == null) {
            savingAccountDetail = new TSavingAccountDetail();
            savingAccountDetail.create(transaction.getMutation().toString(),
                    txAmount,
                    savingAccount,
                    transactionCode,
                    role);
        } else {
            // Update
            savingAccountDetail.update(transaction.getMutation().toString(),
                    txAmount,
                    savingAccount,
                    transactionCode,
                    role);
        }

        return savingAccountDetail;
    }

    @Transactional
    private void createJournal(
            TSavingAccount savingAccount,
            TSavingAccount targetSavingAccount,
            MTransactionCode transactionCode,
            BigDecimal amount,
            String role) {

        TransactionCode code = TransactionCode.valueOf(transactionCode.getTransactionCode());

        if (code == TransactionCode.OVERBOOK) {
            var coa = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.TABUNGAN_UMUM.getCode()),
                    "Gagal mendapatkan COA");

            new SavingJournal.Execute(
                    transactionCode, amount, role, "000001", savingAccount, targetSavingAccount)
                    .createJournalLedger(coa, jlRepository::save)
                    .createJournalLedgerDetail(coa, MutationCode.CREDIT, jldRepository::save)
                    .createJournalLedgerDetail(coa, MutationCode.DEBIT, jldRepository::save);
        }

        if (code == TransactionCode.SETOR_TUNAI) {
            var coaDebit = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.KAS_TELLER.getCode()),
                    "Gagal mendapatkan COA");

            var coaCredit = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.TABUNGAN_UMUM.getCode()),
                    "Gagal mendapatkan COA");

            new SavingJournal.Execute(
                    transactionCode, amount, role, "000001", savingAccount, targetSavingAccount)
                    .createJournalLedger(coaDebit, jlRepository::save)
                    .createJournalLedgerDetail(coaCredit, MutationCode.CREDIT, jldRepository::save)
                    .createJournalLedgerDetail(coaDebit, MutationCode.DEBIT, jldRepository::save);
        }

        if (code == TransactionCode.TARIK_TUNAI) {
            var coaDebit = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.TABUNGAN_UMUM.getCode()),
                    "Gagal mendapatkan COA");

            var coaCredit = SaveHandleEntity.handle(
                    () -> parameterClient.getCoaByCode(CoaCode.KAS_TELLER.getCode()),
                    "Gagal mendapatkan COA");

            new SavingJournal.Execute(
                    transactionCode, amount, role, "000001", savingAccount, targetSavingAccount)
                    .createJournalLedger(coaCredit, jlRepository::save)
                    .createJournalLedgerDetail(coaCredit, MutationCode.CREDIT, jldRepository::save)
                    .createJournalLedgerDetail(coaDebit, MutationCode.DEBIT, jldRepository::save);
        }

    }
}
