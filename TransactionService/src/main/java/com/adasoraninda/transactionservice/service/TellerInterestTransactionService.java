package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.TellerInterestTransactionRequest;
import com.adasoraninda.transactionservice.model.response.TDepositAccountResponse;

public interface TellerInterestTransactionService {

    TDepositAccountResponse interestRateTransaction(
            TellerInterestTransactionRequest titRequest,
            String role);

}
