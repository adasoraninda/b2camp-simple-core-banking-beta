package com.adasoraninda.transactionservice.service.impl.deposit.interest;

import com.adasoraninda.transactionservice.controller.client.ParameterFeignClient;
import com.adasoraninda.transactionservice.controller.code.MutationCode;
import com.adasoraninda.transactionservice.controller.code.StatusCode;
import com.adasoraninda.transactionservice.controller.code.TransactionCode;
import com.adasoraninda.transactionservice.handle.SaveHandleEntity;
import com.adasoraninda.transactionservice.handle.exception.MDepositBillyetException;
import com.adasoraninda.transactionservice.handle.exception.TDepositAccountException;
import com.adasoraninda.transactionservice.handle.exception.TSavingAccountException;
import com.adasoraninda.transactionservice.model.entity.TDepositAccountDetail;
import com.adasoraninda.transactionservice.model.entity.TSavingAccountDetail;
import com.adasoraninda.transactionservice.model.request.CustomerDepositLiquidationRequest;
import com.adasoraninda.transactionservice.model.response.TDepositAccountResponse;
import com.adasoraninda.transactionservice.model.response.TSavingAccountResponse;
import com.adasoraninda.transactionservice.repository.*;
import com.adasoraninda.transactionservice.service.CustomerDepositLiquidationService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Map;

import static com.adasoraninda.transactionservice.handle.exception.code.MDepositBillyetExceptionCode.DEPOSIT_BILLYET_IS_NOT_FOUND;
import static com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountExceptionCode.DEPOSIT_ACCOUNT_IS_NOT_FOUND;
import static com.adasoraninda.transactionservice.handle.exception.code.TSavingAccountExceptionCode.SAVING_ACCOUNT_IS_NOT_FOUND;

// Nasabah mencairkan deposito
@Service
@AllArgsConstructor
public class CustomerDepositLiquidationServiceImpl implements CustomerDepositLiquidationService {

    private final MDepositBillyetRepository dbRepository;
    private final TDepositAccountRepository daRepository;
    private final TDepositAccountDetailRepository dadRepository;
    private final TSavingAccountRepository saRepository;
    private final TSavingAccountDetailRepository sadRepository;

    private final ParameterFeignClient paramClient;
    private final ModelMapper mapper;

    private static final String KEY_DEPOSIT_ACC = "depositAccount";
    private static final String KEY_SAVING_ACC = "savingAccount";

    @Override
    @Transactional
    public Map<String, Object> depositLiquidation(
            CustomerDepositLiquidationRequest cdlRequest,
            String role) {

        var depositBillyet = dbRepository.findDepositBillyetByNumber(cdlRequest.getBillyetNumber())
                .orElseThrow(() -> new MDepositBillyetException(
                        DEPOSIT_BILLYET_IS_NOT_FOUND,
                        "Depoist billyet dengan nomor " + cdlRequest.getBillyetNumber() + " tidak ditemukan"));

        var depositAccount = daRepository.findDepositAccountByNumber(cdlRequest.getDepositAccountNumber())
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Akun deposito dengan nomor " + cdlRequest.getDepositAccountNumber() + " tidak ditemukan"));

        var savingAccount = saRepository.findSavingAccountByNumber(cdlRequest.getSavingAccountNumber())
                .orElseThrow(() -> new TSavingAccountException(
                        SAVING_ACCOUNT_IS_NOT_FOUND,
                        "Akun tabungan dengan nomor " + cdlRequest.getSavingAccountNumber() + " tidak ditemukan"));

        // Status INACTIVE
        var statusInactive = SaveHandleEntity.handle(
                () -> paramClient.getStatusByCode(StatusCode.INACTIVE.name()),
                "Gagal mendapatkan status");

        // OVERBOOK
        var transactionCode = SaveHandleEntity.handle(
                () -> paramClient.getTransactionCodeByCode(TransactionCode.OVERBOOK.getCode()),
                "Gagal mendapatkan kode transaksi");

        // Debit deposit account dan status menjadi INACTIVE
        var depositNominal = depositAccount.getNominal();
        depositAccount.setStatus(statusInactive);
        daRepository.save(depositAccount);

        var depositAccountDetail = new TDepositAccountDetail();
        depositAccountDetail.create(
                MutationCode.DEBIT.toString(),
                depositNominal,
                depositAccount,
                transactionCode,
                role);
        dadRepository.save(depositAccountDetail);

        // Credit saving account
        savingAccount.setBalance(savingAccount.getBalance().add(depositNominal));
        saRepository.save(savingAccount);

        var savingAccountDetail = new TSavingAccountDetail();
        savingAccountDetail.create(
                MutationCode.CREDIT.toString(),
                depositNominal,
                savingAccount,
                transactionCode,
                role);
        sadRepository.save(savingAccountDetail);

        // Hapus deposit billyet
        dbRepository.softDelete(depositBillyet.getId(), role, LocalDateTime.now());

        return Map.of(
                KEY_DEPOSIT_ACC, mapper.map(depositAccount, TDepositAccountResponse.class),
                KEY_SAVING_ACC, mapper.map(savingAccount, TSavingAccountResponse.class));
    }
}
