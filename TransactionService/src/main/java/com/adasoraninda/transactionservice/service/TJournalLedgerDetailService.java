package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.TJournalLedgerDetailRequest;
import com.adasoraninda.transactionservice.model.response.TJournalLedgerDetailResponse;

import java.util.List;

public interface TJournalLedgerDetailService {

    List<TJournalLedgerDetailResponse> getAllJournalLedgerDetail();

    TJournalLedgerDetailResponse getJournalLedgerDetailById(Long jldId);

    TJournalLedgerDetailResponse createJournalLedgerDetail(
            TJournalLedgerDetailRequest jldRequest,
            String role);

    TJournalLedgerDetailResponse updateJournalLedgerDetailById(
            Long jldId,
            TJournalLedgerDetailRequest jldRequest,
            String role);

    void deleteJournalLedgerDetailById(
            Long jldId,
            String role);

}
