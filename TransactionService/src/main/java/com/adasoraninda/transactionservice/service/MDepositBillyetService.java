package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.MDepositBillyetRequest;
import com.adasoraninda.transactionservice.model.response.MDepositBillyetResponse;

import java.util.List;

public interface MDepositBillyetService {

    List<MDepositBillyetResponse> getAllDepositBillyet();

    MDepositBillyetResponse getDepositBillyetById(Long dbId);

    MDepositBillyetResponse createDepositBillyet(
            MDepositBillyetRequest dbRequest,
            String role);

    MDepositBillyetResponse updateDepositBillyetById(
            Long dbId,
            MDepositBillyetRequest dbRequest,
            String role);

    void deleteDepositBillyetById(
            Long dbId,
            String role);

}
