package com.adasoraninda.transactionservice.service.impl.deposit;

import com.adasoraninda.transactionservice.handle.exception.MDepositBillyetException;
import com.adasoraninda.transactionservice.handle.exception.TDepositAccountException;
import com.adasoraninda.transactionservice.model.entity.MDepositBillyet;
import com.adasoraninda.transactionservice.model.request.MDepositBillyetRequest;
import com.adasoraninda.transactionservice.model.response.MDepositBillyetResponse;
import com.adasoraninda.transactionservice.repository.MDepositBillyetRepository;
import com.adasoraninda.transactionservice.repository.TDepositAccountRepository;
import com.adasoraninda.transactionservice.service.MDepositBillyetService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.adasoraninda.transactionservice.handle.exception.code.MDepositBillyetExceptionCode.DEPOSIT_BILLYET_IS_NOT_EMPTY;
import static com.adasoraninda.transactionservice.handle.exception.code.MDepositBillyetExceptionCode.DEPOSIT_BILLYET_IS_NOT_FOUND;
import static com.adasoraninda.transactionservice.handle.exception.code.TDepositAccountExceptionCode.DEPOSIT_ACCOUNT_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class MDepositBillyetServiceImpl implements MDepositBillyetService {

    private final MDepositBillyetRepository billyetRepository;
    private final TDepositAccountRepository accRepository;
    private final ModelMapper mapper;

    @Override
    public List<MDepositBillyetResponse> getAllDepositBillyet() {
        var allDepositBillyet = billyetRepository.findAllDepositBillyet();

        if (allDepositBillyet.isEmpty()) {
            throw new MDepositBillyetException(
                    DEPOSIT_BILLYET_IS_NOT_EMPTY,
                    "Semua data billyet deposito tidak ada");
        }

        return allDepositBillyet.stream()
                .map(db -> mapper.map(db, MDepositBillyetResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public MDepositBillyetResponse getDepositBillyetById(Long dbId) {
        return billyetRepository.findDepositBillyetById(dbId)
                .map(db -> mapper.map(db, MDepositBillyetResponse.class))
                .orElseThrow(() -> new MDepositBillyetException(
                        DEPOSIT_BILLYET_IS_NOT_FOUND,
                        "Data billyet deposito dengan id " + dbId + " tidak ditemukan"));
    }

    @Override
    @Transactional
    public MDepositBillyetResponse createDepositBillyet(MDepositBillyetRequest dbRequest, String role) {
        var depositAccount = accRepository
                .findDepositAccountByNumber(dbRequest.getDepositAccountNumber())
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Akun deposito dengan nomor " + dbRequest.getDepositAccountNumber() + " tidak ditemukan"));

        var depositBillyet = new MDepositBillyet();
        depositBillyet.create(
                dbRequest.getBillyetNumber(),
                depositAccount,
                role);

        return mapper.map(
                billyetRepository.save(depositBillyet),
                MDepositBillyetResponse.class);
    }

    @Override
    @Transactional
    public MDepositBillyetResponse updateDepositBillyetById(
            Long dbId,
            MDepositBillyetRequest dbRequest,
            String role) {
        var depositBillyet = billyetRepository.findDepositBillyetById(dbId)
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Data billyet deposito dengan id " + dbId + " tidak ditemukan"));

        var depositAccount = accRepository
                .findDepositAccountByNumber(dbRequest.getDepositAccountNumber())
                .orElseThrow(() -> new TDepositAccountException(
                        DEPOSIT_ACCOUNT_IS_NOT_FOUND,
                        "Akun deposito dengan nomor " + dbRequest.getDepositAccountNumber() + " tidak ditemukan"));

        depositBillyet.update(dbRequest.getBillyetNumber(), depositAccount, role);

        return mapper.map(
                billyetRepository.save(depositBillyet),
                MDepositBillyetResponse.class);
    }

    @Override
    @Transactional
    public void deleteDepositBillyetById(Long dbId, String role) {
        if (!billyetRepository.existsDepositBillyetById(dbId)) {
            throw new MDepositBillyetException(
                    DEPOSIT_BILLYET_IS_NOT_FOUND,
                    "Data billyet deposito dengan id " + dbId + " tidak ditemukan");
        }

        billyetRepository.softDelete(dbId, role, LocalDateTime.now());
    }
}
