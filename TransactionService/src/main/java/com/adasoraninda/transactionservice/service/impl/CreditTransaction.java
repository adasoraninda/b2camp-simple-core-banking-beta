package com.adasoraninda.transactionservice.service.impl;

import com.adasoraninda.transactionservice.controller.code.MutationCode;

import java.math.BigDecimal;

public class CreditTransaction implements MutationTransaction {

    private final BigDecimal currentBalance;
    private final BigDecimal amount;

    public CreditTransaction(BigDecimal currentBalance, BigDecimal amount) {
        this.currentBalance = currentBalance;
        this.amount = amount;
    }

    @Override
    public BigDecimal getBalance() {
        return currentBalance.add(amount);
    }

    @Override
    public MutationCode getMutation() {
        return MutationCode.CREDIT;
    }
}
