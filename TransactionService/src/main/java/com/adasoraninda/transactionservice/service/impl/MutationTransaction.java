package com.adasoraninda.transactionservice.service.impl;

import com.adasoraninda.transactionservice.controller.code.MutationCode;

import java.math.BigDecimal;

public interface MutationTransaction {
    BigDecimal getBalance();

    MutationCode getMutation();
}
