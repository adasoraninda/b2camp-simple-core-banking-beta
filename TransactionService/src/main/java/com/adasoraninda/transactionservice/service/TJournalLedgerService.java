package com.adasoraninda.transactionservice.service;

import com.adasoraninda.transactionservice.model.request.TJournalLedgerRequest;
import com.adasoraninda.transactionservice.model.response.TJournalLedgerResponse;

import java.util.List;

public interface TJournalLedgerService {

    List<TJournalLedgerResponse> getAllJournalLedger();

    TJournalLedgerResponse getJournalLedgerById(Long jlId);

    TJournalLedgerResponse createJournalLedger(TJournalLedgerRequest jlRequest, String role);

    TJournalLedgerResponse updateJournalLedgerById(
            Long jlId,
            TJournalLedgerRequest jlRequest,
            String role);

    void deleteJournalLedgerById(Long jlId,
                                 String role);

}
