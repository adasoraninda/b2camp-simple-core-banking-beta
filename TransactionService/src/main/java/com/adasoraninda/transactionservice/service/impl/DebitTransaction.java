package com.adasoraninda.transactionservice.service.impl;

import com.adasoraninda.transactionservice.controller.code.MutationCode;
import com.adasoraninda.transactionservice.handle.exception.BalanceException;
import com.adasoraninda.transactionservice.handle.exception.code.BalanceExceptionCode;

import java.math.BigDecimal;

public class DebitTransaction implements MutationTransaction {

    private final BigDecimal currentBalance;
    private final BigDecimal amount;

    public DebitTransaction(BigDecimal currentBalance, BigDecimal amount) {
        this.currentBalance = currentBalance;
        this.amount = amount;
    }

    @Override
    public BigDecimal getBalance() {
        if (currentBalance.compareTo(amount) < 0) {
            throw new BalanceException(
                    BalanceExceptionCode.BALANCE_NOT_ENOUGH,
                    "Saldo anda tidak cukup");
        }

        return currentBalance.subtract(amount);
    }

    @Override
    public MutationCode getMutation() {
        return MutationCode.DEBIT;
    }
}
