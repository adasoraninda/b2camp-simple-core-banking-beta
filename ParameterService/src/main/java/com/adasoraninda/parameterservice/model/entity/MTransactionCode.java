package com.adasoraninda.parameterservice.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_transaction_code")
@EqualsAndHashCode(callSuper = true)
public class MTransactionCode extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(nullable = false, unique=true)
    private Integer transactionCode;

    @Column(nullable = false, unique=true)
    private String transactionName;

    public void update(MTransactionCode newTransactionCode, String role) {
        super.update(role);
        this.transactionCode = newTransactionCode.getTransactionCode();
        this.transactionName = newTransactionCode.getTransactionName();
    }

}
