package com.adasoraninda.parameterservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MDepositProductRequest {

    @NotNull(message = "Nama harus di isi")
    @NotEmpty(message = "Nama tidak boleh kosong")
    @NotBlank(message = "Nama tidak boleh hanya spasi")
    private String name;

    @NotNull(message = "Persentase bunga harus di isi")
    private Double interestRate;

    @NotNull(message = "Kode harus di isi")
    @NotEmpty(message = "Kode tidak boleh kosong")
    @NotBlank(message = "Kode tidak boleh hanya spasi")
    private String code;

    @NotNull(message = "COA Id harus di isi")
    private Long coaId;

}
