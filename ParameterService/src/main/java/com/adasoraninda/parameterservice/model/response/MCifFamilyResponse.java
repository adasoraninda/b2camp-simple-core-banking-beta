package com.adasoraninda.parameterservice.model.response;

import lombok.Data;

@Data
public class MCifFamilyResponse {
    private Long id;
    private String name;
    private String type;
    private MCifResponse cif;
}
