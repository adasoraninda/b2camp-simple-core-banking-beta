package com.adasoraninda.parameterservice.model.response;

import lombok.Data;

@Data
public class MCoaResponse {
    private Long id;
    private String coaCode;
    private String name;
    private String description;
}
