package com.adasoraninda.parameterservice.model.request;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class MCifRequest {

    @NotNull(message = "NIK harus di isi")
    @NotEmpty(message = "NIK tidak boleh kosong")
    @NotBlank(message = "NIK tidak boleh hanya karakter spasi")
    @Size(max = 20, message = "NIK tidak boleh lebih dari 20 karakter")
    private String idKtp;

    @NotNull(message = "Nama harus di isi")
    @NotEmpty(message = "Nama tidak boleh kosong")
    @NotBlank(message = "Nama tidak boleh hanya karakter spasi")
    private String name;

    private String npwp;

    @NotNull(message = "Nomor telepon harus di isi")
    @NotEmpty(message = "Nomor telepon tidak boleh kosong")
    @NotBlank(message = "Nomor telepon tidak boleh hanya karakter spasi")
    @Size(max = 15, message = "Nomor telepon hanya berukuran sampai 15 karakter")
    private String noTelepon;

    @Email(message = "Email tidak valid")
    private String email;

    @NotNull(message = "Tipe harus di isi")
    @NotEmpty(message = "Tipe tidak boleh kosong")
    @NotBlank(message = "Tipe tidak boleh hanya karakter spasi")
    private String type;

}
