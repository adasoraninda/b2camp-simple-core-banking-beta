package com.adasoraninda.parameterservice.model.response;

import lombok.Data;

@Data
public class MDepositProductResponse {
    private Long id;
    private String name;
    private Double interestRate;
    private String code;
    private MCoaResponse coa;
}
