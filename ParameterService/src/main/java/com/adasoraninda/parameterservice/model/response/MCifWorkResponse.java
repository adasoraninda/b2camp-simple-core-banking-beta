package com.adasoraninda.parameterservice.model.response;

import lombok.Data;

@Data
public class MCifWorkResponse {
    private Long id;
    private String name;
    private String type;
    private Long penghasilan;
    private MCifResponse cif;
}
