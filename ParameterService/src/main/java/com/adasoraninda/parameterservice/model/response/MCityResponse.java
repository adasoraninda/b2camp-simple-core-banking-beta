package com.adasoraninda.parameterservice.model.response;

import lombok.Data;

@Data
public class MCityResponse {
    private Long id;
    private String name;
    private String description;
}
