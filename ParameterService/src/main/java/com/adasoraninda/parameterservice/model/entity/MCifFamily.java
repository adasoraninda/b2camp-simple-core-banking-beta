package com.adasoraninda.parameterservice.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_cif_family")
@EqualsAndHashCode(callSuper = true)
public class MCifFamily extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(nullable = false, unique = true)
  private Long id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String type;

  @ManyToOne
  @JoinColumn(name = "cif_id", referencedColumnName = "id", nullable = false)
  private MCif cif;

  public void create(MCif cif, String role) {
    super.create(role);
    this.cif = cif;
  }

  public void update(MCifFamily newFamily, MCif cif, String role) {
    super.update(role);
    this.name = newFamily.getName();
    this.type = newFamily.getType();
    this.cif = cif;
  }
}
