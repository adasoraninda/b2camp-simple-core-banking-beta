package com.adasoraninda.parameterservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MCoaRequest {

    @NotNull(message = "Kode COA harus di isi")
    @NotEmpty(message = "Kode COA tidak boleh kosong")
    @NotBlank(message = "Kode COA tidak boleh hanya spasi")
    private String coaCode;

    @NotNull(message = "Nama harus di isi")
    @NotEmpty(message = "Nama tidak boleh kosong")
    @NotBlank(message = "Nama tidak boleh hanya spasi")
    private String name;

    @NotNull(message = "Deskripsi harus di isi")
    @NotEmpty(message = "Deskripsi tidak boleh kosong")
    @NotBlank(message = "Deskripsi tidak boleh hanya spasi")
    private String description;

}
