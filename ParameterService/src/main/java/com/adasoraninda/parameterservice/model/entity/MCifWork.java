package com.adasoraninda.parameterservice.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_cif_work")
@EqualsAndHashCode(callSuper = true)
public class MCifWork extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(nullable = false, unique = true)
  private Long id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String type;

  @Column(nullable = false)
  private Long penghasilan;

  @ManyToOne
  @JoinColumn(name = "cif_id", referencedColumnName = "id", nullable = false)
  private MCif cif;

  public void create(MCif cif, String role) {
    super.create(role);
    this.cif = cif;
  }

  public void update(MCifWork newWork, MCif cif, String role) {
    super.update(role);
    this.name = newWork.getName();
    this.type = newWork.getType();
    this.penghasilan = newWork.getPenghasilan();
    this.cif = cif;
  }
}
