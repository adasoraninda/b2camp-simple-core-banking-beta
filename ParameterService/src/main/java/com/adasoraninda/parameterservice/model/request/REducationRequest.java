package com.adasoraninda.parameterservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class REducationRequest {

    @NotNull(message = "Nama pendidikan harus di isi")
    @NotEmpty(message = "Nama pendidikan tidak boleh kosong")
    @NotBlank(message = "Nama pendidikan tidak boleh hanya spasi")
    @Size(max = 20, message = "Nama pendidikan tidak boleh lebih dari 20 karakter")
    private String name;

    @Size(max = 50, message = "Deskripsi pendidikan tidak boleh lebih dari 50 karakter")
    private String description = "";
}
