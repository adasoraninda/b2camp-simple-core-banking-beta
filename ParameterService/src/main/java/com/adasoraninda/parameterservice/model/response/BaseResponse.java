package com.adasoraninda.parameterservice.model.response;

import lombok.Getter;

@Getter
public class BaseResponse<T> {

    private final String code;
    private final String message;
    private final T data;

    private BaseResponse(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> BaseResponse<T> success(String message, T data) {
        return new BaseResponse<>(
                ResponseCode.SUCCESS.name(),
                message,
                data);
    }

    public static <T> BaseResponse<T> success(T data) {
        return new BaseResponse<>(
                ResponseCode.SUCCESS.name(),
                ResponseCode.SUCCESS.name(),
                data);
    }

    public static BaseResponse<Object> error(String message) {
        return new BaseResponse<>(
                ResponseCode.ERROR.name(),
                message,
                null);
    }

    public static BaseResponse<Object> error(String code, String message) {
        return new BaseResponse<>(
                code,
                message,
                null);
    }

    public enum ResponseCode {
        SUCCESS, ERROR;
    }

}


