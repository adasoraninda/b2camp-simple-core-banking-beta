package com.adasoraninda.parameterservice.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_cif")
@EqualsAndHashCode(callSuper = true)
public class MCif extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(nullable = false, unique = true)
  private Long id;

  @Column(name = "id_ktp", length = 20, nullable = false, unique = true)
  private String idKtp;

  @Column(nullable = false)
  private String name;

  @Column(unique = true)
  private String npwp;

  @Column(name = "no_telephone", length = 15, nullable = false, unique = true)
  private String noTelepon;

  @Column(unique = true)
  private String email;

  @Column(nullable = false)
  private String type;

  public void update(
      String idKtp,
      String name,
      String npwp,
      String noTelepon,
      String email,
      String type,
      String role) {
    super.update(role);
    this.idKtp = idKtp;
    this.name = name;
    this.npwp = npwp;
    this.noTelepon = noTelepon;
    this.email = email;
    this.type = type;
  }

}
