package com.adasoraninda.parameterservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class MCityRequest {

    @NotNull(message = "Nama kota harus di isi")
    @NotEmpty(message = "Nama kota tidak boleh kosong")
    @NotBlank(message = "Nama kota tidak boleh hanya spasi")
    @Size(max = 50, message = "Nama kota tidak boleh lebih dari 50 karakter")
    private String name;

    @Size(max = 100, message = "Deskripsi kota tidak boleh lebih dari 100")
    private String description;

}
