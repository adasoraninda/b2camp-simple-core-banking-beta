package com.adasoraninda.parameterservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RMaritalStatusRequest {

    @NotNull(message = "Nama status pernikahan harus di isi")
    @NotEmpty(message = "Nama status pernikahan tidak boleh kosong")
    @NotBlank(message = "Nama status pernikahan tidak boleh hanya spasi")
    @Size(max = 20, message = "Nama status pernikahan tidak boleh lebih dari 20 karakter")
    private String name;

    @Size(max = 50, message = "Deskripsi status pernikahan tidak boleh lebih dari 50 karakter")
    private String description = "";
}
