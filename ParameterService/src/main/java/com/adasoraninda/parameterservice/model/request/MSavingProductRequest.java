package com.adasoraninda.parameterservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MSavingProductRequest {

    @NotNull(message = "Nama produk harus di isi")
    @NotEmpty(message = "Nama produk tidak boleh kosong")
    @NotBlank(message = "Nama produk tidak boleh hanya spasi")
    private String productName;

    @NotNull(message = "Deskrips produk harus di isi")
    @NotEmpty(message = "Deskrips produk tidak boleh kosong")
    @NotBlank(message = "Deskrips produk tidak boleh hanya spasi")
    private String description;

    @NotNull(message = "COA Id harus di isi")
    private Long coaId;

}
