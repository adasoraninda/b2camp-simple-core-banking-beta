package com.adasoraninda.parameterservice.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_deposit_product")
@EqualsAndHashCode(callSuper = true)
public class MDepositProduct extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(nullable = false, updatable = false)
  private Long id;

  @Column(nullable = false, unique = true)
  private String name;

  @Column(nullable = false)
  private Double interestRate;

  @Column(nullable = false, unique = true)
  private String code;

  @ManyToOne
  @JoinColumn(name = "coa_id", nullable = false)
  private MCoa coa;

  public void create(MCoa coa, String role) {
    super.create(role);
    this.coa = coa;
  }

  public void update(String name, Double interestRate, String code, MCoa coa, String role) {
    super.update(role);
    this.name = name;
    this.interestRate = interestRate;
    this.code = code;
    this.coa = coa;
  }

}
