package com.adasoraninda.parameterservice.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_coa")
@EqualsAndHashCode(callSuper = true)
public class MCoa extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(nullable = false, updatable = false)
  private Long id;

  @Column(nullable = false, unique = true)
  private String coaCode;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String description;

  public void update(String coaCode, String name, String description, String role) {
    super.update(role);
    this.coaCode = coaCode;
    this.name = name;
    this.description = description;
  }

}
