package com.adasoraninda.parameterservice.model.response;

import lombok.Data;

@Data
public class MSavingProductResponse {
    private Long id;
    private String productName;
    private String description;
    private MCoaResponse coa;
}
