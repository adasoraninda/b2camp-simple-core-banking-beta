package com.adasoraninda.parameterservice.model.request;

import com.adasoraninda.parameterservice.controller.code.TransactionCode;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MTransactionCodeRequest {

    /**
     * Isi dari variabel bisa dilihat disini
     *
     * @see TransactionCode
     */
    @NotNull(message = "Kode transaksi harus di isi")
    private Integer transactionCode;

    @NotNull(message = "Nama transaksi harus di isi")
    @NotEmpty(message = "Nama transaksi tidak boleh kosong")
    @NotBlank(message = "Nama transaksi tidak boleh hanya spasi")
    private String transactionName;

}
