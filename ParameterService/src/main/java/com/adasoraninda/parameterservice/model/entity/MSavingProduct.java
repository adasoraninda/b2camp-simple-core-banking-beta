package com.adasoraninda.parameterservice.model.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "m_saving_product")
@EqualsAndHashCode(callSuper = true)
public class MSavingProduct extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(nullable = false, unique = true)
  private Long id;

  @Column(nullable = false, unique = true)
  private String productName;

  @Column(nullable = false)
  private String description;

  @ManyToOne
  @JoinColumn(name = "coa_id", nullable = false)
  private MCoa coa;

  public void create(MCoa coa, String role) {
    super.create(role);
    this.coa = coa;
  }

  public void update(String productName, String description, MCoa coa, String role) {
    super.update(role);
    this.productName = productName;
    this.description = description;
    this.coa = coa;
  }

}
