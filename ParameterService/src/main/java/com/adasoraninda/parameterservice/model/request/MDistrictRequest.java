package com.adasoraninda.parameterservice.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class MDistrictRequest {

    @NotNull(message = "Nama distrik harus di isi")
    @NotEmpty(message = "Nama distrik tidak boleh kosong")
    @NotBlank(message = "Nama distrik tidak boleh hanya spasi")
    @Size(max = 50, message = "Nama distrik tidak boleh lebih dari 50 karakter")
    private String name;

    @Size(max = 100, message = "Deskripsi distrik tidak boleh lebih dari 100")
    private String description = "";

}
