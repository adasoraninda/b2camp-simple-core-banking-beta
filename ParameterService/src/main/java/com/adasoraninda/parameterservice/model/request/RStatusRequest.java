package com.adasoraninda.parameterservice.model.request;

import com.adasoraninda.parameterservice.controller.code.StatusCode;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class RStatusRequest {

    /**
     * Isi dari variabel bisa dilihat disini
     *
     * @see StatusCode
     */
    @NotNull(message = "Kode harus di isi")
    @NotEmpty(message = "Kode tidak boleh kosong")
    @NotBlank(message = "Kode tidak boleh hanya spasi")
    private String code;

    @NotNull(message = "Nama harus di isi")
    @NotEmpty(message = "Nama tidak boleh kosong")
    @NotBlank(message = "Nama tidak boleh hanya spasi")
    private String name;
}
