package com.adasoraninda.parameterservice.model.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity {

  @Column(name = "created_by", nullable = false, length = 20)
  private String createdBy;

  @Column(name = "created_date", nullable = false)
  private LocalDateTime createdDate;

  @Column(name = "updated_by", nullable = false, length = 20)
  private String updatedBy;

  @Column(name = "updated_date", nullable = false)
  private LocalDateTime updatedDate;

  @Column(name = "is_deleted", nullable = false)
  private Boolean isDeleted = Boolean.FALSE;

  /**
   * Shortcut untuk membuat entitas, supaya tidak perlu set di service
   */
  public void create(String role) {
    this.updatedDate = this.createdDate = LocalDateTime.now();
    this.updatedBy = this.createdBy = role;
  }

  /**
   * Shortcut untuk merubah entitas, supaya tidak perlu set di service
   */
  public void update(String role) {
    this.updatedBy = role;
    this.updatedDate = LocalDateTime.now();
  }

}
