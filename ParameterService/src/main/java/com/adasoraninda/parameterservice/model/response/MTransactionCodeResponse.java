package com.adasoraninda.parameterservice.model.response;

import lombok.Data;

@Data
public class MTransactionCodeResponse {
    private Long id;
    private Integer transactionCode;
    private String transactionName;
}
