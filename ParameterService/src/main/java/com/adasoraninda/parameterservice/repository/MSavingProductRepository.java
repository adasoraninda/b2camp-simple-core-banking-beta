package com.adasoraninda.parameterservice.repository;

import com.adasoraninda.parameterservice.model.entity.MSavingProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MSavingProductRepository extends JpaRepository<MSavingProduct, Long> {

    @Query(value = "SELECT * FROM m_saving_product " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MSavingProduct> findAllSavingProduct();

    @Query(value = "SELECT * FROM m_saving_product " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MSavingProduct> findSavingProductById(Long id);

    @Query(value = "SELECT * FROM m_saving_product " +
            "WHERE is_deleted = false " +
            "AND product_name = ?1",
            nativeQuery = true)
    Optional<MSavingProduct> findSavingProductByName(String name);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_saving_product " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    boolean existsSavingProductById(Long id);

    @Modifying
    @Query(value = "UPDATE m_saving_product " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
