package com.adasoraninda.parameterservice.repository;


import com.adasoraninda.parameterservice.model.entity.REducation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface REducationRepository extends JpaRepository<REducation, Long> {
}
