package com.adasoraninda.parameterservice.repository;

import com.adasoraninda.parameterservice.model.entity.MCifFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MCifFamilyRepository extends JpaRepository<MCifFamily, Long> {
    @Query(value = "SELECT * FROM m_cif_family " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MCifFamily> findAllCifFamily();

    @Query(value = "SELECT * FROM m_cif_family " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MCifFamily> findCifFamilyById(Long id);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_cif_family " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existCifFamilyById(Long id);

    @Modifying
    @Query(value = "UPDATE m_cif_family " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime dateTime);

}
