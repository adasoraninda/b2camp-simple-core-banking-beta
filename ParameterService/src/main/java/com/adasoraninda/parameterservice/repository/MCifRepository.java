package com.adasoraninda.parameterservice.repository;


import com.adasoraninda.parameterservice.model.entity.MCif;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MCifRepository extends JpaRepository<MCif, Long> {

    @Query(value = "SELECT * FROM m_cif " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MCif> findAllCif();

    @Query(value = "SELECT * FROM m_cif " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MCif> findCifById(Long id);

    @Query(value = "SELECT * FROM m_cif " +
            "WHERE is_deleted = false " +
            "AND id_ktp = ?1",
            nativeQuery = true)
    Optional<MCif> findCifByNik(String nik);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_cif " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existCifById(Long id);

    @Modifying
    @Query(value = "UPDATE m_cif " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime dateTime);

}
