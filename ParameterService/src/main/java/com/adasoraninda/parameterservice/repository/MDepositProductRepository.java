package com.adasoraninda.parameterservice.repository;

import com.adasoraninda.parameterservice.model.entity.MDepositProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MDepositProductRepository extends JpaRepository<MDepositProduct, Long> {

    @Query(value = "SELECT * FROM m_deposit_product " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MDepositProduct> findAllDepositProduct();

    @Query(value = "SELECT * FROM m_deposit_product " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MDepositProduct> findDepositProductById(Long id);

    @Query(value = "SELECT * FROM m_deposit_product " +
            "WHERE is_deleted = false " +
            "AND name = ?1",
            nativeQuery = true)
    Optional<MDepositProduct> findDepositProductByName(String name);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_deposit_product " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    boolean existsDepositProductById(Long id);

    @Modifying
    @Query(value = "UPDATE m_deposit_product " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);


}
