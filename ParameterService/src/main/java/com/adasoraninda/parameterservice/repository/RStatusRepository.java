package com.adasoraninda.parameterservice.repository;


import com.adasoraninda.parameterservice.model.entity.RStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RStatusRepository extends JpaRepository<RStatus, Long> {

    @Query(value = "SELECT * FROM r_status WHERE code = ?1",
            nativeQuery = true)
    Optional<RStatus> getStatusByCode(String code);

}
