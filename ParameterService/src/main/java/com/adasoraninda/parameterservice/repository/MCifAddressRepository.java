package com.adasoraninda.parameterservice.repository;

import com.adasoraninda.parameterservice.model.entity.MCifAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MCifAddressRepository extends JpaRepository<MCifAddress, Long> {

    @Query(value = "SELECT * FROM m_cif_address " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MCifAddress> findAllCifAddress();

    @Query(value = "SELECT * FROM m_cif_address " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MCifAddress> findCifAddressById(Long id);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_cif_address " +
            "WHERE id = ?1",
            nativeQuery = true)
    boolean existCifAddressById(Long id);

    @Modifying
    @Query(value = "UPDATE m_cif_address " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime dateTime);

}
