package com.adasoraninda.parameterservice.repository;

import com.adasoraninda.parameterservice.model.entity.MCoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MCoaRepository extends JpaRepository<MCoa, Long> {

    @Query(value = "SELECT * FROM m_coa " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MCoa> findAllCoa();

    @Query(value = "SELECT * FROM m_coa " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MCoa> findCoaById(Long id);

    @Query(value = "SELECT * FROM m_coa " +
            "WHERE is_deleted = false " +
            "AND coa_code = ?1",
            nativeQuery = true)
    Optional<MCoa> findCoaByCode(String code);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_coa " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    boolean existsCoaById(Long id);

    @Modifying
    @Query(value = "UPDATE m_coa " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);


}
