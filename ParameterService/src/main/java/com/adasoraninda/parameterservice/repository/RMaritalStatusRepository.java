package com.adasoraninda.parameterservice.repository;


import com.adasoraninda.parameterservice.model.entity.RMaritalStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RMaritalStatusRepository extends JpaRepository<RMaritalStatus, Long> {
}
