package com.adasoraninda.parameterservice.repository;

import com.adasoraninda.parameterservice.model.entity.MDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MDistrictRepository extends JpaRepository<MDistrict, Long> {

    @Query(value = "SELECT * FROM m_district " +
            "WHERE is_deleted = false",
            nativeQuery = true)
    List<MDistrict> findAllDistrict();

    @Query(value = "SELECT * FROM m_district " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    Optional<MDistrict> findDistrictById(Long id);

    @Query(value = "SELECT COUNT(*) > 0 FROM m_district " +
            "WHERE is_deleted = false " +
            "AND id = ?1",
            nativeQuery = true)
    boolean existsDistrictById(Long id);

    @Modifying
    @Query(value = "UPDATE m_district " +
            "SET updated_by = ?2, " +
            "updated_date = ?3, " +
            "is_deleted = true " +
            "WHERE id = ?1",
            nativeQuery = true)
    void softDelete(Long id, String role, LocalDateTime date);

}
