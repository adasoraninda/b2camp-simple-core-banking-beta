package com.adasoraninda.parameterservice.repository;


import com.adasoraninda.parameterservice.model.entity.RReligion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RReligionRepository extends JpaRepository<RReligion, Long> {
}
