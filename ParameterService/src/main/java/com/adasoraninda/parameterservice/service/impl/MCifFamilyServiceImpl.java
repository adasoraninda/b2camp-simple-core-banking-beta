package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.message.MCifErrorMessage;
import com.adasoraninda.parameterservice.handle.exception.message.MCifFamilyErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.MCifFamilySuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MCifFamily;
import com.adasoraninda.parameterservice.model.request.MCifFamilyRequest;
import com.adasoraninda.parameterservice.model.response.MCifFamilyResponse;
import com.adasoraninda.parameterservice.repository.MCifFamilyRepository;
import com.adasoraninda.parameterservice.repository.MCifRepository;
import com.adasoraninda.parameterservice.service.MCifFamilyService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.parameterservice.handle.exception.code.MCifErrorCode.CIF_IS_NOT_FOUND;
import static com.adasoraninda.parameterservice.handle.exception.code.MCifFamilyErrorCode.CIF_FAMILY_IS_EMPTY;
import static com.adasoraninda.parameterservice.handle.exception.code.MCifFamilyErrorCode.CIF_FAMILY_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class MCifFamilyServiceImpl implements MCifFamilyService {

    private final ModelMapper modelMapper;

    private final MCifRepository cifRepository;
    private final MCifFamilyRepository familyRepository;

    @Override
    public AppSuccessHandler<List<MCifFamilyResponse>> getFamilies() {
        var families = familyRepository.findAllCifFamily();

        if (families.isEmpty()) {
            throw throwEmptyFamilies();
        }

        var message = new MCifFamilySuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = families.stream()
                .map(f -> modelMapper.map(f, MCifFamilyResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MCifFamilyResponse> getFamilyById(Long familyId) {
        var data = familyRepository.findCifFamilyById(familyId)
                .map(f -> modelMapper.map(f, MCifFamilyResponse.class))
                .orElseThrow(() -> throwFamilyNotFound(familyId));
        var message = new MCifFamilySuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifFamilyResponse> createFamily(MCifFamilyRequest familyRequest, String role) {
        var cif = cifRepository.findCifById(familyRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(familyRequest.getCifId()));

        var family = modelMapper.typeMap(MCifFamilyRequest.class, MCifFamily.class)
                .addMappings(mapper -> mapper.skip(MCifFamily::setId))
                .map(familyRequest);

        family.create(cif, role);

        var message = new MCifFamilySuccessMessage(AppSuccessCode.CREATE_DATA);
        var data = modelMapper.map(
                familyRepository.save(family),
                MCifFamilyResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifFamilyResponse> updateFamily(Long familyId, String role, MCifFamilyRequest familyRequest) {
        var cif = cifRepository.findCifById(familyRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(familyRequest.getCifId()));

        var oldFamily = familyRepository.findCifFamilyById(familyId)
                .orElseThrow(() -> throwFamilyNotFound(familyId));

        oldFamily.update(modelMapper.map(familyRequest, MCifFamily.class), cif, role);

        var message = new MCifFamilySuccessMessage(AppSuccessCode.UPDATE_DATA);
        var data = modelMapper.map(
                familyRepository.save(oldFamily),
                MCifFamilyResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteFamilyById(Long familyId, String role) {
        var familyIsExists = familyRepository.existCifFamilyById(familyId);

        if (!familyIsExists) {
            throw throwFamilyNotFound(familyId);
        }

        familyRepository.softDelete(familyId, role, LocalDateTime.now());

        var message = new MCifFamilySuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    private RuntimeException throwEmptyFamilies() {
        return new BusinessException(
                new MCifFamilyErrorMessage(
                        Map.of(CIF_FAMILY_IS_EMPTY, new Object()),
                        CIF_FAMILY_IS_EMPTY));
    }

    private RuntimeException throwFamilyNotFound(Long familyId) {
        return new BusinessException(
                new MCifFamilyErrorMessage(
                        Map.of(CIF_FAMILY_IS_NOT_FOUND, familyId),
                        CIF_FAMILY_IS_NOT_FOUND));
    }

    private RuntimeException throwCifNotFound(Long cifId) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_IS_NOT_FOUND, cifId),
                        CIF_IS_NOT_FOUND));
    }

}
