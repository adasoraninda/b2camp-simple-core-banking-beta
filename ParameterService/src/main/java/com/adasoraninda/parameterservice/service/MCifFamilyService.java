package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.request.MCifFamilyRequest;
import com.adasoraninda.parameterservice.model.response.MCifFamilyResponse;

import java.util.List;

public interface MCifFamilyService {

    AppSuccessHandler<List<MCifFamilyResponse>> getFamilies();

    AppSuccessHandler<MCifFamilyResponse> getFamilyById(Long familyId);

    AppSuccessHandler<MCifFamilyResponse> createFamily(MCifFamilyRequest familyRequest, String role);

    AppSuccessHandler<MCifFamilyResponse> updateFamily(Long familyId, String role, MCifFamilyRequest familyRequest);

    AppSuccessHandler<Object> deleteFamilyById(Long familyId, String role);

}
