package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.code.MCityErrorCode;
import com.adasoraninda.parameterservice.handle.exception.message.MCityErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.MCitySuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MCity;
import com.adasoraninda.parameterservice.model.request.MCityRequest;
import com.adasoraninda.parameterservice.model.response.MCityResponse;
import com.adasoraninda.parameterservice.repository.MCityRepository;
import com.adasoraninda.parameterservice.service.MCityService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MCityServiceImpl implements MCityService {

    private final ModelMapper modelMapper;
    private final MCityRepository repository;

    @Override
    public AppSuccessHandler<List<MCityResponse>> getAllCities() {
        var cities = repository.findAllCity();

        if (cities.isEmpty()) {
            throw new BusinessException(new MCityErrorMessage(
                    Map.of(MCityErrorCode.CITY_IS_EMPTY, new Object()),
                    MCityErrorCode.CITY_IS_EMPTY));
        }

        var message = new MCitySuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = cities.stream()
                .map(c -> modelMapper.map(c, MCityResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MCityResponse> getCityById(Long cityId) {
        var data = repository.findCityById(cityId)
                .map(c -> modelMapper.map(c, MCityResponse.class))
                .orElseThrow(() -> new BusinessException(new MCityErrorMessage(
                        Map.of(MCityErrorCode.CITY_IS_NOT_FOUND, cityId),
                        MCityErrorCode.CITY_IS_NOT_FOUND)));
        var message = new MCitySuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCityResponse> createCity(MCityRequest cityRequest, String role) {
        var city = modelMapper.map(cityRequest, MCity.class);
        city.create(role);

        var message = new MCitySuccessMessage(AppSuccessCode.CREATE_DATA);
        var data = modelMapper.map(
                repository.save(city),
                MCityResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCityResponse> updateCity(Long cityId, MCityRequest cityRequest, String role) {
        var city = repository.findCityById(cityId)
                .orElseThrow(() -> new BusinessException(new MCityErrorMessage(
                        Map.of(MCityErrorCode.CITY_IS_NOT_FOUND, cityId),
                        MCityErrorCode.CITY_IS_NOT_FOUND)));

        city.update(cityRequest.getName(),
                cityRequest.getDescription(),
                role);

        var message = new MCitySuccessMessage(AppSuccessCode.UPDATE_DATA);
        var data = modelMapper.map(
                repository.save(city),
                MCityResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteCity(Long cityId, String role) {
        var isCityExists = repository.existsCityById(cityId);

        if (!isCityExists) {
            throw new BusinessException(new MCityErrorMessage(
                    Map.of(MCityErrorCode.CITY_IS_EMPTY, new Object()),
                    MCityErrorCode.CITY_IS_EMPTY));
        }

        repository.softDelete(cityId, role, LocalDateTime.now());

        var message = new MCitySuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public MCity getCityEntityById(Long cityId) {
        return repository.findCityById(cityId)
                .orElseThrow(() -> new BusinessException(new MCityErrorMessage(
                        Map.of(MCityErrorCode.CITY_IS_NOT_FOUND, cityId),
                        MCityErrorCode.CITY_IS_NOT_FOUND)));
    }

}
