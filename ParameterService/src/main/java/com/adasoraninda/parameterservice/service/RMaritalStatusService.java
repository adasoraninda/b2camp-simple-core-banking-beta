package com.adasoraninda.parameterservice.service;


import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.RMaritalStatus;
import com.adasoraninda.parameterservice.model.request.RMaritalStatusRequest;
import com.adasoraninda.parameterservice.model.response.RMaritalStatusResponse;

import java.util.List;

public interface RMaritalStatusService {

    AppSuccessHandler<List<RMaritalStatusResponse>> getAllMaritalStatus();

    AppSuccessHandler<RMaritalStatusResponse> getMaritalStatusById(Long maritalStatusId);

    AppSuccessHandler<RMaritalStatusResponse> createMaritalStatus(RMaritalStatusRequest maritalStatusRequest);

    AppSuccessHandler<RMaritalStatusResponse> updateMaritalStatusById(Long maritalStatusId, RMaritalStatusRequest maritalStatusRequest);

    AppSuccessHandler<RMaritalStatusResponse> deleteMaritalStatusById(Long maritalStatusId);

    RMaritalStatus getMaritalStatusEntityById(Long maritalStatusId);

}
