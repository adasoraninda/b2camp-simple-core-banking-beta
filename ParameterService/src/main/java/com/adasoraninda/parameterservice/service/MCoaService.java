package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.MCoa;
import com.adasoraninda.parameterservice.model.request.MCoaRequest;
import com.adasoraninda.parameterservice.model.response.MCoaResponse;

import java.util.List;

public interface MCoaService {

    AppSuccessHandler<List<MCoaResponse>> getAllCoa();

    AppSuccessHandler<MCoaResponse> getCoaById(Long coaId);

    AppSuccessHandler<MCoaResponse> createCoa(MCoaRequest coaRequest, String role);

    AppSuccessHandler<MCoaResponse> updateCoa(Long coaId, MCoaRequest coaRequest, String role);

    AppSuccessHandler<Object> deleteCoa(Long coaId, String role);

    MCoa getCoaEntityById(Long coaId);

    MCoa getCoaEntityByCode(String code);

}
