package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.controller.code.TransactionCode;
import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.message.MTransactionCodeErrorMessage;
import com.adasoraninda.parameterservice.handle.success.message.MTransactionCodeSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MTransactionCode;
import com.adasoraninda.parameterservice.model.request.MTransactionCodeRequest;
import com.adasoraninda.parameterservice.model.response.MTransactionCodeResponse;
import com.adasoraninda.parameterservice.repository.MTransactionCodeRepository;
import com.adasoraninda.parameterservice.service.MTransactionCodeService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.parameterservice.handle.exception.code.MTransactionCodeErrorCode.*;
import static com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode.*;

@Service
@AllArgsConstructor
public class MTransactionCodeServiceImpl implements MTransactionCodeService {

    private final ModelMapper modelMapper;
    private final MTransactionCodeRepository transactionCodeRepository;

    @Override
    public AppSuccessHandler<List<MTransactionCodeResponse>> getAllTransactionCode() {
        var allTransactionCode = transactionCodeRepository.findAllTransactionCode();

        if (allTransactionCode.isEmpty()) {
            throw new BusinessException(new MTransactionCodeErrorMessage(
                    Map.of(TRANSACTION_CODE_IS_EMPTY, new Object()),
                    TRANSACTION_CODE_IS_EMPTY));
        }

        var message = new MTransactionCodeSuccessMessage(GET_ALL_DATA);
        var data = allTransactionCode.stream()
                .map(tc -> modelMapper.map(tc, MTransactionCodeResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MTransactionCodeResponse> getTransactionCodeById(Long transactionCodeId) {
        var data = transactionCodeRepository.findTransactionCodeById(transactionCodeId)
                .map(tc -> modelMapper.map(tc, MTransactionCodeResponse.class))
                .orElseThrow(() -> new BusinessException(new MTransactionCodeErrorMessage(
                        Map.of(TRANSACTION_CODE_IS_NOT_FOUND, transactionCodeId),
                        TRANSACTION_CODE_IS_NOT_FOUND)));
        var message = new MTransactionCodeSuccessMessage(GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MTransactionCodeResponse> createTransactionCode(
            MTransactionCodeRequest transactionCodeRequest,
            String role) {

        if (TransactionCode.checkCode(transactionCodeRequest.getTransactionCode())) {
            throw new BusinessException(new MTransactionCodeErrorMessage(
                    Map.of(TRANSACTION_CODE_IS_NOT_FOUND, transactionCodeRequest.getTransactionCode()),
                    TRANSACTION_CODE_IS_NOT_FOUND));
        }

        var transactionCode = modelMapper.map(transactionCodeRequest, MTransactionCode.class);
        transactionCode.create(role);

        var message = new MTransactionCodeSuccessMessage(CREATE_DATA);
        var data = modelMapper.map(
                transactionCodeRepository.save(transactionCode),
                MTransactionCodeResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MTransactionCodeResponse> updateTransactionCode(
            Long transactionCodeId,
            String role,
            MTransactionCodeRequest transactionCodeRequest) {

        if (TransactionCode.checkCode(transactionCodeRequest.getTransactionCode())) {
            throw new BusinessException(new MTransactionCodeErrorMessage(
                    Map.of(TRANSACTION_CODE_INVALID, transactionCodeRequest.getTransactionCode()),
                    TRANSACTION_CODE_INVALID));
        }

        var transactionCode = transactionCodeRepository.findTransactionCodeById(transactionCodeId)
                .orElseThrow(() -> new BusinessException(new MTransactionCodeErrorMessage(
                        Map.of(TRANSACTION_CODE_IS_NOT_FOUND, transactionCodeId),
                        TRANSACTION_CODE_IS_NOT_FOUND)));
        transactionCode.update(modelMapper.map(transactionCodeRequest, MTransactionCode.class), role);

        var message = new MTransactionCodeSuccessMessage(UPDATE_DATA);
        var data = modelMapper.map(
                transactionCodeRepository.save(transactionCode),
                MTransactionCodeResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteTransactionCodeById(Long transactionCodeId, String role) {
        var isTransactionCodeExists = transactionCodeRepository.existsTransactionCodeById(transactionCodeId);

        if (!isTransactionCodeExists) {
            throw new BusinessException(new MTransactionCodeErrorMessage(
                    Map.of(TRANSACTION_CODE_IS_NOT_FOUND, transactionCodeId),
                    TRANSACTION_CODE_IS_NOT_FOUND));
        }

        transactionCodeRepository.softDelete(
                transactionCodeId,
                role,
                LocalDateTime.now());

        var message = new MTransactionCodeSuccessMessage(DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public MTransactionCode getTransactionCodeEntityById(Long transactionCodeId) {
        return transactionCodeRepository.findTransactionCodeById(transactionCodeId)
                .orElseThrow(() -> new BusinessException(new MTransactionCodeErrorMessage(
                        Map.of(TRANSACTION_CODE_IS_NOT_FOUND, transactionCodeId),
                        TRANSACTION_CODE_IS_NOT_FOUND)));
    }

    @Override
    public MTransactionCode getTransactionCodeEntityByCode(Integer code) {
        return transactionCodeRepository.findTransactionCodeByCode(code)
                .orElseThrow(() -> new BusinessException(new MTransactionCodeErrorMessage(
                        Map.of(TRANSACTION_CODE_INVALID, code),
                        TRANSACTION_CODE_INVALID)));
    }
}
