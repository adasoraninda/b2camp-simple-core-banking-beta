package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.message.RMaritalStatusErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.RMaritalStatusSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.RMaritalStatus;
import com.adasoraninda.parameterservice.model.request.RMaritalStatusRequest;
import com.adasoraninda.parameterservice.model.response.RMaritalStatusResponse;
import com.adasoraninda.parameterservice.repository.RMaritalStatusRepository;
import com.adasoraninda.parameterservice.service.RMaritalStatusService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.parameterservice.handle.exception.code.RMaritalStatsErrorCode.MARITAL_STATUS_IS_EMPTY;
import static com.adasoraninda.parameterservice.handle.exception.code.RMaritalStatsErrorCode.MARITAL_STATUS_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class RMaritalStatusServiceImpl implements RMaritalStatusService {

    private final ModelMapper modelMapper;
    private final RMaritalStatusRepository repository;

    @Override
    public AppSuccessHandler<List<RMaritalStatusResponse>> getAllMaritalStatus() {
        var allMaritalStatus = repository.findAll();

        if (allMaritalStatus.isEmpty()) {
            throw new BusinessException(new RMaritalStatusErrorMessage(
                    Map.of(MARITAL_STATUS_IS_EMPTY, new Object()),
                    MARITAL_STATUS_IS_EMPTY));
        }

        var message = new RMaritalStatusSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = allMaritalStatus.stream()
                .map(r -> modelMapper.map(r, RMaritalStatusResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RMaritalStatusResponse> getMaritalStatusById(Long maritalStatusId) {
        var data = repository.findById(maritalStatusId)
                .map(r -> modelMapper.map(r, RMaritalStatusResponse.class))
                .orElseThrow(() -> new BusinessException(new RMaritalStatusErrorMessage(
                        Map.of(MARITAL_STATUS_IS_NOT_FOUND, maritalStatusId),
                        MARITAL_STATUS_IS_NOT_FOUND)));
        var message = new RMaritalStatusSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RMaritalStatusResponse> createMaritalStatus(RMaritalStatusRequest maritalStatusRequest) {
        var maritalStatus = modelMapper.map(maritalStatusRequest, RMaritalStatus.class);
        var data = modelMapper.map(repository.save(maritalStatus), RMaritalStatusResponse.class);
        var message = new RMaritalStatusSuccessMessage(AppSuccessCode.CREATE_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RMaritalStatusResponse> updateMaritalStatusById(Long maritalStatusId, RMaritalStatusRequest maritalStatusRequest) {
        var existsMaritalStats = repository.existsById(maritalStatusId);

        if (!existsMaritalStats) {
            throw new BusinessException(new RMaritalStatusErrorMessage(
                    Map.of(MARITAL_STATUS_IS_NOT_FOUND, maritalStatusId),
                    MARITAL_STATUS_IS_NOT_FOUND));
        }

        var maritalStatus = modelMapper.map(maritalStatusRequest, RMaritalStatus.class);
        maritalStatus.setId(maritalStatusId);

        var data = modelMapper.map(repository.save(maritalStatus), RMaritalStatusResponse.class);
        var message = new RMaritalStatusSuccessMessage(AppSuccessCode.UPDATE_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RMaritalStatusResponse> deleteMaritalStatusById(Long maritalStatusId) {
        var existsMaritalStats = repository.existsById(maritalStatusId);

        if (!existsMaritalStats) {
            throw new BusinessException(new RMaritalStatusErrorMessage(
                    Map.of(MARITAL_STATUS_IS_NOT_FOUND, maritalStatusId),
                    MARITAL_STATUS_IS_NOT_FOUND));
        }

        repository.delete(repository.getById(maritalStatusId));

        var message = new RMaritalStatusSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public RMaritalStatus getMaritalStatusEntityById(Long maritalStatusId) {
        return repository.findById(maritalStatusId)
                .orElseThrow(() -> new BusinessException(new RMaritalStatusErrorMessage(
                        Map.of(MARITAL_STATUS_IS_NOT_FOUND, maritalStatusId),
                        MARITAL_STATUS_IS_NOT_FOUND)));
    }
}
