package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.REducation;
import com.adasoraninda.parameterservice.model.request.REducationRequest;
import com.adasoraninda.parameterservice.model.response.REducationResponse;

import java.util.List;

public interface REducationService {

    AppSuccessHandler<List<REducationResponse>> getAllEducations();

    AppSuccessHandler<REducationResponse> getEducationById(Long educationId);

    AppSuccessHandler<REducationResponse> createEducation(REducationRequest educationRequest);

    AppSuccessHandler<REducationResponse> updateEducationById(Long educationId, REducationRequest educationRequest);

    AppSuccessHandler<REducationResponse> deleteEducationById(Long educationId);

    REducation getEducationEntityById(Long educationId);

}
