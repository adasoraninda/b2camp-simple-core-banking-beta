package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.code.MDistrictErrorCode;
import com.adasoraninda.parameterservice.handle.exception.message.MDistrictErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.MDistrictSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MDistrict;
import com.adasoraninda.parameterservice.model.request.MDistrictRequest;
import com.adasoraninda.parameterservice.model.response.MDistrictResponse;
import com.adasoraninda.parameterservice.repository.MDistrictRepository;
import com.adasoraninda.parameterservice.service.MDistrictService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MDistrictServiceImpl implements MDistrictService {

    private final ModelMapper modelMapper;
    private final MDistrictRepository repository;

    @Override
    public AppSuccessHandler<List<MDistrictResponse>> getAllDistricts() {
        var districts = repository.findAllDistrict();

        if (districts.isEmpty()) {
            throw new BusinessException(new MDistrictErrorMessage(
                    Map.of(MDistrictErrorCode.DISTRICT_IS_EMPTY, new Object()),
                    MDistrictErrorCode.DISTRICT_IS_EMPTY));
        }

        var message = new MDistrictSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = districts.stream()
                .map(d -> modelMapper.map(d, MDistrictResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MDistrictResponse> getDistrictById(Long districtId) {
        var district = repository.findDistrictById(districtId)
                .map(d -> modelMapper.map(d, MDistrictResponse.class))
                .orElseThrow(() -> new BusinessException(new MDistrictErrorMessage(
                        Map.of(MDistrictErrorCode.DISTRICT_IS_NOT_FOUND, districtId),
                        MDistrictErrorCode.DISTRICT_IS_NOT_FOUND)));
        var message = new MDistrictSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, district);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MDistrictResponse> createDistrict(MDistrictRequest districtRequest, String role) {
        var district = modelMapper.map(districtRequest, MDistrict.class);
        district.create(role);

        var message = new MDistrictSuccessMessage(AppSuccessCode.CREATE_DATA);
        var data = modelMapper.map(
                repository.save(district),
                MDistrictResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MDistrictResponse> updateDistrict(Long districtId, MDistrictRequest districtRequest, String role) {
        var district = repository.findDistrictById(districtId)
                .orElseThrow(() -> new BusinessException(new MDistrictErrorMessage(
                        Map.of(MDistrictErrorCode.DISTRICT_IS_NOT_FOUND, districtId),
                        MDistrictErrorCode.DISTRICT_IS_NOT_FOUND)));

        district.update(districtRequest.getName(),
                districtRequest.getDescription(),
                role);

        var message = new MDistrictSuccessMessage(AppSuccessCode.UPDATE_DATA);
        var data = modelMapper.map(
                repository.save(district),
                MDistrictResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteDistrict(Long districtId, String role) {
        var isDistrictExists = repository.existsDistrictById(districtId);

        if (!isDistrictExists) {
            throw new BusinessException(new MDistrictErrorMessage(
                    Map.of(MDistrictErrorCode.DISTRICT_IS_EMPTY, new Object()),
                    MDistrictErrorCode.DISTRICT_IS_EMPTY));
        }

        repository.softDelete(districtId, role, LocalDateTime.now());

        var message = new MDistrictSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public MDistrict getDistrictEntityById(Long districtId) {
        return repository.findDistrictById(districtId)
                .orElseThrow(() -> new BusinessException(new MDistrictErrorMessage(
                        Map.of(MDistrictErrorCode.DISTRICT_IS_NOT_FOUND, districtId),
                        MDistrictErrorCode.DISTRICT_IS_NOT_FOUND)));
    }
}
