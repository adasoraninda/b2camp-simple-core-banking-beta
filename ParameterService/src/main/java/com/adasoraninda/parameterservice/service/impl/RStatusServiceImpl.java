package com.adasoraninda.parameterservice.service.impl;


import com.adasoraninda.parameterservice.controller.code.StatusCode;
import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.code.RStatusErrorCode;
import com.adasoraninda.parameterservice.handle.exception.message.RStatusErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.RStatusSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.RStatus;
import com.adasoraninda.parameterservice.model.request.RStatusRequest;
import com.adasoraninda.parameterservice.model.response.RStatusResponse;
import com.adasoraninda.parameterservice.repository.RStatusRepository;
import com.adasoraninda.parameterservice.service.RStatusService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RStatusServiceImpl implements RStatusService {

    private final RStatusRepository repository;
    private final ModelMapper modelMapper;

    @Override
    public AppSuccessHandler<List<RStatusResponse>> getAllStatus() {
        var allStatus = repository.findAll();

        if (allStatus.isEmpty()) {
            throw new BusinessException(new RStatusErrorMessage(
                    Map.of(RStatusErrorCode.STATUS_IS_EMPTY, new Object()),
                    RStatusErrorCode.STATUS_IS_EMPTY));
        }

        var message = new RStatusSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = allStatus.stream()
                .map(s -> modelMapper.map(s, RStatusResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RStatusResponse> getStatusById(Long statusId) {
        var data = repository.findById(statusId)
                .map(s -> modelMapper.map(s, RStatusResponse.class))
                .orElseThrow(() -> new BusinessException(new RStatusErrorMessage(
                        Map.of(RStatusErrorCode.STATUS_IS_NOT_FOUND, statusId),
                        RStatusErrorCode.STATUS_IS_NOT_FOUND)));
        var message = new RStatusSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RStatusResponse> createStatus(RStatusRequest statusRequest) {
        if (StatusCode.checkStatus(statusRequest.getCode())) {
            throw new BusinessException(new RStatusErrorMessage(
                    Map.of(RStatusErrorCode.STATUS_INVALID, statusRequest.getCode()),
                    RStatusErrorCode.STATUS_INVALID));
        }

        var status = modelMapper.map(statusRequest, RStatus.class);
        var data = modelMapper.map(repository.save(status), RStatusResponse.class);
        var message = new RStatusSuccessMessage(AppSuccessCode.CREATE_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RStatusResponse> updateStatusById(Long statusId, RStatusRequest statusRequest) {
        if (StatusCode.checkStatus(statusRequest.getCode())) {
            throw new BusinessException(new RStatusErrorMessage(
                    Map.of(RStatusErrorCode.STATUS_INVALID, statusRequest.getCode()),
                    RStatusErrorCode.STATUS_INVALID));
        }

        if (!repository.existsById(statusId)) {
            throw new BusinessException(new RStatusErrorMessage(
                    Map.of(RStatusErrorCode.STATUS_IS_NOT_FOUND, statusId),
                    RStatusErrorCode.STATUS_IS_NOT_FOUND));
        }

        var status = modelMapper.map(statusRequest, RStatus.class);
        status.setId(statusId);
        var data = modelMapper.map(repository.save(status), RStatusResponse.class);
        var message = new RStatusSuccessMessage(AppSuccessCode.UPDATE_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RStatusResponse> deleteStatusById(Long statusId) {
        if (repository.existsById(statusId)) {
            throw new BusinessException(
                    new RStatusErrorMessage(
                            Map.of(RStatusErrorCode.STATUS_IS_NOT_FOUND, statusId),
                            RStatusErrorCode.STATUS_IS_NOT_FOUND));
        }

        repository.deleteById(statusId);
        var message = new RStatusSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public RStatus getStatusEntityById(Long statusId) {
        return repository.findById(statusId)
                .orElseThrow(() -> new BusinessException(
                        new RStatusErrorMessage(
                                Map.of(RStatusErrorCode.STATUS_IS_NOT_FOUND, statusId),
                                RStatusErrorCode.STATUS_IS_NOT_FOUND)));
    }

    @Override
    public RStatus getStatusEntityByCode(String code) {
        return repository.getStatusByCode(code)
                .orElseThrow(() -> new BusinessException(
                        new RStatusErrorMessage(
                                Map.of(RStatusErrorCode.STATUS_INVALID, code),
                                RStatusErrorCode.STATUS_INVALID)));
    }
}
