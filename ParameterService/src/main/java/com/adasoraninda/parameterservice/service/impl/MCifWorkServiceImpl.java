package com.adasoraninda.parameterservice.service.impl;


import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.message.MCifErrorMessage;
import com.adasoraninda.parameterservice.handle.exception.message.MCifWorkErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.MCifWorkSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MCifWork;
import com.adasoraninda.parameterservice.model.request.MCifWorkRequest;
import com.adasoraninda.parameterservice.model.response.MCifWorkResponse;
import com.adasoraninda.parameterservice.repository.MCifRepository;
import com.adasoraninda.parameterservice.repository.MCifWorkRepository;
import com.adasoraninda.parameterservice.service.MCifWorkService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.parameterservice.controller.AppRequestUtil.MAX_WORK;
import static com.adasoraninda.parameterservice.handle.exception.code.MCifErrorCode.CIF_IS_NOT_FOUND;
import static com.adasoraninda.parameterservice.handle.exception.code.MCifWorkErrorCode.*;

@Service
@AllArgsConstructor
public class MCifWorkServiceImpl implements MCifWorkService {
    private final ModelMapper modelMapper;

    private final MCifRepository cifRepository;
    private final MCifWorkRepository workRepository;

    @Override
    public AppSuccessHandler<List<MCifWorkResponse>> getWorks() {
        var works = workRepository.findAllCifWork();

        if (works.isEmpty()) {
            throw throwEmptyWorks();
        }

        var message = new MCifWorkSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = works.stream()
                .map(work -> modelMapper.map(work, MCifWorkResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MCifWorkResponse> getWorkById(Long workId) {
        var data = workRepository.findCifWorkById(workId)
                .map(work -> modelMapper.map(work, MCifWorkResponse.class))
                .orElseThrow(() -> throwWorkNotFound(workId));

        var message = new MCifWorkSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifWorkResponse> createWork(MCifWorkRequest workRequest, String role) {
        var cif = cifRepository.findCifById(workRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(workRequest.getCifId()));

        if (workRepository.getTotalWorkByCifId(cif.getId()) >= MAX_WORK) {
            throw throwMaxWork();
        }

        var work = modelMapper.typeMap(MCifWorkRequest.class, MCifWork.class)
                .addMappings(mapper -> mapper.skip(MCifWork::setId))
                .map(workRequest);

        work.create(cif, role);

        var message = new MCifWorkSuccessMessage(AppSuccessCode.CREATE_DATA);
        var data = modelMapper.map(
                workRepository.save(work),
                MCifWorkResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifWorkResponse> updateWork(Long workId, String role, MCifWorkRequest workRequest) {
        var cif = cifRepository.findCifById(workRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(workRequest.getCifId()));

        if (workRepository.getTotalWorkByCifId(cif.getId()) >= MAX_WORK) {
            throw throwMaxWork();
        }

        var oldWork = workRepository.findCifWorkById(workId)
                .orElseThrow(() -> throwWorkNotFound(workId));

        oldWork.update(modelMapper.map(workRequest, MCifWork.class), cif, role);

        var message = new MCifWorkSuccessMessage(AppSuccessCode.UPDATE_DATA);
        var data = modelMapper.map(
                workRepository.save(oldWork),
                MCifWorkResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteWorkById(Long workId, String role) {
        var workIsExists = workRepository.existCifWorkById(workId);

        if (!workIsExists) {
            throw throwWorkNotFound(workId);
        }

        workRepository.softDelete(workId, role, LocalDateTime.now());

        var message = new MCifWorkSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    private RuntimeException throwEmptyWorks() {
        return new BusinessException(
                new MCifWorkErrorMessage(
                        Map.of(CIF_WORK_IS_EMPTY, new Object()),
                        CIF_WORK_IS_EMPTY));
    }

    private RuntimeException throwMaxWork() {
        return new BusinessException(
                new MCifWorkErrorMessage(
                        Map.of(CIF_WORK_MAX, MAX_WORK),
                        CIF_WORK_MAX));
    }

    private RuntimeException throwWorkNotFound(Long workId) {
        return new BusinessException(
                new MCifWorkErrorMessage(
                        Map.of(CIF_WORK_IS_NOT_FOUND, workId),
                        CIF_WORK_IS_NOT_FOUND));
    }

    private RuntimeException throwCifNotFound(Long cifId) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_IS_NOT_FOUND, cifId),
                        CIF_IS_NOT_FOUND));
    }

}
