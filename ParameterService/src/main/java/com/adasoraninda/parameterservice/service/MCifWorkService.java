package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.request.MCifWorkRequest;
import com.adasoraninda.parameterservice.model.response.MCifWorkResponse;

import java.util.List;

public interface MCifWorkService {

    AppSuccessHandler<List<MCifWorkResponse>> getWorks();

    AppSuccessHandler<MCifWorkResponse> getWorkById(Long workId);

    AppSuccessHandler<MCifWorkResponse> createWork(MCifWorkRequest workRequest, String role);

    AppSuccessHandler<MCifWorkResponse> updateWork(Long workId, String role, MCifWorkRequest workRequest);

    AppSuccessHandler<Object> deleteWorkById(Long workId, String role);

}
