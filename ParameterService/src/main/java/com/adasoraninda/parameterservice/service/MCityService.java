package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.MCity;
import com.adasoraninda.parameterservice.model.request.MCityRequest;
import com.adasoraninda.parameterservice.model.response.MCityResponse;

import java.util.List;

public interface MCityService {

    AppSuccessHandler<List<MCityResponse>> getAllCities();

    AppSuccessHandler<MCityResponse> getCityById(Long cityId);

    AppSuccessHandler<MCityResponse> createCity(MCityRequest cityRequest, String role);

    AppSuccessHandler<MCityResponse> updateCity(Long cityId, MCityRequest cityRequest, String role);

    AppSuccessHandler<Object> deleteCity(Long cityId, String role);

    MCity getCityEntityById(Long cityId);

}
