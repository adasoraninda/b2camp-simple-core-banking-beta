package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.code.REducationErrorCode;
import com.adasoraninda.parameterservice.handle.exception.message.REducationErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.REducationSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.REducation;
import com.adasoraninda.parameterservice.model.request.REducationRequest;
import com.adasoraninda.parameterservice.model.response.REducationResponse;
import com.adasoraninda.parameterservice.repository.REducationRepository;
import com.adasoraninda.parameterservice.service.REducationService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class REducationServiceImpl implements REducationService {

    private final ModelMapper modelMapper;
    private final REducationRepository repository;

    @Override
    public AppSuccessHandler<List<REducationResponse>> getAllEducations() {
        var educations = repository.findAll();

        if (educations.isEmpty()) {
            throw new BusinessException(new REducationErrorMessage(
                    Map.of(REducationErrorCode.EDUCATION_IS_EMPTY, new Object()),
                    REducationErrorCode.EDUCATION_IS_EMPTY));
        }

        var data = educations.stream()
                .map(e -> modelMapper.map(e,REducationResponse.class))
                .collect(Collectors.toList());
        var message = new REducationSuccessMessage(AppSuccessCode.GET_ALL_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<REducationResponse> getEducationById(Long educationId) {
        var data = repository.findById(educationId)
                .map(e -> modelMapper.map(e, REducationResponse.class))
                .orElseThrow(() -> new BusinessException(new REducationErrorMessage(
                        Map.of(REducationErrorCode.EDUCATION_IS_NOT_FOUND, educationId),
                        REducationErrorCode.EDUCATION_IS_NOT_FOUND)));
        var message = new REducationSuccessMessage(AppSuccessCode.GET_ALL_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<REducationResponse> createEducation(REducationRequest educationRequest) {
        var education = modelMapper.map(educationRequest, REducation.class);
        var data = modelMapper.map(repository.save(education), REducationResponse.class);
        var message = new REducationSuccessMessage(AppSuccessCode.CREATE_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<REducationResponse> updateEducationById(Long educationId, REducationRequest educationRequest) {
        var isEducationExists = repository.existsById(educationId);

        if (!isEducationExists) {
            throw new BusinessException(new REducationErrorMessage(
                    Map.of(REducationErrorCode.EDUCATION_IS_NOT_FOUND, educationId),
                    REducationErrorCode.EDUCATION_IS_NOT_FOUND));
        }

        var education = modelMapper.map(educationRequest, REducation.class);
        education.setId(educationId);

        var data = modelMapper.map(repository.save(education), REducationResponse.class);
        var message = new REducationSuccessMessage(AppSuccessCode.UPDATE_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<REducationResponse> deleteEducationById(Long educationId) {
        var isEducationExists = repository.existsById(educationId);

        if (!isEducationExists) {
            throw new BusinessException(new REducationErrorMessage(
                    Map.of(REducationErrorCode.EDUCATION_IS_NOT_FOUND, educationId),
                    REducationErrorCode.EDUCATION_IS_NOT_FOUND));
        }

        repository.delete(repository.getById(educationId));

        var message = new REducationSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public REducation getEducationEntityById(Long educationId) {
        return repository.findById(educationId)
                .orElseThrow(() -> new BusinessException(new REducationErrorMessage(
                        Map.of(REducationErrorCode.EDUCATION_IS_NOT_FOUND, educationId),
                        REducationErrorCode.EDUCATION_IS_NOT_FOUND)));
    }
}
