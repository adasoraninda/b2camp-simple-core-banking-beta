package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.MTransactionCode;
import com.adasoraninda.parameterservice.model.request.MTransactionCodeRequest;
import com.adasoraninda.parameterservice.model.response.MTransactionCodeResponse;

import java.util.List;

public interface MTransactionCodeService {

    AppSuccessHandler<List<MTransactionCodeResponse>> getAllTransactionCode();

    AppSuccessHandler<MTransactionCodeResponse> getTransactionCodeById(Long transactionCodeId);

    AppSuccessHandler<MTransactionCodeResponse> createTransactionCode(
            MTransactionCodeRequest transactionCodeRequest,
            String role);

    AppSuccessHandler<MTransactionCodeResponse> updateTransactionCode(
            Long transactionCodeId,
            String role,
            MTransactionCodeRequest transactionCodeRequest);

    AppSuccessHandler<Object> deleteTransactionCodeById(Long transactionCodeId, String role);

    MTransactionCode getTransactionCodeEntityById(Long transactionCodeId);

    MTransactionCode getTransactionCodeEntityByCode(Integer code);

}
