package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.MSavingProduct;
import com.adasoraninda.parameterservice.model.request.MSavingProductRequest;
import com.adasoraninda.parameterservice.model.response.MSavingProductResponse;

import java.util.List;

public interface MSavingProductService {

    AppSuccessHandler<List<MSavingProductResponse>> getAllSavingProduct();

    AppSuccessHandler<MSavingProductResponse> getSavingProductById(Long savingProductId);

    AppSuccessHandler<MSavingProductResponse> createSavingProduct(
            MSavingProductRequest savingProductRequest,
            String role);

    AppSuccessHandler<MSavingProductResponse> updateSavingProduct(
            Long savingProductId,
            String role,
            MSavingProductRequest savingProductRequest);

    AppSuccessHandler<Object> deleteSavingProductById(Long savingProductId, String role);

    MSavingProduct getSavingProductEntityById(Long savingProductId);

    MSavingProduct getSavingProductEntityByName(String name);

}
