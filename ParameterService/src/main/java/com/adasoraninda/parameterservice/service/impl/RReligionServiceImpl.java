package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.code.RReligionErrorCode;
import com.adasoraninda.parameterservice.handle.exception.message.RReligionErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.RReligionSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.RReligion;
import com.adasoraninda.parameterservice.model.request.RReligionRequest;
import com.adasoraninda.parameterservice.model.response.RReligionResponse;
import com.adasoraninda.parameterservice.repository.RReligionRepository;
import com.adasoraninda.parameterservice.service.RReligionService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RReligionServiceImpl implements RReligionService {

    private final ModelMapper modelMapper;
    private final RReligionRepository repository;

    @Override
    public AppSuccessHandler<List<RReligionResponse>> getAllReligions() {
        var religions = repository.findAll();

        if (religions.isEmpty()) {
            throw new BusinessException(new RReligionErrorMessage(
                    Map.of(RReligionErrorCode.RELIGION_IS_EMPTY, new Object()),
                    RReligionErrorCode.RELIGION_IS_EMPTY));
        }

        var message = new RReligionSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = religions.stream()
                .map(r -> modelMapper.map(r, RReligionResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RReligionResponse> getReligionById(Long religionId) {
        var data = repository.findById(religionId)
                .map(r -> modelMapper.map(r, RReligionResponse.class))
                .orElseThrow(() -> new BusinessException(new RReligionErrorMessage(
                        Map.of(RReligionErrorCode.RELIGION_IS_NOT_FOUND, religionId),
                        RReligionErrorCode.RELIGION_IS_NOT_FOUND)));
        var message = new RReligionSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RReligionResponse> createReligion(RReligionRequest religionRequest) {
        var religion = modelMapper.map(religionRequest, RReligion.class);
        var data = modelMapper.map(repository.save(religion), RReligionResponse.class);
        var message = new RReligionSuccessMessage(AppSuccessCode.CREATE_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RReligionResponse> updateReligionById(Long religionId, RReligionRequest religionRequest) {
        var isReligionExists = repository.existsById(religionId);

        if (!isReligionExists) {
            throw new BusinessException(new RReligionErrorMessage(
                    Map.of(RReligionErrorCode.RELIGION_IS_NOT_FOUND, religionId),
                    RReligionErrorCode.RELIGION_IS_NOT_FOUND));
        }

        var religion = modelMapper.map(religionRequest, RReligion.class);
        religion.setId(religionId);

        var data = modelMapper.map(repository.save(religion), RReligionResponse.class);
        var message = new RReligionSuccessMessage(AppSuccessCode.UPDATE_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<RReligionResponse> deleteReligionById(Long religionId) {
        var isReligionExists = repository.existsById(religionId);

        if (!isReligionExists) {
            throw new BusinessException(new RReligionErrorMessage(
                    Map.of(RReligionErrorCode.RELIGION_IS_NOT_FOUND, religionId),
                    RReligionErrorCode.RELIGION_IS_NOT_FOUND));
        }

        repository.delete(repository.getById(religionId));

        var message = new RReligionSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public RReligion getReligionEntityById(Long religionId) {
        return repository.findById(religionId)
                .orElseThrow(() -> new BusinessException(new RReligionErrorMessage(
                        Map.of(RReligionErrorCode.RELIGION_IS_NOT_FOUND, religionId),
                        RReligionErrorCode.RELIGION_IS_NOT_FOUND)));
    }
}
