package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.MCif;
import com.adasoraninda.parameterservice.model.request.MCifRequest;
import com.adasoraninda.parameterservice.model.response.MCifResponse;

import java.util.List;

public interface MCifService {

    AppSuccessHandler<List<MCifResponse>> getListCif();

    AppSuccessHandler<MCifResponse> getCifById(Long cifId);

    AppSuccessHandler<MCifResponse> createCif(MCifRequest cifRequest, String role);

    AppSuccessHandler<MCifResponse> updateCif(Long cifId, String role, MCifRequest cifRequest);

    AppSuccessHandler<Object> deleteCifById(Long cifId, String role);

    MCif getCifEntityById(Long cifId);

    MCif getCifEntityByNik(String nik);

}
