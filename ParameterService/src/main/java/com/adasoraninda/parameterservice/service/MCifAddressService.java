package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.request.MCifAddressRequest;
import com.adasoraninda.parameterservice.model.response.MCifAddressResponse;

import java.util.List;

public interface MCifAddressService {

    AppSuccessHandler<List<MCifAddressResponse>> getAddresses();

    AppSuccessHandler<MCifAddressResponse> getAddressById(Long addressId);

    AppSuccessHandler<MCifAddressResponse> createAddress(MCifAddressRequest addressRequest, String role);

    AppSuccessHandler<MCifAddressResponse> updateAddress(Long addressId, String role, MCifAddressRequest addressRequest);

    AppSuccessHandler<Object> deleteAddressById(Long addressId, String role);

}
