package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.code.MCoaErrorCode;
import com.adasoraninda.parameterservice.handle.exception.message.MCoaErrorMessage;
import com.adasoraninda.parameterservice.handle.exception.message.MSavingProductErrorMessage;
import com.adasoraninda.parameterservice.handle.success.message.MSavingProductSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MSavingProduct;
import com.adasoraninda.parameterservice.model.request.MSavingProductRequest;
import com.adasoraninda.parameterservice.model.response.MSavingProductResponse;
import com.adasoraninda.parameterservice.repository.MCoaRepository;
import com.adasoraninda.parameterservice.repository.MSavingProductRepository;
import com.adasoraninda.parameterservice.service.MSavingProductService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.parameterservice.handle.exception.code.MSavingProductErrorCode.*;
import static com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode.*;

@Service
@AllArgsConstructor
public class MSavingProductServiceImpl implements MSavingProductService {

    private final ModelMapper modelMapper;
    private final MSavingProductRepository savingProductRepository;
    private final MCoaRepository coaRepository;

    @Override
    public AppSuccessHandler<List<MSavingProductResponse>> getAllSavingProduct() {
        var allSavingProduct = savingProductRepository.findAllSavingProduct();

        if (allSavingProduct.isEmpty()) {
            throw new BusinessException(new MSavingProductErrorMessage(
                    Map.of(SAVING_PRODUCT_IS_EMPTY, new Object()),
                    SAVING_PRODUCT_IS_EMPTY));
        }

        var message = new MSavingProductSuccessMessage(GET_ALL_DATA);
        var data = allSavingProduct.stream()
                .map(sp -> modelMapper.map(sp, MSavingProductResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MSavingProductResponse> getSavingProductById(Long savingProductId) {
        var data = savingProductRepository.findSavingProductById(savingProductId)
                .map(sp -> modelMapper.map(sp, MSavingProductResponse.class))
                .orElseThrow(() -> new BusinessException(new MSavingProductErrorMessage(
                        Map.of(SAVING_PRODUCT_IS_NOT_FOUND, savingProductId),
                        SAVING_PRODUCT_IS_NOT_FOUND)));
        var message = new MSavingProductSuccessMessage(GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MSavingProductResponse> createSavingProduct(MSavingProductRequest savingProductRequest, String role) {
        var coa = coaRepository.findCoaById(savingProductRequest.getCoaId())
                .orElseThrow(() -> new BusinessException(new MCoaErrorMessage(
                        Map.of(MCoaErrorCode.COA_IS_NOT_FOUND, savingProductRequest.getCoaId()),
                        MCoaErrorCode.COA_IS_NOT_FOUND)));

        var savingProduct = modelMapper.map(savingProductRequest, MSavingProduct.class);
        savingProduct.create(coa, role);

        var message = new MSavingProductSuccessMessage(CREATE_DATA);
        var data = modelMapper.map(
                savingProductRepository.save(savingProduct),
                MSavingProductResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MSavingProductResponse> updateSavingProduct(Long savingProductId, String role, MSavingProductRequest savingProductRequest) {
        var savingProduct = savingProductRepository.findSavingProductById(savingProductId)
                .orElseThrow(() -> new BusinessException(new MSavingProductErrorMessage(
                        Map.of(SAVING_PRODUCT_IS_NOT_FOUND, savingProductId),
                        SAVING_PRODUCT_IS_NOT_FOUND)));

        var coa = coaRepository.findCoaById(savingProductRequest.getCoaId())
                .orElseThrow(() -> new BusinessException(new MCoaErrorMessage(
                        Map.of(MCoaErrorCode.COA_IS_NOT_FOUND, savingProductRequest.getCoaId()),
                        MCoaErrorCode.COA_IS_NOT_FOUND)));

        savingProduct.update(
                savingProductRequest.getProductName(),
                savingProductRequest.getDescription(),
                coa,
                role);

        var message = new MSavingProductSuccessMessage(UPDATE_DATA);
        var data = modelMapper.map(
                savingProductRepository.save(savingProduct),
                MSavingProductResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteSavingProductById(Long savingProductId, String role) {
        var isSavingProductExists = savingProductRepository.existsSavingProductById(savingProductId);

        if (!isSavingProductExists) {
            throw new BusinessException(new MSavingProductErrorMessage(
                    Map.of(SAVING_PRODUCT_IS_NOT_FOUND, savingProductId),
                    SAVING_PRODUCT_IS_NOT_FOUND));
        }

        savingProductRepository.softDelete(
                savingProductId,
                role,
                LocalDateTime.now());

        var message = new MSavingProductSuccessMessage(DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public MSavingProduct getSavingProductEntityById(Long savingProductId) {
        return savingProductRepository.findSavingProductById(savingProductId)
                .orElseThrow(() -> new BusinessException(new MSavingProductErrorMessage(
                        Map.of(SAVING_PRODUCT_IS_NOT_FOUND, savingProductId),
                        SAVING_PRODUCT_IS_NOT_FOUND)));
    }

    @Override
    public MSavingProduct getSavingProductEntityByName(String name) {
        return savingProductRepository.findSavingProductByName(name)
                .orElseThrow(() -> new BusinessException(new MSavingProductErrorMessage(
                        Map.of(SAVING_PRODUCT_NAME_IS_NOT_FOUND, name),
                        SAVING_PRODUCT_NAME_IS_NOT_FOUND)));
    }
}
