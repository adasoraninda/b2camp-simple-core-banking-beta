package com.adasoraninda.parameterservice.service.impl;

import static com.adasoraninda.parameterservice.handle.exception.code.MDepositProductErrorCode.DEPOSIT_PRODUCT_IS_EMPTY;
import static com.adasoraninda.parameterservice.handle.exception.code.MDepositProductErrorCode.DEPOSIT_PRODUCT_IS_NOT_FOUND;
import static com.adasoraninda.parameterservice.handle.exception.code.MDepositProductErrorCode.DEPOSIT_PRODUCT_NAME_IS_NOT_FOUND;
import static com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode.CREATE_DATA;
import static com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode.DELETE_DATA;
import static com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode.GET_ALL_DATA;
import static com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode.GET_DATA;
import static com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode.UPDATE_DATA;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.code.MCoaErrorCode;
import com.adasoraninda.parameterservice.handle.exception.message.MCoaErrorMessage;
import com.adasoraninda.parameterservice.handle.exception.message.MDepositProductErrorMessage;
import com.adasoraninda.parameterservice.handle.success.message.MDepositProductSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MDepositProduct;
import com.adasoraninda.parameterservice.model.request.MDepositProductRequest;
import com.adasoraninda.parameterservice.model.response.MDepositProductResponse;
import com.adasoraninda.parameterservice.repository.MCoaRepository;
import com.adasoraninda.parameterservice.repository.MDepositProductRepository;
import com.adasoraninda.parameterservice.service.MDepositProductService;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MDepositProductServiceImpl implements MDepositProductService {
    private final ModelMapper modelMapper;
    private final MDepositProductRepository depositProductRepository;
    private final MCoaRepository coaRepository;

    @Override
    public AppSuccessHandler<List<MDepositProductResponse>> getAllDepositProduct() {
        var allDepositProduct = depositProductRepository.findAllDepositProduct();

        if (allDepositProduct.isEmpty()) {
            throw new BusinessException(new MDepositProductErrorMessage(
                    Map.of(DEPOSIT_PRODUCT_IS_EMPTY, new Object()),
                    DEPOSIT_PRODUCT_IS_EMPTY));
        }

        var message = new MDepositProductSuccessMessage(GET_ALL_DATA);
        var data = allDepositProduct.stream()
                .map(sp -> modelMapper.map(sp, MDepositProductResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MDepositProductResponse> getDepositProductById(Long dpId) {
        var data = depositProductRepository.findDepositProductById(dpId)
                .map(sp -> modelMapper.map(sp, MDepositProductResponse.class))
                .orElseThrow(() -> new BusinessException(new MDepositProductErrorMessage(
                        Map.of(DEPOSIT_PRODUCT_IS_NOT_FOUND, dpId),
                        DEPOSIT_PRODUCT_IS_NOT_FOUND)));
        var message = new MDepositProductSuccessMessage(GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MDepositProductResponse> createDepositProduct(MDepositProductRequest depositProductRequest, String role) {
        var coa = coaRepository.findCoaById(depositProductRequest.getCoaId())
                .orElseThrow(() -> new BusinessException(new MCoaErrorMessage(
                        Map.of(MCoaErrorCode.COA_IS_NOT_FOUND, depositProductRequest.getCoaId()),
                        MCoaErrorCode.COA_IS_NOT_FOUND)));

        var depositProduct = modelMapper.map(depositProductRequest, MDepositProduct.class);
        depositProduct.create(coa, role);

        var message = new MDepositProductSuccessMessage(CREATE_DATA);
        var data = modelMapper.map(
                depositProductRepository.save(depositProduct),
                MDepositProductResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MDepositProductResponse> updateDepositProduct(Long dpId, MDepositProductRequest depositProductRequest, String role) {
        var depositProduct = depositProductRepository.findDepositProductById(dpId)
                .orElseThrow(() -> new BusinessException(new MDepositProductErrorMessage(
                        Map.of(DEPOSIT_PRODUCT_IS_NOT_FOUND, dpId),
                        DEPOSIT_PRODUCT_IS_NOT_FOUND)));

        var coa = coaRepository.findCoaById(depositProductRequest.getCoaId())
                .orElseThrow(() -> new BusinessException(new MCoaErrorMessage(
                        Map.of(MCoaErrorCode.COA_IS_NOT_FOUND, depositProductRequest.getCoaId()),
                        MCoaErrorCode.COA_IS_NOT_FOUND)));

        depositProduct.update(
                depositProductRequest.getName(),
                depositProductRequest.getInterestRate(),
                depositProductRequest.getCode(),
                coa,
                role);

        var message = new MDepositProductSuccessMessage(UPDATE_DATA);
        var data = modelMapper.map(
                depositProductRepository.save(depositProduct),
                MDepositProductResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<Object> deleteDepositProduct(Long dpId, String role) {
        var isDepositProductExists = depositProductRepository.existsDepositProductById(dpId);

        if (!isDepositProductExists) {
            throw new BusinessException(new MDepositProductErrorMessage(
                    Map.of(DEPOSIT_PRODUCT_IS_NOT_FOUND, dpId),
                    DEPOSIT_PRODUCT_IS_NOT_FOUND));
        }

        depositProductRepository.softDelete(
                dpId,
                role,
                LocalDateTime.now());

        var message = new MDepositProductSuccessMessage(DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public MDepositProduct getDepositProductEntityById(Long dpId) {
        return depositProductRepository.findDepositProductById(dpId)
                .orElseThrow(() -> new BusinessException(new MDepositProductErrorMessage(
                        Map.of(DEPOSIT_PRODUCT_IS_NOT_FOUND, dpId),
                        DEPOSIT_PRODUCT_IS_NOT_FOUND)));
    }

    @Override
    public MDepositProduct getDepositProductEntityByName(String name) {
        return depositProductRepository.findDepositProductByName(name)
                .orElseThrow(() -> new BusinessException(new MDepositProductErrorMessage(
                        Map.of(DEPOSIT_PRODUCT_NAME_IS_NOT_FOUND, name),
                        DEPOSIT_PRODUCT_NAME_IS_NOT_FOUND)));
    }
}
