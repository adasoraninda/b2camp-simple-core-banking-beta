package com.adasoraninda.parameterservice.service.impl;


import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.message.MCifAddressErrorMessage;
import com.adasoraninda.parameterservice.handle.exception.message.MCifErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.MCifAddressSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MCifAddress;
import com.adasoraninda.parameterservice.model.request.MCifAddressRequest;
import com.adasoraninda.parameterservice.model.response.MCifAddressResponse;
import com.adasoraninda.parameterservice.repository.MCifAddressRepository;
import com.adasoraninda.parameterservice.repository.MCifRepository;
import com.adasoraninda.parameterservice.service.MCifAddressService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.parameterservice.handle.exception.code.MCifAddressErrorCode.CIF_ADDRESS_IS_EMPTY;
import static com.adasoraninda.parameterservice.handle.exception.code.MCifAddressErrorCode.CIF_ADDRESS_IS_NOT_FOUND;
import static com.adasoraninda.parameterservice.handle.exception.code.MCifErrorCode.CIF_IS_NOT_FOUND;

@Service
@AllArgsConstructor
public class MCifAddressServiceImpl implements MCifAddressService {
    private final ModelMapper modelMapper;

    private final MCifRepository cifRepository;
    private final MCifAddressRepository addressRepository;

    @Override
    public AppSuccessHandler<List<MCifAddressResponse>> getAddresses() {
        var addresses = addressRepository.findAllCifAddress();

        if (addresses.isEmpty()) {
            throw throwAddressEmpty();
        }

        var message = new MCifAddressSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = addresses.stream()
                .map(address -> modelMapper.map(address, MCifAddressResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MCifAddressResponse> getAddressById(Long addressId) {
        var data = addressRepository.findCifAddressById(addressId)
                .map(address -> modelMapper.map(address, MCifAddressResponse.class))
                .orElseThrow(() -> throwAddressNotFound(addressId));
        var message = new MCifAddressSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifAddressResponse> createAddress(MCifAddressRequest addressRequest, String role) {
        var cif = cifRepository.findCifById(addressRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(addressRequest.getCifId()));

        var address = modelMapper.typeMap(MCifAddressRequest.class, MCifAddress.class)
                .addMappings(mapper -> mapper.skip(MCifAddress::setId))
                .map(addressRequest);

        address.create(cif, role);

        var message = new MCifAddressSuccessMessage(AppSuccessCode.CREATE_DATA);
        var data = modelMapper.map(
                addressRepository.save(address),
                MCifAddressResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifAddressResponse> updateAddress(Long addressId, String role, MCifAddressRequest addressRequest) {
        var cif = cifRepository.findCifById(addressRequest.getCifId())
                .orElseThrow(() -> throwCifNotFound(addressRequest.getCifId()));

        var oldAddress = addressRepository.findCifAddressById(addressId)
                .orElseThrow(() -> throwAddressNotFound(addressId));

        oldAddress.update(modelMapper.map(addressRequest, MCifAddress.class), cif, role);

        var message = new MCifAddressSuccessMessage(AppSuccessCode.UPDATE_DATA);
        var data = modelMapper.map(
                addressRepository.save(oldAddress),
                MCifAddressResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteAddressById(Long addressId, String role) {
        var addressIsExists = addressRepository.existCifAddressById(addressId);

        if (!addressIsExists) {
            throw throwCifNotFound(addressId);
        }

        addressRepository.softDelete(addressId, role, LocalDateTime.now());

        var message = new MCifAddressSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    private RuntimeException throwAddressEmpty() {
        return new BusinessException(
                new MCifAddressErrorMessage(
                        Map.of(CIF_ADDRESS_IS_EMPTY, new Object()),
                        CIF_ADDRESS_IS_EMPTY));
    }

    private RuntimeException throwAddressNotFound(Long addressId) {
        return new BusinessException(
                new MCifAddressErrorMessage(
                        Map.of(CIF_ADDRESS_IS_NOT_FOUND, addressId),
                        CIF_ADDRESS_IS_NOT_FOUND));
    }

    private RuntimeException throwCifNotFound(Long cifId) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_IS_NOT_FOUND, cifId),
                        CIF_IS_NOT_FOUND));
    }

}
