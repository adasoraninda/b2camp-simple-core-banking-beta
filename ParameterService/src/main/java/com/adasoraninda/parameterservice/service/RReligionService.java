package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.RReligion;
import com.adasoraninda.parameterservice.model.request.RReligionRequest;
import com.adasoraninda.parameterservice.model.response.RReligionResponse;

import java.util.List;

public interface RReligionService {

    AppSuccessHandler<List<RReligionResponse>> getAllReligions();

    AppSuccessHandler<RReligionResponse> getReligionById(Long religionId);

    AppSuccessHandler<RReligionResponse> createReligion(RReligionRequest religionRequest);

    AppSuccessHandler<RReligionResponse> updateReligionById(Long religionId, RReligionRequest religionRequest);

    AppSuccessHandler<RReligionResponse> deleteReligionById(Long religionId);

    RReligion getReligionEntityById(Long religionId);

}
