package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.RStatus;
import com.adasoraninda.parameterservice.model.request.RStatusRequest;
import com.adasoraninda.parameterservice.model.response.RStatusResponse;

import java.util.List;

public interface RStatusService {

    AppSuccessHandler<List<RStatusResponse>> getAllStatus();

    AppSuccessHandler<RStatusResponse> getStatusById(Long statusId);

    AppSuccessHandler<RStatusResponse> createStatus(RStatusRequest statusRequest);

    AppSuccessHandler<RStatusResponse> updateStatusById(Long statusId, RStatusRequest statusRequest);

    AppSuccessHandler<RStatusResponse> deleteStatusById(Long statusId);

    RStatus getStatusEntityById(Long statusId);

    RStatus getStatusEntityByCode(String code);

}
