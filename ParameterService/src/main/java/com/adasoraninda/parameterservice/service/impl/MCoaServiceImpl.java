package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.message.MCoaErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.MCoaSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MCoa;
import com.adasoraninda.parameterservice.model.request.MCoaRequest;
import com.adasoraninda.parameterservice.model.response.MCoaResponse;
import com.adasoraninda.parameterservice.repository.MCoaRepository;
import com.adasoraninda.parameterservice.service.MCoaService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.parameterservice.handle.exception.code.MCoaErrorCode.*;

@Service
@AllArgsConstructor
public class MCoaServiceImpl implements MCoaService {

    private final MCoaRepository repository;
    private final ModelMapper modelMapper;

    @Override
    public AppSuccessHandler<List<MCoaResponse>> getAllCoa() {
        var allCoa = repository.findAllCoa();

        if (allCoa.isEmpty()) {
            throw new BusinessException(new MCoaErrorMessage(
                    Map.of(COA_IS_EMPTY, new Object()),
                    COA_IS_EMPTY));
        }

        var message = new MCoaSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = allCoa.stream()
                .map(c -> modelMapper.map(c, MCoaResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MCoaResponse> getCoaById(Long coaId) {
        var data = repository.findCoaById(coaId)
                .map(c -> modelMapper.map(c, MCoaResponse.class))
                .orElseThrow(() -> new BusinessException(new MCoaErrorMessage(
                        Map.of(COA_IS_NOT_FOUND, coaId),
                        COA_IS_NOT_FOUND)));
        var message = new MCoaSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MCoaResponse> createCoa(MCoaRequest coaRequest, String role) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        var coa = modelMapper.map(coaRequest, MCoa.class);
        coa.create(role);

        var message = new MCoaSuccessMessage(AppSuccessCode.CREATE_DATA);
        var data = modelMapper.map(repository.save(coa), MCoaResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MCoaResponse> updateCoa(Long coaId, MCoaRequest coaRequest, String role) {
        var coa = repository.findCoaById(coaId)
                .orElseThrow(() -> new BusinessException(new MCoaErrorMessage(
                        Map.of(COA_IS_NOT_FOUND, coaId),
                        COA_IS_NOT_FOUND)));
        coa.update(coaRequest.getCoaCode(), coaRequest.getName(), coaRequest.getDescription(), role);

        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        var message = new MCoaSuccessMessage(AppSuccessCode.UPDATE_DATA);
        var data = modelMapper.map(repository.save(coa), MCoaResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<Object> deleteCoa(Long coaId, String role) {
        if (!repository.existsCoaById(coaId)) {
            throw new BusinessException(new MCoaErrorMessage(
                    Map.of(COA_IS_NOT_FOUND, coaId),
                    COA_IS_NOT_FOUND));
        }

        repository.softDelete(coaId, role, LocalDateTime.now());

        var message = new MCoaSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public MCoa getCoaEntityById(Long coaId) {
        return repository.findCoaById(coaId)
                .orElseThrow(() -> new BusinessException(new MCoaErrorMessage(
                        Map.of(COA_IS_NOT_FOUND, coaId),
                        COA_IS_NOT_FOUND)));
    }

    @Override
    public MCoa getCoaEntityByCode(String code) {
        return repository.findCoaByCode(code)
                .orElseThrow(() -> new BusinessException(new MCoaErrorMessage(
                        Map.of(COA_CODE_IS_NOT_FOUND, code),
                        COA_CODE_IS_NOT_FOUND)));
    }
}
