package com.adasoraninda.parameterservice.service.impl;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.handle.exception.BusinessException;
import com.adasoraninda.parameterservice.handle.exception.message.MCifErrorMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import com.adasoraninda.parameterservice.handle.success.message.MCifSuccessMessage;
import com.adasoraninda.parameterservice.model.entity.MCif;
import com.adasoraninda.parameterservice.model.request.MCifRequest;
import com.adasoraninda.parameterservice.model.response.MCifResponse;
import com.adasoraninda.parameterservice.repository.MCifRepository;
import com.adasoraninda.parameterservice.service.MCifService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adasoraninda.parameterservice.controller.AppRequestUtil.COMPANY_CIF_TYPE;
import static com.adasoraninda.parameterservice.handle.exception.code.MCifErrorCode.*;

@Service
@AllArgsConstructor
public class MCifServiceImpl implements MCifService {

    private final ModelMapper modelMapper;
    private final MCifRepository cifRepository;

    @Override
    public AppSuccessHandler<List<MCifResponse>> getListCif() {
        var listCif = cifRepository.findAllCif();

        if (listCif.isEmpty()) {
            throw throwCifIsEmpty();
        }

        var message = new MCifSuccessMessage(AppSuccessCode.GET_ALL_DATA);
        var data = listCif.stream()
                .map(cif -> modelMapper.map(cif, MCifResponse.class))
                .collect(Collectors.toList());

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    public AppSuccessHandler<MCifResponse> getCifById(Long cifId) {
        var data = cifRepository.findCifById(cifId)
                .map(cif -> modelMapper.map(cif, MCifResponse.class))
                .orElseThrow(() -> throwCifNotFound(cifId));
        var message = new MCifSuccessMessage(AppSuccessCode.GET_DATA);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifResponse> createCif(MCifRequest cifRequest, String role) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        var cif = modelMapper.map(cifRequest, MCif.class);

        if (cif.getType().equalsIgnoreCase(COMPANY_CIF_TYPE)) {
            if (cif.getNpwp() == null) {
                throw throwCifNpwp();
            }
        }

        cif.create(role);

        var message = new MCifSuccessMessage(AppSuccessCode.CREATE_DATA);
        var data = modelMapper.map(
                cifRepository.save(cif),
                MCifResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<MCifResponse> updateCif(Long cifId, String role, MCifRequest cifRequest) {
        var oldCif = cifRepository.findCifById(cifId)
                .orElseThrow(() -> throwCifNotFound(cifId));

        oldCif.update(
                cifRequest.getIdKtp(),
                cifRequest.getName(),
                cifRequest.getNpwp(),
                cifRequest.getType(),
                cifRequest.getEmail(),
                cifRequest.getType(),
                role);

        var message = new MCifSuccessMessage(AppSuccessCode.UPDATE_DATA);
        var data = modelMapper.map(
                cifRepository.save(oldCif),
                MCifResponse.class);

        return new AppSuccessHandler<>(message, data);
    }

    @Override
    @Transactional
    public AppSuccessHandler<Object> deleteCifById(Long cifId, String role) {
        var cifExists = cifRepository.existCifById(cifId);

        if (!cifExists) {
            throw throwCifNotFound(cifId);
        }

        cifRepository.softDelete(cifId, role, LocalDateTime.now());

        var message = new MCifSuccessMessage(AppSuccessCode.DELETE_DATA);

        return new AppSuccessHandler<>(message, null);
    }

    @Override
    public MCif getCifEntityById(Long cifId) {
        return cifRepository.findCifById(cifId)
                .orElseThrow(() -> throwCifNotFound(cifId));
    }

    @Override
    public MCif getCifEntityByNik(String nik) {
        return cifRepository.findCifByNik(nik)
                .orElseThrow(() -> throwCifNikNotFound(nik));
    }

    private RuntimeException throwCifIsEmpty() {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_IS_EMPTY, new Object()),
                        CIF_IS_EMPTY));
    }

    private RuntimeException throwCifNotFound(Long cifId) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_IS_NOT_FOUND, cifId),
                        CIF_IS_NOT_FOUND));
    }

    private RuntimeException throwCifNikNotFound(String nik) {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_NIK_IS_NOT_FOUND, nik),
                        CIF_NIK_IS_NOT_FOUND));
    }

    private RuntimeException throwCifNpwp() {
        return new BusinessException(
                new MCifErrorMessage(
                        Map.of(CIF_NPWP_MUST_FILLED, new Object()),
                        CIF_NPWP_MUST_FILLED));
    }

}
