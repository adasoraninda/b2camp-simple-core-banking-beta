package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.MDistrict;
import com.adasoraninda.parameterservice.model.request.MDistrictRequest;
import com.adasoraninda.parameterservice.model.response.MDistrictResponse;

import java.util.List;

public interface MDistrictService {

    AppSuccessHandler<List<MDistrictResponse>> getAllDistricts();

    AppSuccessHandler<MDistrictResponse> getDistrictById(Long districtId);

    AppSuccessHandler<MDistrictResponse> createDistrict(MDistrictRequest districtRequest, String role);

    AppSuccessHandler<MDistrictResponse> updateDistrict(Long districtId, MDistrictRequest districtRequest, String role);

    AppSuccessHandler<Object> deleteDistrict(Long districtId, String role);

    MDistrict getDistrictEntityById(Long districtId);

}
