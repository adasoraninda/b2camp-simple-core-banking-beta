package com.adasoraninda.parameterservice.service;

import com.adasoraninda.parameterservice.handle.AppSuccessHandler;
import com.adasoraninda.parameterservice.model.entity.MDepositProduct;
import com.adasoraninda.parameterservice.model.request.MDepositProductRequest;
import com.adasoraninda.parameterservice.model.response.MDepositProductResponse;

import java.util.List;

public interface MDepositProductService {

    AppSuccessHandler<List<MDepositProductResponse>> getAllDepositProduct();

    AppSuccessHandler<MDepositProductResponse> getDepositProductById(Long dpId);

    AppSuccessHandler<MDepositProductResponse> createDepositProduct(MDepositProductRequest depositProductRequest, String role);

    AppSuccessHandler<MDepositProductResponse> updateDepositProduct(Long dpId, MDepositProductRequest depositProductRequest, String role);

    AppSuccessHandler<Object> deleteDepositProduct(Long dpId, String role);

    MDepositProduct getDepositProductEntityById(Long dpId);

    MDepositProduct getDepositProductEntityByName(String name);

}
