package com.adasoraninda.parameterservice.controller;


import com.adasoraninda.parameterservice.controller.endpoint.MCityEndPoint;
import com.adasoraninda.parameterservice.model.entity.MCity;
import com.adasoraninda.parameterservice.model.request.MCityRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MCityResponse;
import com.adasoraninda.parameterservice.service.MCityService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.adasoraninda.parameterservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.parameterservice.controller.AppRequestUtil.ROLE_PARAM;


@RestController
@AllArgsConstructor
@RequestMapping(path = MCityEndPoint.pathBase)
public class MCityController {

    private final MCityService service;

    @GetMapping
    public BaseResponse<List<MCityResponse>> getAllCity() {
        var response = service.getAllCities();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MCityEndPoint.pathId)
    public BaseResponse<MCityResponse> getCityById(
            @PathVariable(value = MCityEndPoint.variableId) Long id
    ) {
        var response = service.getCityById(id);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public BaseResponse<MCityResponse> createCity(
            @Validated @RequestBody MCityRequest city,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role
    ) {
        var response = service.createCity(city, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MCityEndPoint.pathId)
    public BaseResponse<MCityResponse> updateCity(
            @PathVariable(value = MCityEndPoint.variableId) Long id,
            @Validated @RequestBody MCityRequest city,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role
    ) {
        var response = service.updateCity(id, city, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MCityEndPoint.pathId)
    public BaseResponse<Object> deleteCity(
            @PathVariable(value = MCityEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role
    ) {
        var response = service.deleteCity(id, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MCityEndPoint.pathIdEntity)
    public MCity getCityEntityById(
            @PathVariable(value = MCityEndPoint.variableId) Long id
    ) {
        return service.getCityEntityById(id);
    }

}




