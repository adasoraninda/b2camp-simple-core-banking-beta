package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.RReligionEndPoint;
import com.adasoraninda.parameterservice.model.entity.RReligion;
import com.adasoraninda.parameterservice.model.request.RReligionRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.RReligionResponse;
import com.adasoraninda.parameterservice.service.RReligionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = RReligionEndPoint.pathBase)
public class RReligionController {

    private final RReligionService service;

    @GetMapping
    public BaseResponse<List<RReligionResponse>> getAllReligions() {
        var response = service.getAllReligions();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = RReligionEndPoint.pathId)
    public BaseResponse<RReligionResponse> getReligionById(
            @PathVariable(value = RReligionEndPoint.variableId) Long id
    ) {
        var response = service.getReligionById(id);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<RReligionResponse> createReligion(
            @Valid @RequestBody RReligionRequest religionRequest
    ) {
        var response = service.createReligion(religionRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = RReligionEndPoint.pathId)
    public BaseResponse<RReligionResponse> updateReligionById(
            @PathVariable(value = RReligionEndPoint.variableId) Long religionId,
            @Valid @RequestBody RReligionRequest religionRequest
    ) {
        var response = service.updateReligionById(
                religionId,
                religionRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = RReligionEndPoint.pathId)
    public BaseResponse<Object> deleteReligionById(
            @PathVariable(value = RReligionEndPoint.variableId) Long religionId
    ) {
        var response = service.deleteReligionById(religionId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                null);
    }

    @GetMapping(path = RReligionEndPoint.pathIdEntity)
    public RReligion getReligionEntityById(
            @PathVariable(value = RReligionEndPoint.variableId) Long id
    ) {
        return service.getReligionEntityById(id);
    }

}
