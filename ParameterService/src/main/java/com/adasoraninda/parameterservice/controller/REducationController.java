package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.REducationEndPoint;
import com.adasoraninda.parameterservice.model.entity.REducation;
import com.adasoraninda.parameterservice.model.request.REducationRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.REducationResponse;
import com.adasoraninda.parameterservice.service.REducationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = REducationEndPoint.pathBase)
public class REducationController {

    private final REducationService service;

    @GetMapping
    public BaseResponse<List<REducationResponse>> getAllEducations() {
        var response = service.getAllEducations();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = REducationEndPoint.pathId)
    public BaseResponse<REducationResponse> getEducationById(
            @PathVariable(value = REducationEndPoint.variableId) Long id
    ) {
        var response = service.getEducationById(id);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<REducationResponse> createEducation(
            @Valid @RequestBody REducationRequest educationRequest
    ) {
        var response = service.createEducation(educationRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = REducationEndPoint.pathId)
    public BaseResponse<REducationResponse> updateEducationById(
            @PathVariable(value = REducationEndPoint.variableId) Long educationId,
            @Valid @RequestBody REducationRequest educationRequest
    ) {
        var response = service.updateEducationById(educationId, educationRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = REducationEndPoint.pathId)
    public BaseResponse<REducationResponse> deleteEducationById(
            @PathVariable(value = REducationEndPoint.variableId) Long educationId
    ) {
        var response = service.deleteEducationById(educationId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                null);
    }

    @GetMapping(path = REducationEndPoint.pathIdEntity)
    public REducation getEducationEntityById(
            @PathVariable(value = REducationEndPoint.variableId) Long id
    ) {
        return service.getEducationEntityById(id);
    }

}
