package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.RStatusEndPoint;
import com.adasoraninda.parameterservice.model.entity.RStatus;
import com.adasoraninda.parameterservice.model.request.RStatusRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.RStatusResponse;
import com.adasoraninda.parameterservice.service.RStatusService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = RStatusEndPoint.pathBase)
public class RStatusController {

    private final RStatusService service;

    @GetMapping
    public BaseResponse<List<RStatusResponse>> getAllStatus() {
        var response = service.getAllStatus();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = RStatusEndPoint.pathId)
    public BaseResponse<RStatusResponse> getStatusById(
            @PathVariable(value = RStatusEndPoint.variableId) Long statusId
    ) {
        var response = service.getStatusById(statusId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<RStatusResponse> createStatus(
            @Valid @RequestBody RStatusRequest statusRequest
    ) {
        var response = service.createStatus(statusRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = RStatusEndPoint.pathId)
    public BaseResponse<RStatusResponse> updateStatusById(
            @PathVariable(value = RStatusEndPoint.variableId) Long statusId,
            @Valid @RequestBody RStatusRequest statusRequest
    ) {
        var response = service.updateStatusById(statusId, statusRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = RStatusEndPoint.pathId)
    public BaseResponse<RStatusResponse> deleteStatusById(
            @PathVariable(value = RStatusEndPoint.variableId) Long statusId
    ) {
        var response = service.deleteStatusById(statusId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = RStatusEndPoint.pathIdEntity)
    public RStatus getStatusEntityById(
            @PathVariable(value = RStatusEndPoint.variableId) Long statusId
    ) {
        return service.getStatusEntityById(statusId);
    }

    @GetMapping(path = RStatusEndPoint.pathCodeEntity)
    public RStatus getStatusEntityByCode(
            @PathVariable(value = RStatusEndPoint.variableCode) String code
    ) {
        return service.getStatusEntityByCode(code);
    }

}
