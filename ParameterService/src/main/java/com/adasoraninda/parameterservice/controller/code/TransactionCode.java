package com.adasoraninda.parameterservice.controller.code;

public enum TransactionCode {
    SETOR_TUNAI(1001), TARIK_TUNAI(1002), OVERBOOK(1003);

    private final int code;

    TransactionCode(int code) {
        this.code = code;
    }

    public static boolean checkCode(int code) {
        return code != SETOR_TUNAI.code &&
                code != TARIK_TUNAI.code &&
                code != OVERBOOK.code;
    }

}
