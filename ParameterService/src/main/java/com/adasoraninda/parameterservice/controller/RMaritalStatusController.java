package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.RMaritalStatusEndPoint;
import com.adasoraninda.parameterservice.model.entity.RMaritalStatus;
import com.adasoraninda.parameterservice.model.request.RMaritalStatusRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.RMaritalStatusResponse;
import com.adasoraninda.parameterservice.service.RMaritalStatusService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = RMaritalStatusEndPoint.pathBase)
public class RMaritalStatusController {

    private final RMaritalStatusService service;

    @GetMapping
    public BaseResponse<List<RMaritalStatusResponse>> getAllMaritalStatus() {
        var response = service.getAllMaritalStatus();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = RMaritalStatusEndPoint.pathId)
    public BaseResponse<RMaritalStatusResponse> getMaritalStatusById(
            @PathVariable(value = RMaritalStatusEndPoint.variableId) Long id
    ) {
        var response = service.getMaritalStatusById(id);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<RMaritalStatusResponse> createMaritalStatus(
            @Valid @RequestBody RMaritalStatusRequest maritalStatusRequest
    ) {
        var response = service.createMaritalStatus(maritalStatusRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = RMaritalStatusEndPoint.pathId)
    public BaseResponse<RMaritalStatusResponse> updateMaritalStatusById(
            @PathVariable Long maritalStatusId,
            @Valid @RequestBody RMaritalStatusRequest maritalStatusRequest
    ) {
        var response = service.updateMaritalStatusById(maritalStatusId, maritalStatusRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = RMaritalStatusEndPoint.pathId)
    public BaseResponse<Object> deleteMaritalStatusById(
            @PathVariable Long maritalStatusId
    ) {
        var response = service.deleteMaritalStatusById(maritalStatusId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                null);
    }

    @GetMapping(path = RMaritalStatusEndPoint.pathIdEntity)
    public RMaritalStatus getMaritalStatusEntityById(
            @PathVariable(value = RMaritalStatusEndPoint.variableId) Long id
    ) {
        return service.getMaritalStatusEntityById(id);
    }

}
