package com.adasoraninda.parameterservice.controller.endpoint;

public final class MCoaEndPoint {
    public static final String variableId = "id";
    public static final String variableCode = "id";
    public static final String pathBase = "coa";
    public static final String pathId = "/{" + variableId + "}";
    public static final String pathCode = "/{" + variableCode + "}";
    public static final String pathIdEntity = pathId + "/entity/id";
    public static final String pathCodeEntity = pathCode + "/entity/code";
}
