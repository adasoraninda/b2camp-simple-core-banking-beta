package com.adasoraninda.parameterservice.controller.endpoint;

public final class REducationEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "educations";
    public static final String pathId = "/{" + variableId + "}";
    public static final String pathIdEntity = pathId + "/entity";
}
