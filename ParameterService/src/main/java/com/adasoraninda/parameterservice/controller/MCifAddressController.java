package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.MCifAddressEndPoint;
import com.adasoraninda.parameterservice.model.request.MCifAddressRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MCifAddressResponse;
import com.adasoraninda.parameterservice.service.MCifAddressService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.adasoraninda.parameterservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.parameterservice.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MCifAddressEndPoint.pathBase)
public class MCifAddressController {

    private final MCifAddressService service;

    @GetMapping
    public BaseResponse<List<MCifAddressResponse>> getAddresses() {
        var response = service.getAddresses();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MCifAddressEndPoint.pathId)
    public BaseResponse<MCifAddressResponse> getAddressById(
            @PathVariable(value = MCifAddressEndPoint.variableId) Long addressId
    ) {
        var response = service.getAddressById(addressId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponse<MCifAddressResponse> createAddress(
            @Valid @RequestBody MCifAddressRequest addressRequest,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        var response = service.createAddress(addressRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MCifAddressEndPoint.pathId)
    public BaseResponse<MCifAddressResponse> updateAddress(
            @PathVariable(value = MCifAddressEndPoint.variableId) Long addressId,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role,
            @Valid @RequestBody MCifAddressRequest addressRequest
    ) {
        var response = service.updateAddress(addressId, role, addressRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MCifAddressEndPoint.pathId)
    public BaseResponse<Object> deleteAddressById(
            @PathVariable(value = MCifAddressEndPoint.variableId) Long addressId,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        var response = service.deleteAddressById(addressId, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

}
