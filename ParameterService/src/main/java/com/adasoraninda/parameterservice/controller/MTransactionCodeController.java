package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.MTransactionCodeEndPoint;
import com.adasoraninda.parameterservice.model.entity.MTransactionCode;
import com.adasoraninda.parameterservice.model.request.MTransactionCodeRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MTransactionCodeResponse;
import com.adasoraninda.parameterservice.service.MTransactionCodeService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = MTransactionCodeEndPoint.pathBase)
public class MTransactionCodeController {

    private final MTransactionCodeService service;

    @GetMapping
    public BaseResponse<List<MTransactionCodeResponse>> getAllTransactionCode() {
        var response = service.getAllTransactionCode();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MTransactionCodeEndPoint.pathId)
    public BaseResponse<MTransactionCodeResponse> getTransactionCodeById(
            @PathVariable(value = MTransactionCodeEndPoint.variableId) Long transactionCodeId
    ) {
        var response = service.getTransactionCodeById(transactionCodeId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<MTransactionCodeResponse> createTransactionCode(
            @Valid @RequestBody MTransactionCodeRequest transactionCodeRequest,
            @RequestParam(name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        var response = service.createTransactionCode(
                transactionCodeRequest,
                role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MTransactionCodeEndPoint.pathId)
    public BaseResponse<MTransactionCodeResponse> updateTransactionCode(
            @PathVariable(value = MTransactionCodeEndPoint.variableId) Long transactionCodeId,
            @RequestParam(name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody MTransactionCodeRequest transactionCodeRequest) {
        var response = service.updateTransactionCode(
                transactionCodeId,
                role,
                transactionCodeRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MTransactionCodeEndPoint.pathId)
    public BaseResponse<Object> deleteTransactionCodeById(
            @PathVariable(value = MTransactionCodeEndPoint.variableId) Long transactionCodeId,
            @RequestParam(name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        var response = service.deleteTransactionCodeById(transactionCodeId, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MTransactionCodeEndPoint.pathIdEntity)
    public MTransactionCode getTransactionCodeEntityById(
            @PathVariable(value = MTransactionCodeEndPoint.variableId) Long transactionCodeId
    ) {
        return service.getTransactionCodeEntityById(transactionCodeId);
    }

    @GetMapping(path = MTransactionCodeEndPoint.pathCodeEntity)
    public MTransactionCode getTransactionCodeEntityByCode(
            @PathVariable(value = MTransactionCodeEndPoint.variableCode) Integer code
    ) {
        return service.getTransactionCodeEntityByCode(code);
    }

}
