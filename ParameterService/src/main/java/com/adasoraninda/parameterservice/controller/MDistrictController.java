package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.MDistrictEndPoint;
import com.adasoraninda.parameterservice.model.entity.MDistrict;
import com.adasoraninda.parameterservice.model.request.MDistrictRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MDistrictResponse;
import com.adasoraninda.parameterservice.service.MDistrictService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.adasoraninda.parameterservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.parameterservice.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MDistrictEndPoint.pathBase)
public class MDistrictController {

    private final MDistrictService service;

    @GetMapping
    public BaseResponse<List<MDistrictResponse>> getAllDistricts() {
        var response = service.getAllDistricts();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MDistrictEndPoint.pathId)
    public BaseResponse<MDistrictResponse> getDistrictById(
            @PathVariable(value = MDistrictEndPoint.variableId) Long id
    ) {
        var response = service.getDistrictById(id);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<MDistrictResponse> createDistrict(
            @Validated @RequestBody MDistrictRequest district,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role
    ) {
        var response = service.createDistrict(district, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MDistrictEndPoint.pathId)
    public BaseResponse<MDistrictResponse> updateDistrict(
            @PathVariable(value = MDistrictEndPoint.variableId) Long id,
            @Validated @RequestBody MDistrictRequest district,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role
    ) {
        var response = service.updateDistrict(id, district, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MDistrictEndPoint.pathId)
    public BaseResponse<Object> deleteDistrict(
            @PathVariable(value = MDistrictEndPoint.variableId) Long id,
            @RequestParam(name = ROLE_PARAM, required = false, defaultValue = DEFAULT_ROLE) String role
    ) {
        var response = service.deleteDistrict(id, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MDistrictEndPoint.pathIdEntity)
    public MDistrict getDistrictEntityById(
            @PathVariable(value = MDistrictEndPoint.variableId) Long id
    ) {
        return service.getDistrictEntityById(id);
    }

}
