package com.adasoraninda.parameterservice.controller.endpoint;

public final class RReligionEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "religions";
    public static final String pathId = "/{" + variableId + "}";
    public static final String pathIdEntity = pathId + "/entity";
}
