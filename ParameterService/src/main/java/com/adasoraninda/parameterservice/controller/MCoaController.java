package com.adasoraninda.parameterservice.controller;


import com.adasoraninda.parameterservice.controller.endpoint.MCoaEndPoint;
import com.adasoraninda.parameterservice.model.entity.MCoa;
import com.adasoraninda.parameterservice.model.request.MCoaRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MCoaResponse;
import com.adasoraninda.parameterservice.service.MCoaService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = MCoaEndPoint.pathBase)
public class MCoaController {

    private final MCoaService service;

    @GetMapping
    public BaseResponse<List<MCoaResponse>> getAllCoa() {
        var response = service.getAllCoa();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MCoaEndPoint.pathId)
    public BaseResponse<MCoaResponse> getCoaById(
            @PathVariable(value = MCoaEndPoint.variableId) Long caoId
    ) {
        var response = service.getCoaById(caoId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<MCoaResponse> createCoa(
            @Valid @RequestBody MCoaRequest caoRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role
    ) {
        var response = service.createCoa(caoRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MCoaEndPoint.pathId)
    public BaseResponse<MCoaResponse> updateCoa(
            @PathVariable(value = MCoaEndPoint.variableId) Long caoId,
            @Valid @RequestBody MCoaRequest caoRequest,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role
    ) {
        var response = service.updateCoa(caoId, caoRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MCoaEndPoint.pathId)
    public BaseResponse<Object> deleteCoa(
            @PathVariable(value = MCoaEndPoint.variableId) Long caoId,
            @RequestParam(
                    name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        var response = service.deleteCoa(caoId, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MCoaEndPoint.pathIdEntity)
    public MCoa getCoaEntityById(
            @PathVariable(value = MCoaEndPoint.variableId) Long caoId
    ) {
        return service.getCoaEntityById(caoId);
    }

    @GetMapping(path = MCoaEndPoint.pathCodeEntity)
    public MCoa getCoaEntityByCode(
            @PathVariable(value = MCoaEndPoint.variableCode) String code
    ) {
        return service.getCoaEntityByCode(code);
    }

}
