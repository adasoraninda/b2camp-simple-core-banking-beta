package com.adasoraninda.parameterservice.controller.endpoint;

public final class MCifFamilyEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "families";
    public static final String pathId = "/{" + variableId + "}";
}
