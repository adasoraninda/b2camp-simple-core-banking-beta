package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.MDepositProductEndPoint;
import com.adasoraninda.parameterservice.model.entity.MDepositProduct;
import com.adasoraninda.parameterservice.model.request.MDepositProductRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MDepositProductResponse;
import com.adasoraninda.parameterservice.service.MDepositProductService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.adasoraninda.parameterservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.parameterservice.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MDepositProductEndPoint.pathBase)
public class MDepositProductController {

    private final MDepositProductService service;

    @GetMapping
    public BaseResponse<List<MDepositProductResponse>> getAllDepositProduct() {
        var response = service.getAllDepositProduct();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MDepositProductEndPoint.pathId)
    public BaseResponse<MDepositProductResponse> getDepositProductById(
            @PathVariable(value = MDepositProductEndPoint.variableId) Long dpId
    ) {
        var response = service.getDepositProductById(dpId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<MDepositProductResponse> createDepositProduct(
            @Valid @RequestBody MDepositProductRequest depositProductRequest,
            @RequestParam(
                    name = ROLE_PARAM,
                    required = false,
                    defaultValue = DEFAULT_ROLE) String role
    ) {
        var response =
                service.createDepositProduct(depositProductRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MDepositProductEndPoint.pathId)
    public BaseResponse<MDepositProductResponse> updateDepositProduct(
            @PathVariable(value = MDepositProductEndPoint.variableId) Long dpId,
            @Valid @RequestBody MDepositProductRequest depositProductRequest,
            @RequestParam(
                    name = ROLE_PARAM,
                    required = false,
                    defaultValue = DEFAULT_ROLE) String role) {
        var response =
                service.updateDepositProduct(dpId, depositProductRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MDepositProductEndPoint.pathId)
    public BaseResponse<Object> deleteDepositProduct(
            @PathVariable(value = MDepositProductEndPoint.variableId) Long dpId,
            @RequestParam(
                    name = ROLE_PARAM,
                    required = false,
                    defaultValue = DEFAULT_ROLE) String role) {
        var response = service.deleteDepositProduct(dpId, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MDepositProductEndPoint.pathIdEntity)
    public MDepositProduct getDepositProductEntityById(
            @PathVariable(value = MDepositProductEndPoint.variableId) Long dpId
    ) {
        return service.getDepositProductEntityById(dpId);
    }

    @GetMapping(path = MDepositProductEndPoint.pathNameEntity)
    public MDepositProduct getDepositProductEntityByName(
            @PathVariable(value = MDepositProductEndPoint.variableName) String name
    ) {
        return service.getDepositProductEntityByName(name);
    }

}
