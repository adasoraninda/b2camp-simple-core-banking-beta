package com.adasoraninda.parameterservice.controller;

import com.adasoraninda.parameterservice.controller.endpoint.MSavingProductEndPoint;
import com.adasoraninda.parameterservice.model.entity.MSavingProduct;
import com.adasoraninda.parameterservice.model.request.MSavingProductRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MSavingProductResponse;
import com.adasoraninda.parameterservice.service.MSavingProductService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(path = MSavingProductEndPoint.pathBase)
public class MSavingProductController {

    private final MSavingProductService service;

    @GetMapping
    public BaseResponse<List<MSavingProductResponse>> getAllSavingProduct() {
        var response = service.getAllSavingProduct();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MSavingProductEndPoint.pathId)
    public BaseResponse<MSavingProductResponse> getSavingProductById(
            @PathVariable(value = MSavingProductEndPoint.variableId) Long savingProductId
    ) {
        var response = service.getSavingProductById(savingProductId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    public BaseResponse<MSavingProductResponse> createSavingProduct(
            @Valid @RequestBody MSavingProductRequest savingProductRequest,
            @RequestParam(name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        var response = service.createSavingProduct(savingProductRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MSavingProductEndPoint.pathId)
    public BaseResponse<MSavingProductResponse> updateSavingProduct(
            @PathVariable(value = MSavingProductEndPoint.variableId) Long savingProductId,
            @RequestParam(name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role,
            @Valid @RequestBody MSavingProductRequest savingProductRequest) {
        var response = service.updateSavingProduct(savingProductId, role, savingProductRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MSavingProductEndPoint.pathId)
    public BaseResponse<Object> deleteSavingProductById(
            @PathVariable(value = MSavingProductEndPoint.variableId) Long savingProductId,
            @RequestParam(name = AppRequestUtil.ROLE_PARAM,
                    required = false,
                    defaultValue = AppRequestUtil.DEFAULT_ROLE) String role) {
        var response = service.deleteSavingProductById(savingProductId, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MSavingProductEndPoint.pathIdEntity)
    public MSavingProduct getSavingProductEntityById(
            @PathVariable(value = MSavingProductEndPoint.variableId) Long id
    ) {
        return service.getSavingProductEntityById(id);
    }

    @GetMapping(path = MSavingProductEndPoint.pathNameEntity)
    public MSavingProduct getSavingProductEntityByName(
            @PathVariable(value = MSavingProductEndPoint.variableName) String name
    ) {
        return service.getSavingProductEntityByName(name);
    }

}
