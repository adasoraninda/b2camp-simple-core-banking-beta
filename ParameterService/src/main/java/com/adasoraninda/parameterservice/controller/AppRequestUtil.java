package com.adasoraninda.parameterservice.controller;

public final class AppRequestUtil {
    public static final String ROLE_PARAM = "role";
    public static final String DEFAULT_ROLE = "system";
    public static final String COMPANY_CIF_TYPE = "company";

    public static final int MAX_WORK = 5;
}
