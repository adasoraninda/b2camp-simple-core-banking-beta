package com.adasoraninda.parameterservice.controller.code;

public enum StatusCode {
    ACTIVE, INACTIVE;

    public static boolean checkStatus(String status) {
        return !status.equals(ACTIVE.name()) &&
                !status.equals(INACTIVE.name());
    }
}
