package com.adasoraninda.parameterservice.controller;


import com.adasoraninda.parameterservice.controller.endpoint.MCifWorkEndPoint;
import com.adasoraninda.parameterservice.model.request.MCifWorkRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MCifWorkResponse;
import com.adasoraninda.parameterservice.service.MCifWorkService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.adasoraninda.parameterservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.parameterservice.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MCifWorkEndPoint.pathBase)
public class MCifWorkController {

    private final MCifWorkService service;

    @GetMapping
    public BaseResponse<List<MCifWorkResponse>> getWorks() {
        var response = service.getWorks();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MCifWorkEndPoint.pathId)
    public BaseResponse<MCifWorkResponse> getWorkById(
            @PathVariable(value = MCifWorkEndPoint.variableId) Long workId
    ) {
        var response = service.getWorkById(workId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponse<MCifWorkResponse> createWork(
            @Valid @RequestBody MCifWorkRequest workRequest,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        var response = service.createWork(workRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MCifWorkEndPoint.pathId)
    public BaseResponse<MCifWorkResponse> updateWork(
            @PathVariable(value = MCifWorkEndPoint.variableId) Long workId,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role,
            @Valid @RequestBody MCifWorkRequest workRequest
    ) {
        var response = service.updateWork(workId, role, workRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MCifWorkEndPoint.pathId)
    public BaseResponse<Object> deleteWorkById(
            @PathVariable(value = MCifWorkEndPoint.variableId) Long workId,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        var response = service.deleteWorkById(workId, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

}
