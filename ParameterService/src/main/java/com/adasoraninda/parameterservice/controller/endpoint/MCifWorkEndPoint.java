package com.adasoraninda.parameterservice.controller.endpoint;

public final class MCifWorkEndPoint {
    public static final String variableId = "id";
    public static final String pathBase = "works";
    public static final String pathId = "/{" + variableId + "}";
}
