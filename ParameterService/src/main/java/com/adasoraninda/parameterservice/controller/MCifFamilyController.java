package com.adasoraninda.parameterservice.controller;


import com.adasoraninda.parameterservice.controller.endpoint.MCifFamilyEndPoint;
import com.adasoraninda.parameterservice.model.request.MCifFamilyRequest;
import com.adasoraninda.parameterservice.model.response.BaseResponse;
import com.adasoraninda.parameterservice.model.response.MCifFamilyResponse;
import com.adasoraninda.parameterservice.service.MCifFamilyService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.adasoraninda.parameterservice.controller.AppRequestUtil.DEFAULT_ROLE;
import static com.adasoraninda.parameterservice.controller.AppRequestUtil.ROLE_PARAM;

@RestController
@AllArgsConstructor
@RequestMapping(path = MCifFamilyEndPoint.pathBase)
public class MCifFamilyController {

    private final MCifFamilyService service;

    @GetMapping
    public BaseResponse<List<MCifFamilyResponse>> getFamilies() {
        var response = service.getFamilies();

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @GetMapping(path = MCifFamilyEndPoint.pathId)
    public BaseResponse<MCifFamilyResponse> getFamilyById(
            @PathVariable(value = MCifFamilyEndPoint.variableId) Long familyId
    ) {
        var response = service.getFamilyById(familyId);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponse<MCifFamilyResponse> createFamily(
            @Valid @RequestBody MCifFamilyRequest familyRequest,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        var response = service.createFamily(familyRequest, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @PutMapping(path = MCifFamilyEndPoint.pathId)
    public BaseResponse<MCifFamilyResponse> updateFamily(
            @PathVariable(value = MCifFamilyEndPoint.variableId) Long familyId,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role,
            @Valid @RequestBody MCifFamilyRequest familyRequest
    ) {
        var response = service.updateFamily(familyId, role, familyRequest);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

    @DeleteMapping(path = MCifFamilyEndPoint.pathId)
    public BaseResponse<Object> deleteFamilyById(
            @PathVariable(value = MCifFamilyEndPoint.variableId) Long familyId,
            @RequestParam(name = ROLE_PARAM, defaultValue = DEFAULT_ROLE, required = false) String role
    ) {
        var response = service.deleteFamilyById(familyId, role);

        return BaseResponse.success(
                response.getSuccessMessage().getMessage(),
                response.getData());
    }

}
