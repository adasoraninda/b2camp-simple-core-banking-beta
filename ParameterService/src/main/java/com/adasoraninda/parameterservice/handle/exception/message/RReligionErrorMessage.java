package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.RReligionErrorCode;
import com.adasoraninda.parameterservice.handle.AppMessage;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class RReligionErrorMessage implements AppMessage {

    private final Map<RReligionErrorCode, Object> data;
    private final RReligionErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case RELIGION_IS_EMPTY:
                return errorIsEmpty();
            case RELIGION_IS_NOT_FOUND:
                return errorNotFoundWithId();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data agama dengan id " + data.get(RReligionErrorCode.RELIGION_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data agama tidak ada";
    }
}
