package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RStatusSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAllStatus();
            case GET_DATA:
                return successGetStatus();
            case CREATE_DATA:
                return successCreateStatus();
            case UPDATE_DATA:
                return successUpdateStatus();
            case DELETE_DATA:
                return successDeleteStatus();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAllStatus() {
        return "Berhasil mendapatkan semua data status";
    }

    private String successGetStatus() {
        return "Berhasil mendapatkan data status";
    }

    private String successCreateStatus() {
        return "Berhasil menambahkan data status";
    }

    private String successUpdateStatus() {
        return "Berhasil mengubah data status";
    }

    private String successDeleteStatus() {
        return "Berhasil menghapus datastatus";
    }
}
