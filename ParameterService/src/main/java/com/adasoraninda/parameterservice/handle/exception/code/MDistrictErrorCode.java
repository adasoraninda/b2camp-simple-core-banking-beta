package com.adasoraninda.parameterservice.handle.exception.code;

public enum MDistrictErrorCode {
    DISTRICT_IS_EMPTY,
    DISTRICT_IS_NOT_FOUND;
}
