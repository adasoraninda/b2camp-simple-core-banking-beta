package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MCifWorkSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetWorks();
            case GET_DATA:
                return successGetWork();
            case CREATE_DATA:
                return successCreateWork();
            case UPDATE_DATA:
                return successUpdateWork();
            case DELETE_DATA:
                return successDeleteWork();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetWorks() {
        return "Berhasil mendapatkan semua data pekerjaan";
    }

    private String successGetWork() {
        return "Berhasil mendapatkan data pekerjaan";
    }

    private String successCreateWork() {
        return "Berhasil menambahkan data pekerjaan";
    }

    private String successUpdateWork() {
        return "Berhasil mengubah data pekerjaan";
    }

    private String successDeleteWork() {
        return "Berhasil menghapus data pekerjaan";
    }

}
