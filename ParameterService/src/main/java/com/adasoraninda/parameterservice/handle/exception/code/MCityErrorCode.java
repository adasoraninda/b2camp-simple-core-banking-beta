package com.adasoraninda.parameterservice.handle.exception.code;

public enum MCityErrorCode {
    CITY_IS_EMPTY,
    CITY_IS_NOT_FOUND;
}
