package com.adasoraninda.parameterservice.handle.exception.code;

public enum RMaritalStatsErrorCode {
    MARITAL_STATUS_IS_EMPTY,
    MARITAL_STATUS_IS_NOT_FOUND;
}
