package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RMaritalStatusSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAllMaritalStatus();
            case GET_DATA:
                return successGetMaritalStatus();
            case CREATE_DATA:
                return successCreateMaritalStatus();
            case UPDATE_DATA:
                return successUpdateMaritalStatus();
            case DELETE_DATA:
                return successDeleteMaritalStatus();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAllMaritalStatus() {
        return "Berhasil mendapatkan semua data pernikahan";
    }

    private String successGetMaritalStatus() {
        return "Berhasil mendapatkan data pernikahan";
    }

    private String successCreateMaritalStatus() {
        return "Berhasil menambahkan data pernikahan";
    }

    private String successUpdateMaritalStatus() {
        return "Berhasil mengubah data pernikahan";
    }

    private String successDeleteMaritalStatus() {
        return "Berhasil menghapus data pernikahan";
    }
}