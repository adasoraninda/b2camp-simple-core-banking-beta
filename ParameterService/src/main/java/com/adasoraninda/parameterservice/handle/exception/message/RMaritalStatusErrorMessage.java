package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.RMaritalStatsErrorCode;
import com.adasoraninda.parameterservice.handle.AppMessage;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.RMaritalStatsErrorCode.MARITAL_STATUS_IS_NOT_FOUND;

@AllArgsConstructor
public class RMaritalStatusErrorMessage implements AppMessage {

    private final Map<RMaritalStatsErrorCode, Object> data;
    private final RMaritalStatsErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case MARITAL_STATUS_IS_EMPTY:
                return errorIsEmpty();
            case MARITAL_STATUS_IS_NOT_FOUND:
                return errorNotFoundWithId();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data pernikana dengan id " + data.get(MARITAL_STATUS_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data pernikahan tidak ada";
    }
}