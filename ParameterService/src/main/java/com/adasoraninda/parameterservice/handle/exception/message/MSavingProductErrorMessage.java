package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MSavingProductErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.MSavingProductErrorCode.SAVING_PRODUCT_IS_NOT_FOUND;
import static com.adasoraninda.parameterservice.handle.exception.code.MSavingProductErrorCode.SAVING_PRODUCT_NAME_IS_NOT_FOUND;

@AllArgsConstructor
public class MSavingProductErrorMessage implements AppMessage {

    private final Map<MSavingProductErrorCode, Object> data;
    private final MSavingProductErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case SAVING_PRODUCT_IS_EMPTY:
                return errorIsEmpty();
            case SAVING_PRODUCT_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case SAVING_PRODUCT_NAME_IS_NOT_FOUND:
                return errorNotFoundWithName();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithName() {
        return "Data tabungan dengan nama " + data.get(SAVING_PRODUCT_NAME_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorNotFoundWithId() {
        return "Data tabungan dengan id " + data.get(SAVING_PRODUCT_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data tabungan tidak ada";
    }

}
