package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.REducationErrorCode;
import com.adasoraninda.parameterservice.handle.AppMessage;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class REducationErrorMessage implements AppMessage {

    private final Map<REducationErrorCode, Object> data;
    private final REducationErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case EDUCATION_IS_EMPTY:
                return errorIsEmpty();
            case EDUCATION_IS_NOT_FOUND:
                return errorNotFoundWithId();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data pendidikan dengan id " + data.get(REducationErrorCode.EDUCATION_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data pendidikan tidak ada";
    }
}
