package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MTransactionCodeErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.MTransactionCodeErrorCode.TRANSACTION_CODE_INVALID;
import static com.adasoraninda.parameterservice.handle.exception.code.MTransactionCodeErrorCode.TRANSACTION_CODE_IS_NOT_FOUND;

@AllArgsConstructor
public class MTransactionCodeErrorMessage implements AppMessage {

    private final Map<MTransactionCodeErrorCode, Object> data;
    private final MTransactionCodeErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case TRANSACTION_CODE_IS_EMPTY:
                return errorIsEmpty();
            case TRANSACTION_CODE_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case TRANSACTION_CODE_INVALID:
                return errorCodeNotValid();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorCodeNotValid() {
        return "Data kode transaksi " + data.get(TRANSACTION_CODE_INVALID) + " tidak ada";
    }

    private String errorNotFoundWithId() {
        return "Data kode transaksi dengan id " + data.get(TRANSACTION_CODE_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data kode transaksi tidak ada";
    }
}
