package com.adasoraninda.parameterservice.handle.exception.code;

public enum MCifErrorCode {
    CIF_IS_NOT_FOUND,
    CIF_NIK_IS_NOT_FOUND,
    CIF_IS_EMPTY,
    CIF_NPWP_MUST_FILLED;
}
