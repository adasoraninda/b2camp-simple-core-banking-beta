package com.adasoraninda.parameterservice.handle.exception.code;

public enum MTransactionCodeErrorCode {
    TRANSACTION_CODE_IS_EMPTY,
    TRANSACTION_CODE_IS_NOT_FOUND,
    TRANSACTION_CODE_INVALID
}
