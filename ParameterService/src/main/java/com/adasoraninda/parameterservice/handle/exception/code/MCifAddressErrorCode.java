package com.adasoraninda.parameterservice.handle.exception.code;

public enum MCifAddressErrorCode {
    CIF_ADDRESS_IS_NOT_FOUND,
    CIF_ADDRESS_IS_EMPTY;
}
