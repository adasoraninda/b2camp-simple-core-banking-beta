package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MDistrictErrorCode;
import com.adasoraninda.parameterservice.handle.AppMessage;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class MDistrictErrorMessage implements AppMessage {

    private final Map<MDistrictErrorCode, Object> data;
    private final MDistrictErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case DISTRICT_IS_EMPTY:
                return errorIsEmpty();
            case DISTRICT_IS_NOT_FOUND:
                return errorNotFoundWithId();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data distrik dengan id " + data.get(MDistrictErrorCode.DISTRICT_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data distrik tidak ada";
    }
}