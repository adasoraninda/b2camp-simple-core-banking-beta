package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MCityErrorCode;
import com.adasoraninda.parameterservice.handle.AppMessage;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class MCityErrorMessage implements AppMessage {

    private final Map<MCityErrorCode, Object> data;
    private final MCityErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CITY_IS_EMPTY:
                return errorIsEmpty();
            case CITY_IS_NOT_FOUND:
                return errorNotFoundWithId();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data kota dengan id " + data.get(MCityErrorCode.CITY_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data kota tidak ada";
    }
}
