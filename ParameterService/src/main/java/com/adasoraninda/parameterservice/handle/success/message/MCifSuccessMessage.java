package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MCifSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAllCif();
            case GET_DATA:
                return successGetCif();
            case CREATE_DATA:
                return successCreateCif();
            case UPDATE_DATA:
                return successUpdateCif();
            case DELETE_DATA:
                return successDeleteCif();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAllCif() {
        return "Berhasil mendapatkan semua data CIF";
    }

    private String successGetCif() {
        return "Berhasil mendapatkan data CIF";
    }

    private String successCreateCif() {
        return "Berhasil menambahkan data CIF";
    }

    private String successUpdateCif() {
        return "Berhasil mengubah data CIF";
    }

    private String successDeleteCif() {
        return "Berhasil menghapus data CIF";
    }

}
