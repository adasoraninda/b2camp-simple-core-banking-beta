package com.adasoraninda.parameterservice.handle.exception.code;

public enum MCoaErrorCode {
    COA_IS_NOT_FOUND,
    COA_CODE_IS_NOT_FOUND,
    COA_IS_EMPTY;
}
