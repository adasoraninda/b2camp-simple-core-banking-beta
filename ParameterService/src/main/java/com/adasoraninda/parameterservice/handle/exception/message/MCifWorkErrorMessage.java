package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MCifWorkErrorCode;
import com.adasoraninda.parameterservice.handle.AppMessage;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.MCifWorkErrorCode.*;

@AllArgsConstructor
public class MCifWorkErrorMessage implements AppMessage {

    private final Map<MCifWorkErrorCode, Object> data;
    private final MCifWorkErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CIF_WORK_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case CIF_WORK_IS_EMPTY:
                return errorIsEmpty();
            case CIF_WORK_MAX:
                return errorLimit();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data pekerjaan dengan id " + data.get(CIF_WORK_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data pekerjaan tidak ada";
    }

    private String errorLimit() {
        return "Pekerjaan hanya diperbolehkan sebanyak " + data.get(CIF_WORK_MAX) + " pekerjaan";
    }

}
