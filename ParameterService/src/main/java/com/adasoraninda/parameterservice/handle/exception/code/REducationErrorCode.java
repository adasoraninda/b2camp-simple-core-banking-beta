package com.adasoraninda.parameterservice.handle.exception.code;

public enum REducationErrorCode {
    EDUCATION_IS_EMPTY,
    EDUCATION_IS_NOT_FOUND;
}
