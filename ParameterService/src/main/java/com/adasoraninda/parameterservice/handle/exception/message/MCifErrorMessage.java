package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MCifErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.MCifErrorCode.CIF_IS_NOT_FOUND;

@AllArgsConstructor
public class MCifErrorMessage implements AppMessage {

    private final Map<MCifErrorCode, Object> data;
    private final MCifErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CIF_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case CIF_IS_EMPTY:
                return errorIsEmpty();
            case CIF_NIK_IS_NOT_FOUND:
                return errorNotFoundWithNik();
            case CIF_NPWP_MUST_FILLED:
                return errorNpwpMustBeFilled();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithNik() {
        return "CIF dengan NIK " + data.get(CIF_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorNotFoundWithId() {
        return "CIF dengan id " + data.get(CIF_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data CIF tidak ada";
    }

    private String errorNpwpMustBeFilled() {
        return "NPWP dengan tipe COMPANY harus di isi";
    }

}
