package com.adasoraninda.parameterservice.handle.exception.code;

public enum RReligionErrorCode {
    RELIGION_IS_EMPTY,
    RELIGION_IS_NOT_FOUND;
}
