package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.RStatusErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.RStatusErrorCode.STATUS_INVALID;
import static com.adasoraninda.parameterservice.handle.exception.code.RStatusErrorCode.STATUS_IS_NOT_FOUND;

@AllArgsConstructor
public class RStatusErrorMessage implements AppMessage {

    private final Map<RStatusErrorCode, Object> data;
    private final RStatusErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case STATUS_IS_EMPTY:
                return errorIsEmpty();
            case STATUS_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case STATUS_INVALID:
                return errorInvalid();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Status dengan id " + data.get(STATUS_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua status tidak ada";
    }

    private String errorInvalid() {
        return "Status " + data.get(STATUS_INVALID) + " tidak ada";
    }
}
