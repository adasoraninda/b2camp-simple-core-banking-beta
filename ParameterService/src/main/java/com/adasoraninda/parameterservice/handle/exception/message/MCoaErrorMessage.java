package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MCoaErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.MCoaErrorCode.COA_CODE_IS_NOT_FOUND;
import static com.adasoraninda.parameterservice.handle.exception.code.MCoaErrorCode.COA_IS_NOT_FOUND;

@AllArgsConstructor
public class MCoaErrorMessage implements AppMessage {

    private final Map<MCoaErrorCode, Object> data;
    private final MCoaErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case COA_IS_EMPTY:
                return errorIsEmpty();
            case COA_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case COA_CODE_IS_NOT_FOUND:
                return errorNotFoundWithCode();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithCode() {
        return "COA dengan code " + data.get(COA_CODE_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorNotFoundWithId() {
        return "COA dengan id " + data.get(COA_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua COA tabungan tidak ada";
    }
}
