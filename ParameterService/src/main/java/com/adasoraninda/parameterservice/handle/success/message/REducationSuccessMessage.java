package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class REducationSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetEducations();
            case GET_DATA:
                return successGetEducation();
            case CREATE_DATA:
                return successCreateEducation();
            case UPDATE_DATA:
                return successUpdateEducation();
            case DELETE_DATA:
                return successDeleteEducation();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetEducations() {
        return "Berhasil mendapatkan semua data pendidikan";
    }

    private String successGetEducation() {
        return "Berhasil mendapatkan data pendidikan";
    }

    private String successCreateEducation() {
        return "Berhasil menambahkan data pendidikan";
    }

    private String successUpdateEducation() {
        return "Berhasil mengubah data pendidikan";
    }

    private String successDeleteEducation() {
        return "Berhasil menghapus data pendidikan";
    }
}