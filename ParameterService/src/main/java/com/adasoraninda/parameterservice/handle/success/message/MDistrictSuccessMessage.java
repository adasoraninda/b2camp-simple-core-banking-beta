package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MDistrictSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetDistricts();
            case GET_DATA:
                return successGetDistrict();
            case CREATE_DATA:
                return successCreateDistrict();
            case UPDATE_DATA:
                return successUpdateDistrict();
            case DELETE_DATA:
                return successDeleteDistrict();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetDistricts() {
        return "Berhasil mendapatkan semua data distrik";
    }

    private String successGetDistrict() {
        return "Berhasil mendapatkan data distrik";
    }

    private String successCreateDistrict() {
        return "Berhasil menambahkan data distrik";
    }

    private String successUpdateDistrict() {
        return "Berhasil mengubah data distrik";
    }

    private String successDeleteDistrict() {
        return "Berhasil menghapus data distrik";
    }
}