package com.adasoraninda.parameterservice.handle.exception.code;

public enum MSavingProductErrorCode {
    SAVING_PRODUCT_IS_EMPTY,
    SAVING_PRODUCT_IS_NOT_FOUND,
    SAVING_PRODUCT_NAME_IS_NOT_FOUND
}
