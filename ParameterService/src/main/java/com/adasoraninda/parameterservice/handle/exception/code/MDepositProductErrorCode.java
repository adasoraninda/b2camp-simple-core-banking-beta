package com.adasoraninda.parameterservice.handle.exception.code;

public enum MDepositProductErrorCode {
    DEPOSIT_PRODUCT_IS_EMPTY,
    DEPOSIT_PRODUCT_IS_NOT_FOUND,
    DEPOSIT_PRODUCT_NAME_IS_NOT_FOUND;
}
