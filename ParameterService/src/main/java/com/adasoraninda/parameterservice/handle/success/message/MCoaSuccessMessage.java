package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MCoaSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAllCoa();
            case GET_DATA:
                return successGetCoa();
            case CREATE_DATA:
                return successCreateCoa();
            case UPDATE_DATA:
                return successUpdateCoa();
            case DELETE_DATA:
                return successDeleteCoa();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAllCoa() {
        return "Berhasil mendapatkan semua data COA";
    }

    private String successGetCoa() {
        return "Berhasil mendapatkan data COA";
    }

    private String successCreateCoa() {
        return "Berhasil menambahkan data COA";
    }

    private String successUpdateCoa() {
        return "Berhasil mengubah data COA";
    }

    private String successDeleteCoa() {
        return "Berhasil menghapus data COA";
    }
}
