package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MCitySuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetCities();
            case GET_DATA:
                return successGetCity();
            case CREATE_DATA:
                return successCreateCity();
            case UPDATE_DATA:
                return successUpdateCity();
            case DELETE_DATA:
                return successDeleteCity();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetCities() {
        return "Berhasil mendapatkan semua data kota";
    }

    private String successGetCity() {
        return "Berhasil mendapatkan data kota";
    }

    private String successCreateCity() {
        return "Berhasil menambahkan data kota";
    }

    private String successUpdateCity() {
        return "Berhasil mengubah data kota";
    }

    private String successDeleteCity() {
        return "Berhasil menghapus data kota";
    }
}
