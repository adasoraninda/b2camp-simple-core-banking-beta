package com.adasoraninda.parameterservice.handle.exception.code;

public enum MCifWorkErrorCode {
    CIF_WORK_IS_NOT_FOUND,
    CIF_WORK_IS_EMPTY,
    CIF_WORK_MAX;
}
