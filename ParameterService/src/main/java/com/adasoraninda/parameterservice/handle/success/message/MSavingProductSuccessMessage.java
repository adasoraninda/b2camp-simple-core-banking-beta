package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MSavingProductSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAllSaveProduct();
            case GET_DATA:
                return successGetSaveProduct();
            case CREATE_DATA:
                return successCreateSaveProduct();
            case UPDATE_DATA:
                return successUpdateSaveProduct();
            case DELETE_DATA:
                return successDeleteSaveProduct();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAllSaveProduct() {
        return "Berhasil mendapatkan semua data tabungan";
    }

    private String successGetSaveProduct() {
        return "Berhasil mendapatkan data tabungan";
    }

    private String successCreateSaveProduct() {
        return "Berhasil menambahkan data tabungan";
    }

    private String successUpdateSaveProduct() {
        return "Berhasil mengubah data tabungan";
    }

    private String successDeleteSaveProduct() {
        return "Berhasil menghapus data tabungan";
    }
}
