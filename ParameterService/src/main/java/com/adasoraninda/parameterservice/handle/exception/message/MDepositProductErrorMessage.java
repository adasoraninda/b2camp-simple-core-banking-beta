package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MDepositProductErrorCode;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.MDepositProductErrorCode.DEPOSIT_PRODUCT_IS_NOT_FOUND;
import static com.adasoraninda.parameterservice.handle.exception.code.MDepositProductErrorCode.DEPOSIT_PRODUCT_NAME_IS_NOT_FOUND;

@AllArgsConstructor
public class MDepositProductErrorMessage implements AppMessage {

    private final Map<MDepositProductErrorCode, Object> data;
    private final MDepositProductErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case DEPOSIT_PRODUCT_IS_EMPTY:
                return errorIsEmpty();
            case DEPOSIT_PRODUCT_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case DEPOSIT_PRODUCT_NAME_IS_NOT_FOUND:
                return errorNotFoundWithName();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithName() {
        return "Data deposito dengan nama " + data.get(DEPOSIT_PRODUCT_NAME_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorNotFoundWithId() {
        return "Data deposito dengan id " + data.get(DEPOSIT_PRODUCT_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data deposito tidak ada";
    }
}
