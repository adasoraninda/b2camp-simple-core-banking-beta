package com.adasoraninda.parameterservice.handle.exception;


import com.adasoraninda.parameterservice.handle.AppMessage;

public class BusinessException extends RuntimeException {

    private final AppMessage message;

    public BusinessException(AppMessage message) {
        super(message.getMessage());
        this.message = message;
    }

    @Override
    public String toString() {
        return message.getCode();
    }
}
