package com.adasoraninda.parameterservice.handle.exception.code;

public enum MCifFamilyErrorCode {
    CIF_FAMILY_IS_NOT_FOUND,
    CIF_FAMILY_IS_EMPTY;
}
