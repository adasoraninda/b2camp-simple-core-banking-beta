package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MDepositProductSuccessMessage implements AppMessage {
    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAllDepositProduct();
            case GET_DATA:
                return successGetDepositProduct();
            case CREATE_DATA:
                return successCreateDepositProduct();
            case UPDATE_DATA:
                return successUpdateDepositProduct();
            case DELETE_DATA:
                return successDeleteDepositProduct();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAllDepositProduct() {
        return "Berhasil mendapatkan semua data deposito";
    }

    private String successGetDepositProduct() {
        return "Berhasil mendapatkan data deposito";
    }

    private String successCreateDepositProduct() {
        return "Berhasil menambahkan data deposito";
    }

    private String successUpdateDepositProduct() {
        return "Berhasil mengubah data deposito";
    }

    private String successDeleteDepositProduct() {
        return "Berhasil menghapus data deposito";
    }
}
