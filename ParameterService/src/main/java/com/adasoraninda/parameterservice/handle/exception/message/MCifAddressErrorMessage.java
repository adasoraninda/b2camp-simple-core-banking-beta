package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MCifAddressErrorCode;
import com.adasoraninda.parameterservice.handle.AppMessage;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.MCifAddressErrorCode.CIF_ADDRESS_IS_NOT_FOUND;

@AllArgsConstructor
public class MCifAddressErrorMessage implements AppMessage {

    private final Map<MCifAddressErrorCode, Object> data;
    private final MCifAddressErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CIF_ADDRESS_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case CIF_ADDRESS_IS_EMPTY:
                return errorIsEmpty();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data alamat dengan id " + data.get(CIF_ADDRESS_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data alamat tidak ada";
    }

}
