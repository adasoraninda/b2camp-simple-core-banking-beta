package com.adasoraninda.parameterservice.handle.exception.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.exception.code.MCifFamilyErrorCode;
import com.adasoraninda.parameterservice.handle.AppMessage;
import lombok.AllArgsConstructor;

import java.util.Map;

import static com.adasoraninda.parameterservice.handle.exception.code.MCifFamilyErrorCode.CIF_FAMILY_IS_NOT_FOUND;

@AllArgsConstructor
public class MCifFamilyErrorMessage implements AppMessage {

    private final Map<MCifFamilyErrorCode, Object> data;
    private final MCifFamilyErrorCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CIF_FAMILY_IS_NOT_FOUND:
                return errorNotFoundWithId();
            case CIF_FAMILY_IS_EMPTY:
                return errorIsEmpty();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String errorNotFoundWithId() {
        return "Data keluarga dengan id " + data.get(CIF_FAMILY_IS_NOT_FOUND) + " tidak ditemukan";
    }

    private String errorIsEmpty() {
        return "Semua data keluarga tidak ada";
    }

}