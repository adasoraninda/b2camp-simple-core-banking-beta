package com.adasoraninda.parameterservice.handle;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AppSuccessHandler<T> {
    private AppMessage successMessage;
    private T data;
}
