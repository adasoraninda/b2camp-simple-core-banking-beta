package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RReligionSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetReligions();
            case GET_DATA:
                return successGetReligion();
            case CREATE_DATA:
                return successCreateReligion();
            case UPDATE_DATA:
                return successUpdateReligion();
            case DELETE_DATA:
                return successDeleteReligion();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetReligions() {
        return "Berhasil mendapatkan semua data agama";
    }

    private String successGetReligion() {
        return "Berhasil mendapatkan data agama";
    }

    private String successCreateReligion() {
        return "Berhasil menambahkan data agama";
    }

    private String successUpdateReligion() {
        return "Berhasil mengubah data agama";
    }

    private String successDeleteReligion() {
        return "Berhasil menghapus data agama";
    }
}
