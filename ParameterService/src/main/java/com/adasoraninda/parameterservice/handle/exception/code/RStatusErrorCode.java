package com.adasoraninda.parameterservice.handle.exception.code;

public enum RStatusErrorCode {
    STATUS_IS_EMPTY,
    STATUS_IS_NOT_FOUND,
    STATUS_INVALID;
}
