package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MTransactionCodeSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAllTransactionCode();
            case GET_DATA:
                return successGetTransactionCode();
            case CREATE_DATA:
                return successCreateTransactionCode();
            case UPDATE_DATA:
                return successUpdateTransactionCode();
            case DELETE_DATA:
                return successDeleteTransactionCode();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAllTransactionCode() {
        return "Berhasil mendapatkan semua data kode transaksi";
    }

    private String successGetTransactionCode() {
        return "Berhasil mendapatkan data kode transaksi";
    }

    private String successCreateTransactionCode() {
        return "Berhasil menambahkan data kode transaksi";
    }

    private String successUpdateTransactionCode() {
        return "Berhasil mengubah data kode transaksi";
    }

    private String successDeleteTransactionCode() {
        return "Berhasil menghapus data kode transaksi";
    }
}
