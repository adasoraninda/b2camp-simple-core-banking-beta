package com.adasoraninda.parameterservice.handle.success.message;

import com.adasoraninda.parameterservice.handle.exception.AppCodeException;
import com.adasoraninda.parameterservice.handle.AppMessage;
import com.adasoraninda.parameterservice.handle.success.code.AppSuccessCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MCifAddressSuccessMessage implements AppMessage {

    private final AppSuccessCode code;

    @Override
    public String getCode() {
        return code.name();
    }

    @Override
    public String getMessage() {
        switch (code) {
            case GET_ALL_DATA:
                return successGetAddresses();
            case GET_DATA:
                return successGetAddress();
            case CREATE_DATA:
                return successCreateAddress();
            case UPDATE_DATA:
                return successUpdateAddress();
            case DELETE_DATA:
                return successDeleteAddress();
            default:
                throw new AppCodeException(code.name());
        }
    }

    private String successGetAddresses() {
        return "Berhasil mendapatkan semua data alamat";
    }

    private String successGetAddress() {
        return "Berhasil mendapatkan data alamat";
    }

    private String successCreateAddress() {
        return "Berhasil menambahkan data alamat";
    }

    private String successUpdateAddress() {
        return "Berhasil mengubah data alamat";
    }

    private String successDeleteAddress() {
        return "Berhasil menghapus data alamat";
    }

}
