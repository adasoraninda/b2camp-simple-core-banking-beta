package com.adasoraninda.parameterservice.handle;

public interface AppMessage {
    String getCode();
    String getMessage();
}
